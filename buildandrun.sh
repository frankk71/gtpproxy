#!/bin/bash
echo "cleanup"
killall gtpproxy
rm gtpproxy.log*

echo "building..."
cd release
make clean && make
cp gtpproxy ..
cd ..

echo "copy example config file..."
cp ./config/config.1socketloop ./conf

read -p "Press any key to run the gtpproxy" -n1 -s
clear
./gtpproxy -c conf &

echo -e "to start testing call nc -u 127.0.0.1 2123 and send some data\n"
tail -f gtpproxy.log