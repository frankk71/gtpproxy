/*
 * timerwheelmanager.h
 *
 *  Created on: Jul 26, 2019
 *      Author: xubuntu
 */

#ifndef INC_TIMERWHEELMANAGER_H_
#define INC_TIMERWHEELMANAGER_H_

#ifdef HAVE_STDINT_H
#include <stdint.h>
#else
#include <inttypes.h>
#endif

#include <vector>
#include <set>

#include "timer-wheel.h"

class TimerWheelEvent;

typedef std::tuple<void*/*TimerId*/,uint32_t/*KeyValue*/> TimerIdAndKeyType;
typedef std::vector<TimerIdAndKeyType> ExpiredTimersType;

class TimerWheelManager
{
public:
	TimerWheelManager();

	virtual ~TimerWheelManager();

	void AdvanceTimerWheel(const time_t Now, ExpiredTimersType& ExpiredTimers);
	void* CreateTimer(const uint32_t KeyValue, const time_t TimerTime);
	bool RescheduleTimer(void* TimerId, const time_t TimerTime);
	bool CancelTimer(void*& TimerId);

private:

	time_t	m_LastAdvanceOnTimerWheel;
	TimerWheel* m_TimerWheel;
	std::set<void*>m_TimerIdKeeper;

	ExpiredTimersType m_ExpiredTimers;

	friend class TimerWheelEvent;
};

#endif /* INC_TIMERWHEELMANAGER_H_ */
