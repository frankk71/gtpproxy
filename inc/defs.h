/*
 * defs.h
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#ifndef INC_DEFS_H_
#define INC_DEFS_H_

#define GTPC_DEFAULT_PORT 2123
#define GTPU_DEFAULT_PORT 2152
#define VTI_DEFAULT_PORT  9999

#define SOCKET_IDLE_TIMOUT_SEC 	0
#define SOCKET_IDLE_TIMOUT_MSEC 500
#define SOCKET_IDLE_TIMOUT_MUSEC (1000*SOCKET_IDLE_TIMOUT_MSEC)

enum ProxyDirection
{
	TO_SGW_REMOTE = 1,
	TO_PGW_REMOTE = 2
};

#define MAX_UDP_PACKET_RCV_SIZE 65535	//the path MTU is < 1500 thus the GTP packet size is smaller that that; Max for UDP is 65535 bytes

//#define TESTONLY
//#define NO_MAPPING			//disable sequence number and TEID mapping
//#define NO_SOURCE_CHECK		//disable source address check for TEID or Sequence number PDP record lookup

#endif /* INC_DEFS_H_ */
