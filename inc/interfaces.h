/*
 * interfaces.h
 *
 *  Created on: Feb 25, 2019
 *      Author: JpU FK
 */

#ifndef INC_INTERFACES_H_
#define INC_INTERFACES_H_

#include "gtppacket.h"
#include "pdprecord.h"
#include "defs.h"


class PgwManagerInterface
{
public:
	virtual ~PgwManagerInterface() {}

	virtual uint32_t MakeTeid(int PgwIndex, ProxyDirection Direction) = 0;
	virtual void UpdatePgwRemoteCounter(gtppacket::GTP_VERSION Version, uint32_t SequenceNumber, unsigned char RemoteRecoveryCounter, struct sockaddr_in& src) = 0;
	virtual void AssignPgw(const std::string& IMSI, const std::string& APN, GatewayAddress& Pgw, int& PacketGateway) = 0;
	virtual void OnPdpRecordRemoved(const int PacketGateway) = 0;
	virtual void GetReceivedEchoResponseFromPgw(const int PacketGateway, bool& ReceivedEchoResponseFromPgw) = 0;
};

class PdpManagerInterface
{
public:
	virtual ~PdpManagerInterface() {}

	virtual bool SetErrorIndicationOnPdp(uint32_t& Teid, int /*Direction*/, struct sockaddr_in& Source) = 0;
	virtual void SetPgwAddressIfNeeded(PdpRecord& pdpRecord) = 0;
	virtual void SetPgwAddressForTeidNsapiIfNeeded(uint32_t Teid, uint8_t LinkedNSAPI, PdpRecord& pdpRecord, struct sockaddr_in& Source) = 0;
	virtual bool GetGpduDestinationAddress(uint32_t& teid, ProxyDirection Direction, struct sockaddr_in& Destination, struct sockaddr_in& Source, bool &ReceivedEchoResponseFromPgw, uint32_t GpduSize) = 0;
	virtual bool GetOrInitPdpRecord(PdpRecord& pdpRecord, uint32_t& PdpRecordKey, unsigned char* buffer, ssize_t rcvsize, struct sockaddr_in fromaddr, uint8_t GtpMsgType, ProxyDirection Direction, uint32_t PacketTeid) = 0;
	virtual void AddOrUpdatePdpRecord(uint32_t PdpRecordKey, const PdpRecord& Record) = 0;
	virtual unsigned int AddDataPlaneReferencesIfNeeded(const PdpRecord& pdpRecord, const PdpRecord::DataPlaneTeids* DataPlaneTeids) = 0;
	virtual void AddPgwCreateSessionRequestReference(const PdpRecord& pdpRecord, struct sockaddr_in Dest) = 0;
};

class PdpManagerTimerInterface
{
public:
	virtual ~PdpManagerTimerInterface() {}

	virtual void StopTimerOnRecord(void*& RecordTimerId) = 0;
	virtual void StartTimerOnRecord(void*& RecordTimerId, const uint32_t RecordKeyValue, time_t DueTime) = 0;
};
//TODO interfaces for globals


#endif /* INC_INTERFACES_H_ */
