/*
 * messageprocessor.h
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#ifndef INC_MESSAGEPROCESSOR_H_
#define INC_MESSAGEPROCESSOR_H_

#include <netinet/in.h>

#include "interfaces.h"
#include "defs.h"

class MessageProcessor
{
private:

	PgwManagerInterface* m_ThePgwManagerInterface;
	PdpManagerInterface* m_ThePdpManagerInterface;
	PdpManagerTimerInterface* m_ThePdpManagerTimerInterface;
#ifdef TESTONLY
public:
#endif

	void ProcessEchoRequestMessage(gtppacket::GTP_VERSION Version, void* Buffer, size_t BufferSize, int fd_echo, struct sockaddr_in& echo);
	void ProcessEchoResponseMessage(gtppacket::GTP_VERSION Version, void* Buffer, size_t BufferSize, struct sockaddr_in& src);
	bool ProcessErrorIndicationMessage(uint32_t& Teid, ProxyDirection Direction, const unsigned char* Buffer, size_t BufferSize, int fd, struct sockaddr_in& from);

	bool HandleVersion1ControlMessage(PdpRecord &pdpRecord, unsigned char* buffer, ssize_t rcvsize, struct sockaddr_in fromaddr, uint8_t GtpMsgType, ProxyDirection Direction, uint32_t PacketTeid);
	bool HandleVersion2ControlMessage(PdpRecord &pdpRecord, unsigned char* buffer, ssize_t rcvsize, struct sockaddr_in fromaddr, uint8_t GtpMsgType, ProxyDirection Direction, uint32_t PacketTeid);

	void SendDataPlaneErrorIndicationMessage(uint32_t teid, int fd, struct sockaddr_in& dst, struct sockaddr_in& src) const;
	bool SendErrorResponseMessage(int fd, struct sockaddr_in& dst, gtppacket::GTP_VERSION Version, uint8_t GtpMsgType, uint32_t Sequence, uint8_t CauseValue) const;

public:

	MessageProcessor(PgwManagerInterface* PgwManagerInterface, PdpManagerInterface* PdpManagerInterface, PdpManagerTimerInterface* PdpManagerTimerInterface)
	{
		m_ThePgwManagerInterface = PgwManagerInterface;
		m_ThePdpManagerInterface = PdpManagerInterface;
		m_ThePdpManagerTimerInterface = PdpManagerTimerInterface;
	}
	virtual ~MessageProcessor() {}

	void HandleControlMessage(int fd_from, struct sockaddr_in bindaddr, int fd_src, struct sockaddr_in src, ProxyDirection Direction);
	void HandleUserDataMessage(int fd_from, struct sockaddr_in bindaddr, int fd_src, struct sockaddr_in src, ProxyDirection Direction);

	bool SendEchoRequestMessage(gtppacket::GTP_VERSION Version, uint32_t Sequence, uint8_t RestartCount, int fd, struct sockaddr_in& src, struct sockaddr_in& dst) const;
};

#endif /* INC_MESSAGEPROCESSOR_H_ */
