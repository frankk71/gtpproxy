/*
 * configuration.h
 *
 *  Created on: Mar 16, 2019
 *      Author: xubuntu
 */

#ifndef INC_CONFIGURATION_H_
#define INC_CONFIGURATION_H_

#include "defs.h"

#include <libconfig.h++>
#include <netinet/in.h>
#include <vector>

#define CONFIG_TIMER_GROUP		"Timer"
#define CONFIG_LOGGING_GROUP	"Logging"
#define CONFIG_VTY_GROUP		"VTY"
#define CONFIG_MD_GROUP			"MatchingDomains"
#define CONFIG_PGW_GROUP		"PacketGateways"
#define CONFIG_INTERFACES_GROUP	"Interfaces"

using namespace libconfig;

struct	localAddresses{
	sockaddr_in sgw_c_local_addr, sgw_u_local_addr, pgw_c_local_addr, pgw_u_local_addr;
};

struct pgwParams{
	std::string name, remoteaddr_c, remoteaddr_u;
	int version,remoteport_c,remoteport_u;
};

struct matchingDomainParams{
	std::string domain, APN_regex, IMSI_regex;
	int matchonlywhenconnected;
	std::vector<std::string> pgw_names;
};

class Configuration
{
private:

	Config cfg;
	bool m_IsOpen;
	std::string m_FileName;

	bool OpenConfig(const char* FileName);

public:

	Configuration(const char* FileName);

	bool IsOpen() const { return m_IsOpen; }
	bool HasConfigFileChanged();//only works once the filename remains consistent
	void ConfigFileChangeHandled();

	unsigned int getPgwConfigCount();
	bool readPgwConfig(int Index, pgwParams& Params);

	unsigned int getMatchingDomainConfigCount();
	bool readMatchingDomainConfig(int Index, matchingDomainParams& Params);

	struct	localAddresses m_localAddresses;
	bool readInterfaceConfig();//fills m_localAddresses

	bool readLoggingConfig(unsigned char& DebugLogLevel, unsigned char& DebugCategory);
	bool writeLoggingConfig(unsigned char DebugLogLevel, unsigned char DebugCategory);

	bool readVtyConfig(unsigned short& VtyPort);

	//Generic
	bool readStringConfig(const char* GroupName, const char* ValueName, std::string& Value);
	bool readIntConfig(const char* GroupName, const char* ValueName, int& Value);
	bool writeIntConfig(const char* GroupName, const char* ValueName, int Value);
};

#endif /* INC_CONFIGURATION_H_ */
