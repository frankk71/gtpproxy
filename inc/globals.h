/*
 * globals.h
 *
 *  Created on: Feb 25, 2019
 *      Author: JpU FK
 */

#ifndef INC_GLOBALS_H_
#define INC_GLOBALS_H_

#include  <netinet/in.h>

//globals for the time being
extern struct sockaddr_in sgw_c_local_addr, sgw_u_local_addr, pgw_c_local_addr, pgw_u_local_addr;

extern long long sgw_2_pgw_c_data_count;
extern long long sgw_2_pgw_u_data_count;
extern long long pgw_2_sgw_c_data_count;
extern long long pgw_2_sgw_u_data_count;

extern time_t g_ApplicationStartTime;

extern unsigned int g_LogStatisticsTimeout;
extern unsigned int g_PgwEchoRequestTimeout;
extern unsigned int g_GtpSessionCreateRequestTimeout;
extern unsigned int g_GtpSessionCreateRequestRejectTimeout;
extern unsigned int g_GtpSessionDeleteRequestTimeout;

#endif /* INC_GLOBALS_H_ */
