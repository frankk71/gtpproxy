/*
 * packetgateway.h
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#ifndef INC_PACKETGATEWAY_H_
#define INC_PACKETGATEWAY_H_

#include <regex>
#include <vector>

#include "gtppacket.h"
#include "gatewayaddress.h"

extern const char* GetRegexExceptionReason(const std::regex_error& e);

class PacketGateway : public GatewayAddress
{
private:

	const char* GetStatusString() const;

public:

	PacketGateway();

	unsigned int ConfigIndex;
	std::string Name;
	gtppacket::GTP_VERSION GtpEchoVersion;

	uint32_t NextCplaneSequenceNumber;
	unsigned char RemoteRecoveryCounter;
	//unsigned char LocalRecoveryCounter;
	time_t LastEchoRequestSend;
	time_t LastEchoResponseReceived;
	unsigned int TeidCount;
	unsigned int PdpCount;

	bool RespondedToEchoRequest() const;

	bool IsConnected() const;

	std::string ToString() const;
	std::string ToVtyString() const;
};


class MatchingDomain
{
private:

	bool FindAvailablePgw(const std::vector<PacketGateway>& ThePacketGateways, int& PacketGatewayIndex) const;

public:

	MatchingDomain();

	unsigned int ConfigIndex;
	std::string Domain;

	bool MatchOnlyWhenConnected;

	std::string ImsiMatchingString;
	std::string ApnMatchingString;

	std::regex ImsiMatchingRegEx;
	std::regex ApnMatchingRegEx;

	std::vector<std::string> PacketGatewayNames;

	bool AssignPgw(std::vector<PacketGateway>& ThePacketGateways, GatewayAddress& Pgw, int& PacketGatewayIndex) const;

	bool IsMatchingImsi(const std::string& IMSI) const;
	bool IsMatchingApn(const std::string& APN) const;


	std::string ToString() const;
	std::string ToVtyString() const;
};

#endif /* INC_PACKETGATEWAY_H_ */
