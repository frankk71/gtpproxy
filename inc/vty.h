#ifndef VTY_H_
#define VTY_H_

//#pragma once

#include <stddef.h>
#ifdef HAVE_STDINT_H
#include <stdint.h>
#else
#include <inttypes.h>
#endif
#include <unistd.h>
#include "../libvty/vty_server.h"

#include "utils.h"
#include "cmdline.h"

/* show_version */
extern vty_cmd_match show_version_mt();
extern void show_version_f(vty_cmd_match* m, vty_client* sh, void* arg);

/* exit */
extern vty_cmd_match exit_mt();
extern void exit_f(vty_cmd_match* m, vty_client* sh, void* arg);

/* clear */
vty_cmd_match clear_mt();
void clear_f(vty_cmd_match* m, vty_client* sh, void* arg);

/* list */
vty_cmd_match list_mt();
void list_f(vty_cmd_match* m, vty_client* sh, void* arg);

//======================================

class vty
{
	vty_server* v;
public:

	vty(uint32_t addr, uint16_t port);
	virtual ~vty() { delete v; }

	void poll_dispatch() { v->poll_dispatch(); }
	void fill_fd_array(std::vector<int>& fd_arr) { v->fill_fd_array(fd_arr); }

	void install_command(vty_cmd_match m, vty_cmdcallback_t f, void* arg)
	{
		v->install_command(m, f, arg);
	}
};

#endif /* VTY_H_ */
