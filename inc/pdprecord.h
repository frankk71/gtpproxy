/*
 * pdprecord.h
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#ifndef _PDPRECORD_H
#define _PDPRECORD_H

#include "gatewayaddress.h"
#include "gtppacket.h"
#include <map>

class PdpManagerTimerInterface;

enum PDP_STATE
{
	PDP_STATE_UNKNOWN = -1,
	PDP_STATE_CREATE_REQESTED = 0,
	PDP_STATE_CREATE_REJECTED = 1,
	PDP_STATE_CREATED = 2,
	PDP_STATE_DELETE_REQUESTED = 3,
	PDP_STATE_DELETED = 4
};

class PdpRecord
{
private:

	PDP_STATE m_PdpState;
	time_t m_PdpStateTime;

	time_t m_PdpCreateRequestTime;

	gtppacket::GTP_VERSION m_GtpVersion;

	//TODO: profile the effort updating the activity time & data params
	time_t LastSgwActivityTime;
	uint32_t SgwData;
	time_t LastPgwActivityTime;
	uint32_t PgwData;

public:

	PdpRecord();

	uint32_t KeyValue;//A unique key

	inline bool SetVersion(gtppacket::GTP_VERSION Version)
	{
		if(m_GtpVersion == gtppacket::GTP_VERSION::GTP_VERSION_UNKNOWN)
			m_GtpVersion = Version;

		return (m_GtpVersion == Version);
	}

	void SetPdpState(PDP_STATE NewState, PdpManagerTimerInterface* PdpManager);
	PDP_STATE GetPdpState() const { return m_PdpState; }

	const char* GetPdpStateStr(const PDP_STATE Val) const;

	uint32_t PdpCreateSgwSequenceNumber;
	uint32_t PdpCreatePgwSequenceNumber;

	//TBD whether more than one control message can be without response
	uint32_t PdpLastSgwSequenceNumber;
	uint32_t PdpLastPgwSequenceNumber;

	uint32_t ModificationCount;

	GatewayAddress Sgw;
	GatewayAddress Pgw;

	//TODO/TBD can there be more that one outstanding response with different port
	struct sockaddr_in SgwAddrPdpResponseAddress;//should be the same as S-GW c_plane address except for the port that may not be the standard value

	void ReportSgwData(uint32_t ByteCount, PdpManagerTimerInterface* PdpTimerManager, time_t AcivitTimeout);
	time_t GetLastSgwActivityTime() const { return LastSgwActivityTime;}
	void ReportPgwData(uint32_t ByteCount, PdpManagerTimerInterface* PdpTimerManager, time_t AcivitTimeout);
	time_t GetLastPgwActivityTime() const { return LastSgwActivityTime;}

	//TODO/TBD: proxy re-write of the charging gateway address
	//TODO/TBD: rewriting the S-GW TEID to a unique TEID towards the P-GW by hashing the S-GW IP for data plane & S-GW TEID
	uint32_t SgwTeidControlPlane;
	uint32_t SgwTeidControlPlaneMapped;

	uint32_t PgwTeidControlPlane;
	uint32_t PgwTeidControlPlaneMapped;

	typedef struct
	{
		uint32_t SgwTeidDataPlane;//TBD shall this be an array on max NSAPI value 0-15 for secondary PDP contexts or will secondary contexts be tracked by a dedicated PdpRecord?
		uint32_t SgwTeidDataPlaneMapped;//TBD shall this be an array on max NSAPI value 0-15 for secondary PDP contexts or will secondary contexts be tracked by a dedicated PdpRecord?
		uint32_t SgwTeidDataPlaneError;
		uint32_t SgwData;

		uint32_t PgwTeidDataPlane;//TBD shall this be an array on max NSAPI value 0-15 for secondary PDP contexts or will secondary contexts be tracked by a dedicated PdpRecord?
		uint32_t PgwTeidDataPlaneError;
		uint32_t PgwTeidDataPlaneMapped;//TBD shall this be an array on max NSAPI value 0-15 for secondary PDP contexts or will secondary contexts be tracked by a dedicated PdpRecord?
		uint32_t PgwData;
	}DataPlaneTeids;

	typedef std::map<uint8_t/*NSAPI_EBI*/,DataPlaneTeids> TeidMapType;
	typedef TeidMapType::const_iterator TeidMapTypeConstIter;
	typedef TeidMapType::iterator TeidMapTypeIter;

	TeidMapType DataPlaneContextMap;

	bool HaveNsapiEbi(uint8_t NSAPI_EBI)
	{
		return DataPlaneContextMap.find(NSAPI_EBI) != DataPlaneContextMap.end();
	}

	DataPlaneTeids* GetDataPlaneTeidsForNsapiEbi(uint8_t NSAPI_EBI)
	{
		if(!HaveNsapiEbi(NSAPI_EBI))
		{
			DataPlaneContextMap[NSAPI_EBI].SgwTeidDataPlane = (uint32_t)-1;//inbound Create Request from SGW
			DataPlaneContextMap[NSAPI_EBI].SgwTeidDataPlaneMapped = (uint32_t)-1;//outbound Create Request to PGW
			DataPlaneContextMap[NSAPI_EBI].PgwTeidDataPlane = (uint32_t)-1;//inbound Create Response from PGW
			DataPlaneContextMap[NSAPI_EBI].PgwTeidDataPlaneMapped = (uint32_t)-1;//outbound to Create Response SGW
			DataPlaneContextMap[NSAPI_EBI].SgwTeidDataPlaneError = DataPlaneContextMap[NSAPI_EBI].PgwTeidDataPlaneError = 0;
			DataPlaneContextMap[NSAPI_EBI].SgwData = DataPlaneContextMap[NSAPI_EBI].PgwData = 0;
		}
		return &DataPlaneContextMap[NSAPI_EBI];
	}

	DataPlaneTeids* GetDataPlaneTeidsForDplaneTeid(uint32_t DplaneTeid, bool PgwOnly = false)
	{
		for(TeidMapTypeIter Iter = DataPlaneContextMap.begin(), EndIter = DataPlaneContextMap.end();Iter!=EndIter;Iter++)
		{
			if( (!PgwOnly && DplaneTeid == Iter->second.SgwTeidDataPlane) || DplaneTeid == Iter->second.PgwTeidDataPlane )
				return &Iter->second;
		}
		return NULL;
	}

	DataPlaneTeids* GetDataPlaneTeidsForDplaneMappedTeid(uint32_t DplaneTeid)
	{
		for(TeidMapTypeIter Iter = DataPlaneContextMap.begin(), EndIter = DataPlaneContextMap.end();Iter!=EndIter;Iter++)
		{
			if( DplaneTeid == Iter->second.SgwTeidDataPlaneMapped || DplaneTeid == Iter->second.PgwTeidDataPlaneMapped )
				return &Iter->second;
		}
		return NULL;
	}

	std::string APN;
	std::string EUA;
	uint64_t IMSI;
	uint8_t RatType;//UTRAN 1, GERAN 2, WLAN 3, GAN 4, HSPA Evolution 5, E-UTRAN 6, Virtual 7

	time_t RemoveMeTime;//linger timeout after deletion

	void* TimerId;//pending timer

	int PacketGateway;//associated PacketGateway index on PgwManager::ThePacketGateways array

	std::string ToString() const;
	std::string ToVtyString(time_t Now) const;
};

#endif //_PDPRECORD_H
