/********************************************************************
Name:     Debug Macros 

Type:     CPP include file

Desc:     Define Function and macros for debug level. 

File:     unoHlrDebug.h

 **********************************************************************/
/* Debug Function Prototype */

#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <stdio.h>
#ifdef HAVE_STDINT_H
#include <stdint.h>
#else
#include <inttypes.h>
#endif
#include <time.h>
#include <limits.h>

//#define LOCK_ACCESS_WITH_PTHREAD_MUTEX

class Logger
{
    private:
		static bool  externalLogCycleCall;
        static unsigned char dbgLevel;
        static FILE* logFp;
        static char* logFileName;
        static char* logFilePath;
        static char  logFilePathName[PATH_MAX];
        static void  makeLogFilePathName();
        static unsigned int logMaxLogFileSize;//in bytes
        static unsigned int logMaxLogFileTime;//in seconds
        static FILE* getNewLogFile();
        static int 	 createLogFileIfNeeded();
    public:
        static void  setExternalLogCycleCall(bool On);
        static void  setLogfileName(const char* logFileName);
        static void  setLogfilePath(const char* logFilePath);
        static void  setLogfileMaxSize(const unsigned int maxSize);
        static void  setLogfileMaxTime(const unsigned int maxFileIime);
        static void  setDbgLevel(unsigned char dbglevel);
        static int   cycleLogFileIfNeeded(const time_t cur_time);
        static unsigned int getLogfileMaxSize();
        static unsigned int getLogfileMaxTime();
        static unsigned char getDbgLevel();
        static int   dbgFunc(const char* fileName, int line, const char *type);
        static int   dbgTime();
        static char* nowTime();
        static int   debugPrint(const char* str,...);
        static int   msgPrint(const char* str,...);
        static int   printHexDump(void *data, int len);
};

int Getlock(void);
int Unlock(void);

#define MSG_DBG_1 0x01 
#define MSG_DBG_2 0x02 
#define MSG_DBG_3 0x04
#define MSG_DBG_4 0x08
#define MSG_DBG_5 0x10
#define MSG_DBG_6 0x20
#define MSG_DBG_7 0x40
#define MSG_DBG_8 0x80

#define LOG_LEVEL_ERROR  	(Logger::getDbgLevel() & MSG_DBG_1)
#define LOG_LEVEL_WARN		(Logger::getDbgLevel() & MSG_DBG_2)
#define LOG_LEVEL_INFO  	(Logger::getDbgLevel() & MSG_DBG_3)
#define LOG_LEVEL_DEBUG  	(Logger::getDbgLevel() & MSG_DBG_4)
#define LOG_LEVEL_MEM  		(Logger::getDbgLevel() & MSG_DBG_5)
#define LOG_LEVEL_FTRACE  	(Logger::getDbgLevel() & MSG_DBG_6)
#define LOG_LEVEL_HEXDUMP  	(Logger::getDbgLevel() & MSG_DBG_7)
#define LOG_LEVEL_INFODUMP  (Logger::getDbgLevel() & MSG_DBG_8)

#define MAX_DEBUG_SIZE_DEFAULT (4 * 1024 * 1024)//4 MB
#define MAX_DEBUG_TIME_DEFAULT (60*60)//1 hour in seconds

#define ERROR(X)   	    if(LOG_LEVEL_ERROR && Getlock() && Logger::dbgFunc(__FILE__,__LINE__,"ERROR")  &&  Logger::debugPrint X ) Unlock()
#define WARN(X)   	    if(LOG_LEVEL_WARN && Getlock() && Logger::dbgFunc(__FILE__,__LINE__,"WARNING")  &&  Logger::debugPrint X ) Unlock()
#define INFO(X)    	    if(LOG_LEVEL_INFO && Getlock() && Logger::dbgFunc(__FILE__,__LINE__,"INFO")   &&  Logger::debugPrint X ) Unlock()
#define DEBUG(X)   	    if(LOG_LEVEL_DEBUG && Getlock() && Logger::dbgFunc(__FILE__,__LINE__,"DEBUG")  &&  Logger::debugPrint X ) Unlock()
#define MEM(X)   	    if(LOG_LEVEL_MEM && Getlock() && Logger::dbgFunc(__FILE__,__LINE__,"MEM")    &&  Logger::debugPrint X ) Unlock()
#define FTRACE(X)  	    if(LOG_LEVEL_FTRACE && Getlock() && Logger::dbgFunc(__FILE__,__LINE__,"FTRACE") &&  Logger::debugPrint X ) Unlock()

#define PRINT_DUMP(X)    if(LOG_LEVEL_HEXDUMP && Getlock() && Logger::printHexDump X) Unlock()
#define ERROR_DUMP(X)    if(Getlock() && Logger::printHexDump X) Unlock()
#define INFO_DUMP(X)     if(LOG_LEVEL_INFODUMP && Getlock() && Logger::printHexDump X) Unlock()

#define PRINT(X)         if((LOG_LEVEL_HEXDUMP || LOG_LEVEL_INFODUMP) && Logger::dbgFunc(__FILE__,__LINE__,"DUMP") && Logger::debugPrint X)


#define LOG_CATEGORY_1 0x01
#define LOG_CATEGORY_2 0x02
#define LOG_CATEGORY_3 0x04
#define LOG_CATEGORY_4 0x08
#define LOG_CATEGORY_5 0x10
#define LOG_CATEGORY_6 0x20
#define LOG_CATEGORY_7 0x40
#define LOG_CATEGORY_8 0x80

class LogCategory
{
public:
	static unsigned char logCategory;
};

#define LOG_CATEGORY_GTP_C (LogCategory::logCategory & LOG_CATEGORY_1)
#define LOG_CATEGORY_GTP_U (LogCategory::logCategory & LOG_CATEGORY_2)
#define LOG_CATEGORY_GTP_ECHO (LogCategory::logCategory & LOG_CATEGORY_3)
#define LOG_CATEGORY_GTP_CHECK (LogCategory::logCategory & LOG_CATEGORY_4)

#include "vty.h"

/* vty change loglevel*/
vty_cmd_match change_loglevel_mt();
void change_loglevel_f(vty_cmd_match* m, vty_client* sh, void* arg);

/* vty change loglevel*/
vty_cmd_match change_logcategory_mt();
void change_logcategory_f(vty_cmd_match* m, vty_client* sh, void* arg);

#endif
/* End of file */

