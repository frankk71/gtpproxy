/*
 * gatewayaddress.h
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#ifndef GATEWAYADDRESS_H_
#define GATEWAYADDRESS_H_

#include <netinet/in.h>
#include <string>

class GatewayAddress
{
public:
	struct sockaddr_in c_plane_addr;
	struct sockaddr_in u_plane_addr;

	GatewayAddress();

	std::string ToString() const;
};

#endif /* GATEWAYADDRESS_H_ */
