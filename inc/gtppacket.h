/*
 * gtppacket.h
 *
 *  Created on: Aug 29, 2018
 *      Author: JpU FK
 */

#ifndef _GTPPACKET_H
#define _GTPPACKET_H

#ifdef HAVE_STDINT_H
#include <stdint.h>
#else
#include <inttypes.h>
#endif

#include <arpa/inet.h>

#include <string>

namespace gtppacket
{

#define GTPV1_HEADER_MIN  		8
#define GTPV2_HEADER_MIN  		4
#define GTPV1_HEADER_WITH_SEQ  12

#define GTP_MAX_PL		2048
#define GTPV1IE_MAX_TLV	512
#define GTPV2IE_MAX_TLV	512

/// GTP version 1 extension header type definitions.
#define GTP_EXT_PDCP_PDU    0xC0 // PDCP PDU

// GTP V.1 message types
// 0 future use
#define GTPV1_ECHO_REQ          1 	// Echo Request
#define GTPV1_ECHO_RSP          2 	// Echo Response
#define GTPV1_NOT_SUPPORTED     3 	// Version Not Supported
// 4-7 GTP'
// 8-15 future use
#define GTPV1_CREATE_PDP_REQ   	16 	// Create PDP Context Request
#define GTPV1_CREATE_PDP_RSP   	17 	// Create PDP Context Response
#define GTPV1_UPDATE_PDP_REQ   	18 	// Update PDP Context Request
#define GTPV1_UPDATE_PDP_RSP   	19 	// Update PDP Context Response
#define GTPV1_DELETE_PDP_REQ   	20 	// Delete PDP Context Request
#define GTPV1_DELETE_PDP_RSP   	21 	// Delete PDP Context Response
#define GTPV1_INIT_PDP_ACTIVATE_REQ  22 	// Initiate PDP Context Activation Request
#define GTPV1_INIT_PDP_ACTIVATE_RSP  23 	// Initiate PDP Context Activation Response
// 24-25 future use
#define GTPV1_ERROR            	26	// Error Indication
#define GTPV1_PDU_NOTIF_REQ      	27	// PDU Notification Request
#define GTPV1_PDU_NOTIF_RSP      	28	// PDU Notification Response
#define GTPV1_PDU_NOTIF_REJ_REQ  	29	// PDU Notification Reject Request
#define GTPV1_PDU_NOTIF_REJ_RSP  	30	// PDU Notification Reject Response
#define GTPV1_SUPP_EXT_HEADER  	31	// Supported Extension Headers Notification
#define GTPV1_SND_ROUTE_REQ    	32	// Send Route Info for GPRS Request
#define GTPV1_SND_ROUTE_RSP    	33	// Send Route Info for GPRS Response
#define GTPV1_FAILURE_REQ      	34	// Failure Report Request
#define GTPV1_FAILURE_RSP      	35	// Failure Report Response
#define GTPV1_MS_PRESENT_REQ   	36	// MS GPRS Present Request
#define GTPV1_MS_PRESENT_RSP   	37	// MS GPRS Present Response
// 38-47 future use.
#define GTPV1_IDENT_REQ         48	// Identification Request
#define GTPV1_IDENT_RSP         49	// Identification Response
#define GTPV1_SGSN_CONTEXT_REQ 	50	// SGSN Context Request
#define GTPV1_SGSN_CONTEXT_RSP 	51	// SGSN Context Response
#define GTPV1_SGSN_CONTEXT_ACK 	52	// SGSN Context Acknowledge
#define GTPV1_FWD_RELOC_REQ    	53	// Forward Relocation Request
#define GTPV1_FWD_RELOC_RSP    	54	// Forward Relocation Response
#define GTPV1_FWD_RELOC_COMPL  	55	// Forward Relocation Complete
#define GTPV1_RELOC_CANCEL_REQ 	56	// Relocation Cancel Request
#define GTPV1_RELOC_CANCEL_RSP 	57	// Relocation Cancel Response
#define GTPV1_FWD_SRNS         	58	// Forward SRNS Context
#define GTPV1_FWD_RELOC_ACK    	59	// Forward Relocation Complete Acknowledge
#define GTPV1_FWD_SRNS_ACK     	60	// Forward SRNS Context Acknowledge
// 61-69 future use.
#define GTPV1_RAN_INFO_REL     	70	// RAN Information Relay
// 71-254 MBMS, GTP' or future use.
#define GTPV1_GPDU            	255 // G-PDU

#define GTPV2_ECHO_REQ     		1	// Echo Request
#define GTPV2_ECHO_RSP     		2	// Echo Response
//SGSN/MME to PGW
#define GTPV2_CREATE_SESSION_REQUEST    32	// Create Session Request
#define GTPV2_CREATE_SESSION_RESPONSE   33	// Create Session Response
#define GTPV2_MODIFY_BEARER_REQUEST     34	// Modify Bearer Request
#define GTPV2_MODIFY_BEARER_RESPONSE    35	// Modify Bearer Response
#define GTPV2_DELETE_SESSION_REQUEST    36	// Delete Session Request
#define GTPV2_DELETE_SESSION_RESPONSE   37	// Delete Session Response
//SGSN to PGW
#define GTPV2_CHANGE_NOTIFICATION_REQUEST   38	//Change Notification Request
#define GTPV2_CHANGE_NOTIFICATION_RESPONSE  39	//Change Notification Response

#define GTPV2_MODIFY_BEARER_COMMAND         		64	// Modify Bearer Command
#define GTPV2_MODIFY_BEARER_FAILURE_INDICATION      65 	// Modify Bearer Command Failure Ind.
#define GTPV2_DELETE_BEARER_COMMAND                 66	// Delete Bearer Command
#define GTPV2_DELETE_BEARER_FAILURE_INDICATION      67	// Delete Bearer Command Failure Ind.
#define GTPV2_BEARER_RESOURCE_COMMAND               68	// Bearer Resource Command
#define GTPV2_BEARER_RESOURCE_FAILURE_INDICATION    69	// Bearer Resource Command Failure Ind.
#define GTPV2_DOWNLINK_DATA_NOTIFICATION_FAILURE_INDICATION 70	// Downlink Data Notification Failure Indication
#define GTPV2_TRACE_SESSION_ACTIVATION                      71	// Trace Session Activation
#define GTPV2_TRACE_SESSION_DEACTIVATION                    72	// Trace Session De-activation
#define GTPV2_STOP_PAGING_INDICATION                        73	// Stop Paging Indication

#define GTPV2_CREATE_BEARER_REQUEST         95	// Create Bearer Request
#define GTPV2_CREATE_BEARER_RESPONSE        96	// Create Bearer Response
#define GTPV2_UPDATE_BEARER_REQUEST         97	// Update Bearer Request
#define GTPV2_UPDATE_BEARER_RESPONSE        98	// Update Bearer Response
#define GTPV2_DELETE_BEARER_REQUEST         99	// Delete Bearer Request
#define GTPV2_DELETE_BEARER_RESPONSE        100	// Delete Bearer Response


// GTPv1 Information elements from 3GPP 29.060
// IE TV types
#define GTPV1_IE_CAUSE           1 // Cause Length 1
#define GTPV1_IE_IMSI            2 // International Mobile Subscriber Identity Length 8
#define GTPV1_IE_RAI             3 // Routing Area Identity (RAI) Length 8
#define GTPV1_IE_TLLI            4 // Temporary Logical Link Identity (TLLI) Length 4
#define GTPV1_IE_P_TMSI          5 // Packet TMSI (P-TMSI) Length 4
#define GTPV1_IE_QOS_PROFILE0    6 // Quality of Service Profile GTPv0 Length 3
                               	   // 6-7 SPARE  // 6 is QoS Profile GTPv0
#define GTPV1_IE_REORDER         8 // Reordering Required Length 1
#define GTPV1_IE_AUTH_TRIPLET    9 // Authentication Triplet Length 28
                                // 10 SPARE
#define GTPV1_IE_MAP_CAUSE      11 // MAP Cause Length Length 1
#define GTPV1_IE_P_TMSI_S       12 // P-TMSI Signature Length Length 3
#define GTPV1_IE_MS_VALIDATED   13 // MS Validated Length Length 1
#define GTPV1_IE_RECOVERY       14 // Recovery Length Length 1
#define GTPV1_IE_SELECTION_MODE 15 // Selection Mode Length 1
#define GTPV1_IE_TEI_DI         16 // Tunnel Endpoint Identifier Data I Length 4
#define GTPV1_IE_TEI_C          17 // Tunnel Endpoint Identifier Control Plane Length 4
#define GTPV1_IE_TEI_DII        18 // Tunnel Endpoint Identifier Data II Length 5
#define GTPV1_IE_TEARDOWN       19 // Teardown Ind Length 1
#define GTPV1_IE_NSAPI          20 // NSAPI Length 1
#define GTPV1_IE_RANAP_CAUSE    21 // RANAP Cause Length 1
#define GTPV1_IE_RAB_CONTEXT    22 // RAB Context Length 9
#define GTPV1_IE_RP_SMS         23 // Radio Priority SMS Length 1
#define GTPV1_IE_RP             24 // Radio Priority Length 1
#define GTPV1_IE_PFI            25 // Packet Flow Id Length 2
#define GTPV1_IE_CHARGING_C     26 // Charging Characteristics Length 2
#define GTPV1_IE_TRACE_REF      27 // Trace Reference Length 2
#define GTPV1_IE_TRACE_TYPE     28 // Trace Type Length 2
#define GTPV1_IE_MS_NOT_REACH   29 // MS Not Reachable Reason Length 1
                                // 30-116 not used
// 117-126 Reserved for the GPRS charging protocol (see GTP' of GSM 12.15)
#define GTPV1_IE_CHARGING_ID   127 // Charging ID Length 4
// IE TLV types
#define GTPV1_IE_EUA           128 // End User Address
#define GTPV1_IE_MM_CONTEXT    129 // MM Context
#define GTPV1_IE_PDP_CONTEXT   130 // PDP Context
#define GTPV1_IE_APN           131 // Access Point Name
#define GTPV1_IE_PCO           132 // Protocol Configuration Options
#define GTPV1_IE_GSN_ADDR      133 // GSN Address
#define GTPV1_IE_MSISDN        134 // MS International PSTN/ISDN Number
#define GTPV1_IE_QOS_PROFILE   135 // Quality of Service Profile
#define GTPV1_IE_AUTH_QUINTUP  136 // Authentication Quintuplet
#define GTPV1_IE_TFT           137 // Traffic Flow Template
#define GTPV1_IE_TARGET_INF    138 // Target Identification
#define GTPV1_IE_UTRAN_TRANS   139 // UTRAN Transparent Container
#define GTPV1_IE_RAB_SETUP     140 // RAB Setup Information
#define GTPV1_IE_EXT_HEADER_T  141 // Extension Header Type List
#define GTPV1_IE_TRIGGER_ID    142 // Trigger Id
#define GTPV1_IE_OMC_ID        143 // OMC Identity
#define GTPV1_IE_RAN_TRANS_CONT	144 //RAN Transparent Container
#define GTPV1_IE_PDP_CTX_PRIO   145 // PDP Context Prioritization
#define GTPV1_IE_RAB_SET_INFO  146 // Additional RAB Setup Information
#define GTPV1_IE_SGSN_NUM	   147 // SGSN Number
#define GTPV1_IE_COMM_FLAGS    148 // Common Flags
#define GTPV1_IE_APN_RESTR 	   149 // APN Restriction
#define GTPV1_IE_RP_LCS    	   150 // Radio Priority LCS
#define GTPV1_IE_RAT_TYPE  	   151 // RAT Type
#define GTPV1_IE_USER_LOCATION 152 // CGI/SAI/RAI of where the MS is currently located
#define GTPV1_IE_MS_TIMEZONE   153 // MS TIMEZONE
#define GTPV1_IE_IMEI      	   154 // IMEI(SV)
#define GTPV1_IE_CAMEL_CIC	   155 // CAMEL Charging Information Container
#define GTPV1_IE_MBMS_UE_CTX   156 // MBMS UE Context
#define GTPV1_IE_TMGI          157 // Temporary Mobile Group Identity (TMGI)
#define GTPV1_IE_RIM_RA        158 // RIM Routing Address
#define GTPV1_IE_MBMS_PCO      159 // MBMS Protocol Configuration Options
#define GTPV1_IE_MBMS_RA       160 // MBMS Service Area
//TODO
#define GTPV1_IE_EARP_I    	   191 // Evolved Allocation/Retention Priority I
#define GTPV1_IE_EARP_II   	   192 // Evolved Allocation/Retention Priority II
//TODO
// 239-250 Reserved for the GPRS charging protocol (see GTP' of GSM 12.15)
#define GTPV1_IE_CHARGING_ADDR 251 // Charging Gateway Address
// 252-254 Reserved for the GPRS charging protocol (see GTP' of GSM 12.15)
#define GTPV1_IE_PRIVATE       255 // Private Extension

// GTPv2 Information elements from 3GPP 29.274
#define GTPV2_IE_IMSI                       1
#define GTPV2_IE_CAUSE                      2
#define GTPV2_IE_RECOVERY                   3
#define GTPV2_IE_APN                        71
#define GTPV2_IE_AMBR                       72
#define GTPV2_IE_EPS_BEARER_ID              73
#define GTPV2_IE_IMEI                       75
#define GTPV2_IE_MSISDN                     76
#define GTPV2_IE_INDICATION                 77
#define GTPV2_IE_PCO                        78
#define GTPV2_IE_PDN_ADDRESS_ALLOCATION     79
#define GTPV2_IE_EPS_BEARER_LVL_QOS         80
#define GTPV2_IE_EPS_FLOW_QOS               81
#define GTPV2_IE_RAT_TYPE                   82
#define GTPV2_IE_SERVING_NET                83
#define GTPV2_IE_EPS_BEARER_TFT             84
#define GTPV2_IE_TAD                        85
#define GTPV2_IE_ULI                        86
#define GTPV2_IE_FTEID                      87
#define GTPV2_IE_BEARER_CONTEXT             93
#define GTPV2_IE_CHARGING_ID                94
#define GTPV2_IE_CHARGING_CHAR              95
#define GTPV2_IE_PDN_TYPE                   99
#define GTPV2_IE_PROCEDURE_TRANS_ID         100
#define GTPV2_IE_UE_TIMEZONE                114
#define GTPV2_IE_APN_RESTRICTION            127
#define GTPV2_IE_SEL_MODE                   128
#define GTPV2_IE_FQCSID                     132

// GTP information element cause codes from 3GPP 29.060
#define GTPV1_CAUSE_REQ_IMSI                   0 // Request IMSI
#define GTPV1_CAUSE_REQ_IMEI                   1 // Request IMEI
#define GTPV1_CAUSE_REQ_IMSI_IMEI              2 // Request IMSI and IMEI
#define GTPV1_CAUSE_NO_ID_NEEDED               3 // No identity needed
#define GTPV1_CAUSE_MS_REFUSES_X               4 // MS refuses
#define GTPV1_CAUSE_MS_NOT_RESP_X              5 // MS is not GPRS responding
#define GTPV1_CAUSE_006                        6 // For future use 6-48
#define GTPV1_CAUSE_049                       49 // Cause values reserved for GPRS charging protocol use (See GTP' in GSM 12.15) 49-63
#define GTPV1_CAUSE_064                       64 // For future use 64-127
#define GTPV1_CAUSE_ACC_REQ                  128 // Request accepted
#define GTPV1_CAUSE_129                      129 // For future use 129-176
#define GTPV1_CAUSE_177                      177 // Cause values reserved for GPRS charging protocol use (See GTP' In GSM 12.15) 177-191
#define GTPV1_CAUSE_NON_EXIST                192 // Non-existent
#define GTPV1_CAUSE_INVALID_MESSAGE          193 // Invalid message format
#define GTPV1_CAUSE_IMSI_NOT_KNOWN           194 // IMSI not known
#define GTPV1_CAUSE_MS_DETACHED              195 // MS is GPRS detached
#define GTPV1_CAUSE_MS_NOT_RESP              196 // MS is not GPRS responding
#define GTPV1_CAUSE_MS_REFUSES               197 // MS refuses
#define GTPV1_CAUSE_198                      198 // For future use
#define GTPV1_CAUSE_NO_RESOURCES             199 // No resources available
#define GTPV1_CAUSE_NOT_SUPPORTED            200 // Service not supported
#define GTPV1_CAUSE_MAN_IE_INCORRECT         201 // Mandatory IE incorrect
#define GTPV1_CAUSE_MAN_IE_MISSING           202 // Mandatory IE missing
#define GTPV1_CAUSE_OPT_IE_INCORRECT         203 // Optional IE incorrect
#define GTPV1_CAUSE_SYS_FAIL                 204 // System failure
#define GTPV1_CAUSE_ROAMING_REST             205 // Roaming Restriction
#define GTPV1_CAUSE_PTIMSI_MISMATCH          206 // P-TMSI signature mismatch
#define GTPV1_CAUSE_CONN_SUSP                207 // GPRS connection suspended
#define GTPV1_CAUSE_AUTH_FAIL                208 // Authentication failure
#define GTPV1_CAUSE_USER_AUTH_FAIL           209 // User authentication failed
#define GTPV1_CAUSE_CONTEXT_NOT_FOUND        210 // Context not found
#define GTPV1_CAUSE_ADDR_OCCUPIED            211 // All dynamic PDP addresses are occupied
#define GTPV1_CAUSE_NO_MEMORY                212 // No memory is available
#define GTPV1_CAUSE_RELOC_FAIL               213 // Relocation failure
#define GTPV1_CAUSE_UNKNOWN_MAN_EXTHEADER    214 // Unknown mandatory extension header
#define GTPV1_CAUSE_SEM_ERR_TFT              215 // Semantic error in the TFT operation
#define GTPV1_CAUSE_SYN_ERR_TFT              216 // Syntactic error in the TFT operation
#define GTPV1_CAUSE_SEM_ERR_FILTER           217 // Semantic errors in packet filter(s)
#define GTPV1_CAUSE_SYN_ERR_FILTER           218 // Syntactic errors in packet filter(s)
#define GTPV1_CAUSE_MISSING_APN              219 // Missing or unknown APN
#define GTPV1_CAUSE_UNKNOWN_PDP              220 // Unknown PDP address or PDP type
#define GTPV1_CAUSE_221                      221 // For Future Use 221-240
#define GTPV1_CAUSE_241                      241 // Cause Values Reserved For Gprs Charging Protocol Use (See Gtp' In Gsm 12.15) 241-255

#define GTPV2_CAUSE_REQUEST_ACCEPTED                         16
#define GTPV2_CAUSE_REQUEST_PARTIALLY_ACCEPTED               17
#define GTPV2_CAUSE_NEW_PDN_TYPE_DUE_TO_NW_PREFERENCE        18
#define GTPV2_CAUSE_NEW_PDN_TYPE_DUE_TO_SINGLE_ADDR_BEARER   19
#define GTPV2_CAUSE_CONTEXT_NOT_FOUND                        64
#define GTPV2_CAUSE_INVALID_MSG_FORMAT                       65
#define GTPV2_CAUSE_VERSION_NOT_SUPPORTED_BY_NEXT_PEER       66
#define GTPV2_CAUSE_INVALID_LENGTH                           67
#define GTPV2_CAUSE_SERVICE_NOT_SUPPORTED                    68
#define GTPV2_CAUSE_MANDATORY_IE_INCORRECT                   69
#define GTPV2_CAUSE_MANDATORY_IE_MISSING                     70
#define GTPV2_CAUSE_RESERVED                                 71
#define GTPV2_CAUSE_SYSTEM_FAILURE                           72
#define GTPV2_CAUSE_NO_RESOURCE_AVAILABLE                    73
#define GTPV2_CAUSE_SEMANTIC_ERR_IN_TFT                      74
#define GTPV2_CAUSE_SYNTACTIC_ERR_IN_TFT                     75
#define GTPV2_CAUSE_SEMANTIC_ERR_IN_PACKET_FILTER            76
#define GTPV2_CAUSE_SYNTACTIC_ERR_IN_PACKET_FILTER           77
#define GTPV2_CAUSE_MISSING_OR_UNKNOWN_APN                   78
#define GTPV2_CAUSE_GRE_KEY_NOT_FOUND                        80
#define GTPV2_CAUSE_RELOCATION_FAILURE                       81
#define GTPV2_CAUSE_DENIED_IN_RAT                            82
#define GTPV2_CAUSE_PREFFERED_PDN_TYPE_NOT_SUPPORTED         83
#define GTPV2_CAUSE_ALL_DYNAMIC_ADDRESSES_OCCUPIED           84
#define GTPV2_CAUSE_UE_CTX_WITHOUT_TFT_ALREADY_ACTIVATED     85
#define GTPV2_CAUSE_PROTOCOL_TYPE_NOT_SUPPORTED              86
#define GTPV2_CAUSE_UE_NOT_RESPONDING                        87
#define GTPV2_CAUSE_UE_REFUSES                               88
#define GTPV2_CAUSE_SERVICE_DENIED                           89
#define GTPV2_CAUSE_UNABLE_TO_PAGE_UE                        90
#define GTPV2_CAUSE_NO_MEMORY_AVAILABLE                      91
#define GTPV2_CAUSE_USER_AUTH_FAILED                         92
#define GTPV2_CAUSE_APN_ACCESS_DENIED                        93
#define GTPV2_CAUSE_REQUEST_REJECTED                         94
#define GTPV2_CAUSE_PTMSI_SIGNATURE_MISMATCH                 95
#define GTPV2_CAUSE_IMSI_NOT_KNOWN                           96
#define GTPV2_CAUSE_SEMANTIC_ERROR_IN_TAD_OPERATION          97
#define GTPV2_CAUSE_SYNTACTIC_ERROR_IN_TAD_OPERATION         98
#define GTPV2_CAUSE_REMOTE_PEER_NOT_REPSONDING               100
#define GTPV2_CAUSE_COLLISION_WITH_NW_INITIATED_REQ          101
#define GTPV2_CAUSE_UNABLE_TO_PAGE_UE_DUE_TO_SUSPENSION      102
#define GTPV2_CAUSE_CONDITIONAL_IE_MISSING                   103
#define GTPV2_CAUSE_APN_RESTRICTION_TYPE_INCOMPATIBLE_WITH_CURRENTLY_ACTIVE_PDN_CONN       104
#define GTPV2_CAUSE_INVALID_OVERALL_LEN_OF_TRIGGERED_MSG_AND_PIGGYBACKED_INITIAL_MSG       105
#define GTPV2_CAUSE_DATA_FORWARDING_NOT_SUPPORTED            106
#define GTPV2_CAUSE_INVALID_REPLY_FROM_REMOTE_PEER           107
#define GTPV2_CAUSE_FALLBACK_TO_GTPV1                        108
#define GTPV2_CAUSE_INVALID_PEER                             109
#define GTPV2_CAUSE_TEMPORARY_REJECTED_DUE_TO_HANDOVER_IN_PROGRESS  110
#define GTPV2_CAUSE_MODIFICATIONS_NOT_LIMITED_TO_S1U_BEARERS        111
#define GTPV2_CAUSE_REQUEST_REJECTED_FOR_A_PMIPV6_REASON     112
#define GTPV2_CAUSE_APN_CONGESTION                           113
#define GTPV2_CAUSE_BEARER_HANDLING_NOT_SUPPORTED            114
#define GTPV2_CAUSE_UE_ALREADY_REATTACHED                    115


struct gtpv1HeaderShortVersion { //    Descriptions from 3GPP 29.060
  uint8_t  flags;         // Flag Bits
  	  	  	  	  	  	   // Example:
                           //    001..... Version: 1
                           //    ...1.... Protocol Type: GTP=1, GTP'=0
                           //    ....0... Spare = 0
                           //    .....0.. Extension header flag: 0
                           //    ......0. Sequence number flag: 0
                           //    .......0 PN: N-PDU Number flag
  uint8_t  type;	   	  // Message type. T-PDU = 0xff
  uint16_t length;	   	  // Length
  uint32_t teid;          // Tunnel Endpoint ID
} __attribute__((packed));

struct gtpv1HeaderExtendedVersion	{  //    Descriptions from 3GPP 29.060
  uint8_t  flags;         // Flag Bits
 	  	   	   	   	   	   // Example:
  	  	  	  	  	  	   //    001..... Version: 1
                           //    ...1.... Protocol Type: GTP=1, GTP'=0
                           //    ....0... Spare = 0
  	  	  	  	  	  	   //    Header is extended once one of the following flags are set:
                           //    .....0.. Extension header flag: 0
                           //    ......1. Sequence number flag: 1
                           //    .......0 PN: N-PDU Number flag
  uint8_t  type;	   	  // Message type. T-PDU = 0xff
  uint16_t length;	   	  // Length
  uint32_t teid;          // Tunnel Endpoint ID
  uint16_t seq;	   		  // Sequence Number
  uint8_t  npdu;	      // N-PDU Number
  uint8_t  next;	      // Next extension header type. Empty = 0
} __attribute__((packed));

struct gtpv2HeaderShortVersion
{
    uint8_t flags;  	        // Flag Bits
						         // Example:
						         //  010..... Version: 2
						         //  ...1.... Piggybacking flag 0 or 1; Additional GTP-C message at the end of the message when present
						         //  ....0... TEID flag, TEID present or not
						         //  .....000  SPARE
    uint8_t  type;	   	        // Message type. T-PDU = 0xff
    uint16_t length;	        // Length
    uint32_t seqwithspare;		// Sequence is 3 bytes in length. The last byte is a spare

    uint32_t getseq() {
    	return ntohl(seqwithspare) >> 8;
    }

    void setseq(uint32_t seq) {
    	seqwithspare = (ntohl(seq << 8)) & 0x00FFFFFF;
    }

    bool teidPresent() {
    	return ((flags & 0x08) != 0);
    }

    bool piggybackedPresent() {
    	return ((flags & 0x10) != 0);
    }
} __attribute__((packed));

struct gtpv2HeaderExtendedVersion
{
    uint8_t flags;  	        // Flag Bits
						         // Example:
						         //  010..... Version: 2
						         //  ...1.... Piggy-backing flag 0 or 1; Additional GTP-C message at the end of the message when present
						         //  ....1... TEID flag, TEID present or not
						         //  .....000  SPARE
    uint8_t  type;	   	        // Message type. T-PDU = 0xff
    uint16_t length;	        // Length
    uint32_t teid;   	        // Tunnel Endpoint ID. Presence depends on TEID flag in the flags field
    uint32_t seqwithspare;		// Sequence is 3 bytes in length. The last byte is a spare

    uint32_t getseq() {
    	return ntohl(seqwithspare) >> 8;
    }

    void setseq(uint32_t seq) {
    	seqwithspare = (ntohl(seq << 8)) & 0x00FFFFFF;
    }

    bool teidPresent() {
    	return ((flags & 0x08) != 0);
    }

    bool piggybackedPresent() {
    	return ((flags & 0x10) != 0);
    }
} __attribute__((packed));

struct gtpv1PacketShortVersion {
  struct gtpv1HeaderShortVersion header;
  uint8_t payload[GTP_MAX_PL];
} __attribute__((packed));

struct gtpv1PacketLongVersion {
  struct gtpv1HeaderExtendedVersion header;
  uint8_t payload[GTP_MAX_PL];
} __attribute__((packed));

struct gtpv2PacketShortVersion {
  struct gtpv2HeaderShortVersion header;
  uint8_t payload[GTP_MAX_PL];
} __attribute__((packed));

struct gtpv2PacketLongVersion {
  struct gtpv2HeaderExtendedVersion header;
  uint8_t payload[GTP_MAX_PL];
} __attribute__((packed));

union gtpPacketHelper {
  uint8_t flags;
  struct gtpv1PacketShortVersion  	gtp1s;
  struct gtpv1PacketLongVersion   	gtp1l;
  struct gtpv2PacketShortVersion	gtp2s;
  struct gtpv2PacketLongVersion		gtp2l;
} __attribute__((packed));


// GTP information element in network byte order
struct gtpv1_ie_tv {
  uint8_t type;//value < 128
  uint8_t data[GTPV1IE_MAX_TLV];//real length tag value dependent
}__attribute__((packed));

struct gtpv1_ie_tlv {
  uint8_t type;
  uint16_t len;//real length of data element; present only when MSB of type is set (tag value > 127). See TV type
  uint8_t data[GTPV1IE_MAX_TLV];
}__attribute__((packed));

struct gtpv2_ie_tlv
{
    uint8_t type;
    uint16_t length;//real length of data element
    uint8_t instance;//LS 4 bit only
    uint8_t data[GTPV2IE_MAX_TLV];
};

typedef enum
{
	GTP_VERSION_UNKNOWN = -1,
	GTP_VERSION_0		= 0,
	GTP_VERSION_1		= 1,
	GTP_VERSION_2		= 2,
}GTP_VERSION;

const char* getGtpVersionStr(const GTP_VERSION val);

// Gets the header version of a GTP packet
// Returns -1 on error
extern GTP_VERSION getGtpHeaderVersion(const void *data, const uint32_t datalen);
// Get message type of a packet.
// Returns 0 on error - message type reserved for future use
extern uint8_t getGtpHeaderMessageType(const void *data, const uint32_t datalen);
// Gets the header length of a GTP packet
// Returns 0 on error
extern uint16_t getGtpHeaderLength(const void *data, const uint32_t datalen);
// Get sequence number of a packet.
// Returns 0 on error
extern uint32_t getGtpHeaderSequenceNumber(const void *data, const uint32_t datalen);
// Set SequenceNumber of a packet. Valid header required in input data.
extern void setGtpHeaderSequenceNumber(void *data, const uint32_t datalen, const uint32_t SequenceNumber);
// Get TEID of a packet.
// Returns (uint32_t)(-1) on error
extern uint32_t getGtpHeaderTeid(const void *data, const uint32_t datalen);
// Set TEID of a packet. Valid header required in input data.
extern void setGtpHeaderTeid(void *data, const uint32_t datalen, const uint32_t teid);
// Get payload length of a packet.
// Returns 0 on error
extern uint16_t getGtpHeaderPayloadLength(const void *data, const uint32_t datalen);

extern bool checkGtpPacket(const void *data, const uint32_t datalen, const bool Log);

extern bool getGtpv1Ie(const unsigned char* packet, const unsigned int packetlen, const unsigned char Gtpv1IeTag, const unsigned char IeInstance, unsigned char*& iedata, size_t& ielen);
extern bool getGtpv2Ie(const unsigned char* packet, const unsigned int packetlen, const unsigned char Gtpv2IeTag, int& IeInstance, unsigned char*& iedata, size_t& ielen);
extern bool getGtpv2NestedIe(const unsigned char* ievalue, const unsigned int ievaluelen, const unsigned char Gtpv2IeTag, int& IeInstance, unsigned char*& iedata, size_t& ielen);

extern uint8_t getResponseMessageType(GTP_VERSION Version, uint8_t RequestMessageType);
extern std::string getGtpv2MessageTypeStr(const uint8_t Tag);
extern std::string getGtpv1MessageTypeStr(const uint8_t Tag);

inline bool isRequestGtpMsgType(uint8_t GtpMsgType)
{
	return (GtpMsgType == GTPV1_CREATE_PDP_REQ ||
			GtpMsgType == GTPV1_UPDATE_PDP_REQ ||
			GtpMsgType == GTPV1_DELETE_PDP_REQ ||
			GtpMsgType == GTPV1_INIT_PDP_ACTIVATE_REQ ||
			GtpMsgType == GTPV2_CREATE_SESSION_REQUEST ||
			GtpMsgType == GTPV2_MODIFY_BEARER_REQUEST ||
			GtpMsgType == GTPV2_DELETE_SESSION_REQUEST ||
			GtpMsgType == GTPV2_CHANGE_NOTIFICATION_REQUEST ||
			GtpMsgType == GTPV2_CREATE_BEARER_REQUEST ||
			GtpMsgType == GTPV2_UPDATE_BEARER_REQUEST ||
			GtpMsgType == GTPV2_DELETE_BEARER_REQUEST);
}

inline bool isResponseGtpMsgType(uint8_t GtpMsgType)
{
	return (GtpMsgType == GTPV1_CREATE_PDP_RSP ||
			GtpMsgType == GTPV1_UPDATE_PDP_RSP ||
			GtpMsgType == GTPV1_DELETE_PDP_RSP ||
			GtpMsgType == GTPV1_INIT_PDP_ACTIVATE_RSP ||
			GtpMsgType == GTPV2_CREATE_SESSION_RESPONSE ||
			GtpMsgType == GTPV2_MODIFY_BEARER_RESPONSE ||
			GtpMsgType == GTPV2_DELETE_SESSION_RESPONSE ||
			GtpMsgType == GTPV2_CHANGE_NOTIFICATION_RESPONSE ||
			GtpMsgType == GTPV2_CREATE_BEARER_RESPONSE ||
			GtpMsgType == GTPV2_UPDATE_BEARER_RESPONSE ||
			GtpMsgType == GTPV2_DELETE_BEARER_RESPONSE);
}

const char* getRatTypeStr(const uint8_t val);

}

#endif //_GTPPACKET_H
