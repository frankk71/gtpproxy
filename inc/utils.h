/*
 * utils.h
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#ifndef INC_UTILS_H_
#define INC_UTILS_H_

#ifdef HAVE_STDINT_H
#include <stdint.h>
#else
#include <inttypes.h>
#endif
#include <string>
#include <vector>

extern void HexDumpToStr(const unsigned char* data, const unsigned int len, std::string& dump, bool reversenibble = false);
extern void ApnToStr(const unsigned char* data, const unsigned int len, std::string& dump);
extern std::string FormatStr(const char *fmt, ...);
extern std::string FormatLocalTime(const time_t t);
extern const char* GetIPv4AddressStr(const uint32_t Address);
extern void CheckSocketErrorNo(int Result, struct sockaddr_in& addr, const char* Operation);
extern std::string VectorOfStringsToCsv(const std::vector<std::string>& StringArray);


#endif /* INC_UTILS_H_ */
