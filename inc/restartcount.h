/*
 * restartcount.h
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#ifndef INC_RESTARTCOUNT_H_
#define INC_RESTARTCOUNT_H_

class RestartCounter
{
public:

	RestartCounter() {}

	static unsigned int RecoveryCounter;//the global recovery counter. TBD: perhaps make this larger than the largest received in an GTP echo response from any GGSN

	static void WriteRecoveryCounter(FILE *fptr);
	static void InitRecoveryCounter();
};

#include "vty.h"

/* vty change g_recovery_counter*/
extern vty_cmd_match change_recovery_counter_mt();
extern void change_recovery_counter_f(vty_cmd_match* m, vty_client* sh, void* arg);

#endif /* INC_RESTARTCOUNT_H_ */
