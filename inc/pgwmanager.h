/*
 * pgwmanager.h
 *
 *  Created on: Feb 25, 2019
 *      Author: JpU FK
 */

#ifndef INC_PGWMANAGER_H_
#define INC_PGWMANAGER_H_

#include "interfaces.h"
#include "packetgateway.h"
#include "cmdline.h"
#include "messageprocessor.h"
#include "utils.h"
#include "vty.h"
#include "configuration.h"

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Packet Gateway Manager ///////////////////////////
/////////////////////////////////////////////////////////////////////////////////

class PgwManager : public PgwManagerInterface
{
private:

	time_t m_LastPgwArrayMaintenanceTime;
	uint32_t m_TeidCount;

	typedef std::vector<PacketGateway> PacketGatewayArray;
	PacketGatewayArray ThePacketGateways;

	typedef std::vector<MatchingDomain> MatchingDomainArray;
	MatchingDomainArray TheMatchingDomains;

	void GetMdConfiguration(unsigned int ConfigIndex, const matchingDomainParams& Params);
	void GetPgwConfiguration(unsigned int ConfigIndex, const pgwParams& Params);
	void LogPgwConfiguration(const char* Context, struct sockaddr_in& localaddr, struct sockaddr_in& remoteaddr) const;

public:

	PgwManager();
	virtual ~PgwManager() {}

	//from PgwManagerInterface
	virtual uint32_t MakeTeid(int PgwIndex, ProxyDirection Direction);
	virtual void UpdatePgwRemoteCounter(gtppacket::GTP_VERSION Version, uint32_t SequenceNumber, unsigned char RemoteRecoveryCounter, struct sockaddr_in& src);
	virtual void AssignPgw(const std::string& IMSI, const std::string& APN, GatewayAddress& Pgw, int& PacketGateway);
	virtual void OnPdpRecordRemoved(const int PacketGateway);
	virtual void GetReceivedEchoResponseFromPgw(const int PacketGateway, bool& ReceivedEchoResponseFromPgw);

	bool Configure(Configuration& Config);
	void UpdateConfigValues(Configuration& Config);
	void TimerMaintenance(time_t Now, int pgwc_fd, const MessageProcessor& TheMessageProcessor);

	//VTY functions
	void ShowPgwList(vty_client* sh) const;
	void FindMatchingPgw(vty_client* sh, const std::string& matchingToken) const;

};

/* vty show P-GW*/
extern vty_cmd_match show_pgw_mt();
extern void show_pgw_f(vty_cmd_match* m, vty_client* sh, void* arg);
/* vty util find matching P-GW*/
extern vty_cmd_match util_match_query_mt();
extern void util_match_query_f(vty_cmd_match* m, vty_client* sh, void* arg);

#endif /* INC_PGWMANAGER_H_ */
