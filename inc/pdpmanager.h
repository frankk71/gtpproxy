/*
 * pdpmanager.h
 *
 *  Created on: Feb 25, 2019
 *      Author: JpU FK
 */

#ifndef INC_PDPMANAGER_H_
#define INC_PDPMANAGER_H_

#include <tuple>

#include "interfaces.h"
#include "vty.h"
#include "configuration.h"
#include "timerwheelmanager.h"

#include <map>

/////////////////////////////////////////////////////////////////////////////////
///////////////////////////// PDP Record Manager ////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

class PdpManager : public PdpManagerInterface, public PdpManagerTimerInterface
{
private:

	PgwManagerInterface* m_ThePgwManagerInterface;
	time_t m_LastPdpRecordArrayMaintenanceTime;

	uint32_t m_PdpKeyNumberGenerator;
	uint16_t m_PdpSequenceNumberGenerator;

	bool GetPdpRecordPgwTeidControlPlane(uint32_t Teid, PdpRecord& Record, struct sockaddr_in& Source) const;
	bool GetPdpRecordSgwTeidControlPlane(uint32_t Teid, PdpRecord& Record, struct sockaddr_in& Source) const;
	bool GetGpduPgwDstAddrForTeidDataPlane(uint32_t& Teid, struct sockaddr_in& Destination, struct sockaddr_in& Source, bool &ReceivedEchoResponseFromPgw, uint32_t GpduSize);
	bool GetGpduSgwDstAddrForTeidDataPlane(uint32_t& Teid, struct sockaddr_in& Destination, struct sockaddr_in& Source, uint32_t GpduSize);
	bool GetCreatePdpRecord(uint32_t SequenceNumber, PdpRecord& Record, struct sockaddr_in& Source, bool CreateRequest);

	//TODO use a (in-memory) DB with multiple key values (sequence numbers for create req/rsp, teid values for control & data planes).
	//Until then iterate over the entire PdpRecordArray
	typedef std::map<uint32_t/*PdpRecord Key*/,PdpRecord> PdpRecords;//std::map to reduce the record update effort
	typedef PdpRecords::const_iterator PdpRecordsMapConstIter;
	typedef PdpRecords::iterator PdpRecordsMapIter;
	PdpRecords m_PdpRecords;

	typedef std::tuple<in_addr_t/*SourceIp*/, uint32_t/*Teid Or Sequence*/> SourceTeid;
	typedef std::map<SourceTeid, uint32_t/*PdpRecord Key*/> TeidRecordKeyMap;
	typedef TeidRecordKeyMap::const_iterator TeidRecordKeyMapConstIter;
	typedef TeidRecordKeyMap::iterator TeidRecordKeyMapIter;

	//TBD merge the two/four TEID+Source <-> Record.KeyValue maps into one. See TimerMaintenance iteration for cleanup ( O(log(n)) vs O(n) )
	TeidRecordKeyMap m_SgwTeidDataPlaneRecordKeyMap; //key is MakeTeidDataPlaneRecordKey(record.Sgw.u_plane_addr.sin_addr.s_addr,record.PgwTeidDataPlaneMapped)
	TeidRecordKeyMap m_PgwTeidDataPlaneRecordKeyMap; //key is MakeTeidDataPlaneRecordKey(record.Pgw.u_plane_addr.sin_addr.s_addr,record.SgwTeidDataPlaneMapped)

	//TODO faster record lookup for control plane messages with TEID != 0
	//TBD merge the two/four TEID+Source <-> Record.KeyValue maps into one. See TimerMaintenance iteration for cleanup ( O(log(n)) vs O(n) )
	//TeidRecordKeyMap m_SgwTeidControlPlaneRecordKeyMap; //key is MakeSequenceControlPlaneRecordKey(record.Sgw.c_plane_addr.sin_addr.s_addr, record.PgwTeidControlPlaneMapped)
	//TeidRecordKeyMap m_PgwTeidControlPlaneRecordKeyMap; //key is MakeSequenceControlPlaneRecordKey(record.Pgw.c_plane_addr.sin_addr.s_addr, record.SgwTeidControlPlaneMapped)

	inline SourceTeid MakeTeidDataPlaneRecordKey(const in_addr_t s_addr, const uint32_t TeidDataPlaneMapped) const
	{
		return std::make_tuple(s_addr, TeidDataPlaneMapped);
	}

	TeidRecordKeyMap m_PgwSequenceControlPlaneRecordKeyMap; //key is MakeSequenceControlPlaneRecordKey(record.Pgw.c_plane_addr.sin_addr.s_addr, record.PdpCreatePgwSequenceNumber)

	inline SourceTeid MakeSequenceControlPlaneRecordKey(const in_addr_t s_addr, const uint32_t Sequence) const
	{
		//TBD only use MakeTeidDataPlaneRecordKey or keep the alias
		return std::make_tuple(s_addr, Sequence);
	}

	unsigned int RemoveDataPlaneReferences(const PdpRecord& pdpRecord);

	unsigned int m_PdpRecordMaintenanceInterval;
	unsigned int m_PdpRecordActivityTimeout;

	TimerWheelManager m_PdpTimerWheelManager;

public:

	PdpManager();
	virtual ~PdpManager();

	void Advise(PgwManagerInterface* PgwManagerInterface) { m_ThePgwManagerInterface = PgwManagerInterface; }

	void TimerMaintenance(time_t Now);

	void UpdateConfigValues(Configuration& Config);

	//from PdpManagerInterface
	virtual void AddOrUpdatePdpRecord(uint32_t PdpRecordKey, const PdpRecord& Record);
	virtual bool GetGpduDestinationAddress(uint32_t& teid, ProxyDirection Direction, struct sockaddr_in& Destination, struct sockaddr_in& Source, bool &ReceivedEchoResponseFromPgw, uint32_t GpduSize);
	virtual bool SetErrorIndicationOnPdp(uint32_t& Teid, int /*Direction*/, struct sockaddr_in& Source);
	virtual void SetPgwAddressIfNeeded(PdpRecord& pdpRecord);
	virtual void SetPgwAddressForTeidNsapiIfNeeded(uint32_t Teid, uint8_t LinkedNSAPI, PdpRecord& pdpRecord, struct sockaddr_in& Source);
	virtual bool GetOrInitPdpRecord(PdpRecord& pdpRecord, uint32_t& PdpRecordKey, unsigned char* buffer, ssize_t rcvsize, struct sockaddr_in fromaddr, uint8_t GtpMsgType, ProxyDirection Direction, uint32_t PacketTeid);
	virtual unsigned int AddDataPlaneReferencesIfNeeded(const PdpRecord& pdpRecord, const PdpRecord::DataPlaneTeids* DataPlaneTeids);
	virtual void AddPgwCreateSessionRequestReference(const PdpRecord& Record, struct sockaddr_in Dest);
	//from PdpManagerTimerInterface
	virtual void StopTimerOnRecord(void*& RecordTimerId);
	virtual void StartTimerOnRecord(void*& RecordTimerId, const uint32_t RecordKeyValue, time_t DueTime);

	//VTY functions
	void ShowPdpRecords(vty_client* sh) const;
	void PdpRecordsToLog(vty_client* sh) const;
	void ShowPdpRecordsByImsi(vty_client* sh, const std::string& Imsi) const;
	void ChangePdpRecordsRemoveByKeyValue(vty_client* sh, const std::string& KeyValue);
	void ChangePdpRecordsRemoveByImsi(vty_client* sh, const std::string& Imsi);
};


#include "vty.h"
/* vty show PDP Records*/
extern vty_cmd_match show_pdp_records_mt();
extern void show_pdp_records_f(vty_cmd_match* m, vty_client* sh, void* arg);

/* vty show PDP Records to logfile*/
extern vty_cmd_match show_pdp_records_tolog_mt();
extern void show_pdp_records_tolog_f(vty_cmd_match* m, vty_client* sh, void* arg);

/* vty show PDP Records*/
extern vty_cmd_match show_pdp_records_imsi_mt();
extern void show_pdp_records_imsi_f(vty_cmd_match* m, vty_client* sh, void* arg);

/* vty change pdp record remove status*/
extern vty_cmd_match change_pdp_records_remove_key_mt();
extern void change_pdp_records_remove_key_f(vty_cmd_match* m, vty_client* sh, void* arg);

/* vty change pdp record remove status*/
extern vty_cmd_match change_pdp_records_remove_imsi_mt();
extern void change_pdp_records_remove_imsi_f(vty_cmd_match* m, vty_client* sh, void* arg);


#endif /* INC_PDPMANAGER_H_ */
