#!/bin/bash
#Relevant for AWS EC2 Amazon Linux AMI release 2017.03
#for working std::regex support:
sudo yum -y install gcc72 gcc72-c++
#for structured config files support:
sudo yum -y install libconfig
#for build platform install libconfig-devel (which also yields libconfig installation)
#NOTE: the available version on AMI release 2017.03 is libconfig-devel-1.3.2-1.1.2.amzn1.x86_64.rpm which is from July 2011
#sudo yum -y install libconfig-devel

#Relevant for Ubuntu Xenial 16.04 and higher
#sudo apt-get install libconfig++-dev