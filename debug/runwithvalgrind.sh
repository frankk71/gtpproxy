#!/bin/bash
/usr/bin/valgrind \
    -v \
    --error-limit=no \
    --num-callers=40 \
    --fullpath-after= \
    --track-origins=yes \
    --log-file=./valgrind.log \
    --leak-check=full \
    --show-reachable=yes \
    --vex-iropt-register-updates=allregs-at-mem-access \
    ./gtpproxy  -c ../config/gtpproxy.cfg
