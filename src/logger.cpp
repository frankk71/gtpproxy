
#include "logger.h"

#include "defs.h"
#include "configuration.h"

#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <errno.h>
#include <ctype.h>
#include <libgen.h>

#ifdef LOCK_ACCESS_WITH_PTHREAD_MUTEX
#include <pthread.h>
static pthread_mutex_t _rti_app_log_lock_ = PTHREAD_MUTEX_INITIALIZER;
#endif

unsigned char LogCategory::logCategory = 0xFF;

FILE* Logger::logFp = NULL;
unsigned char Logger::dbgLevel = 0xFF;
static const char* defaultLogFileName = "gtpproxy.log";
static const char* defaultLogFilePath = "./logs";
char Logger::logFilePathName[PATH_MAX] = {0};
char* Logger::logFileName = (char*)defaultLogFileName;
char* Logger::logFilePath = (char*)defaultLogFilePath;
unsigned int Logger::logMaxLogFileSize = MAX_DEBUG_SIZE_DEFAULT;//in bytes
unsigned int Logger::logMaxLogFileTime = MAX_DEBUG_TIME_DEFAULT;//in seconds
bool Logger::externalLogCycleCall = false;

time_t cur_log_file_create_time;
unsigned long int cur_file_size = 0;
unsigned int cur_seq_no = 0;

//Helper for setLogfilePath - create directory recursively
int mkpath(char *dir, mode_t mode)
{
    struct stat sb;

    if (!dir) {
        errno = EINVAL;
        return 1;
    }

    if (!stat(dir, &sb))
        return 0;

    mkpath(dirname(strdupa(dir)), mode);//strdupa() uses alloca() (storage on stack frame) which is free'd when function returns

    return mkdir(dir, mode);
}

void Logger::makeLogFilePathName()
{
	mkpath(logFilePath,0755);//read+execute access for all
	snprintf(logFilePathName,PATH_MAX-1,"%s/%s",logFilePath, logFileName);
	logFilePathName[PATH_MAX - 1]='\0';
}

/*---
 * Set the log file name for the logger
 * ---*/
void Logger::setLogfileName(const char* logfileName)
{
	if(logFp == NULL)
	{
		logFileName = (char*)logfileName;
		printf("Set logFileName to %s\n", logFileName);
		makeLogFilePathName();
	}
}

/*---
 * Set the log file path for the logger
 * ---*/
void Logger::setLogfilePath(const char* logfilePath)
{
	if(logFp == NULL)
	{
		logFilePath = (char*)logfilePath;
		printf("Set logFilePath to %s\n", logFilePath);
		makeLogFilePathName();
	}
}

void  Logger::setLogfileMaxSize(const unsigned int maxSize)
{
	logMaxLogFileSize = maxSize;
	printf("Set logMaxLogFileSize to %d\n", logMaxLogFileSize);
}

unsigned int Logger::getLogfileMaxSize()
{
	return logMaxLogFileSize;
}

void  Logger::setLogfileMaxTime(const unsigned int maxTime)
{
	logMaxLogFileTime = maxTime;
	printf("Set logMaxLogFileTime to %d\n", logMaxLogFileTime);
}

unsigned int Logger::getLogfileMaxTime()
{
	return logMaxLogFileTime;
}


void  Logger::setExternalLogCycleCall(bool On)
{
	externalLogCycleCall = On;
}

/*---
 * Set the debug level for the logger
 * ---*/
void Logger::setDbgLevel(unsigned char dbglevel)
{
    dbgLevel = dbglevel;
}

/*---
 * Get the debug level for the logger
 * ---*/
unsigned char Logger::getDbgLevel()
{
    return(dbgLevel);
}

/*---
 * Prints the filename, linenumber and type of debug. Used from MACROS defined in Logger.h
 * ---*/
int Logger::createLogFileIfNeeded()
{
	if(logFp == NULL)
	{
		cur_log_file_create_time = time(NULL);
		logFp = getNewLogFile();
		if(logFp == NULL)
		{
			fprintf(stderr, "Error opening file %s, error(%s)\n", logFilePathName, strerror(errno));
			return(0);
		}
		cur_file_size = 0;
	}
	return(1);
}

int Logger::dbgFunc(const char* fileName, int line, const char *type)
{
	if(createLogFileIfNeeded())
    {
#ifdef LOCK_ACCESS_WITH_PTHREAD_MUTEX
		fprintf(logFp,"%s|thread:%x|%s|%s|(%d)|", nowTime(), (int)pthread_self(), type, fileName, line);
#else
    	fprintf(logFp,"%s|%s|%s|(%d)|", nowTime(), type, fileName, line);
#endif
    	//fflush(logFp);
    }
	else
	{
		printf("%s (%d)", fileName, line);
	}
    return(1);
}

/*---
 * Prints the application debug information. Used from MACROS defined in Logger.h
 * ---*/
int Logger::debugPrint(const char* str,...)
{
    va_list arg;
    char    dbgstr[1024];

    va_start(arg, str);
    vsnprintf(dbgstr,1024,str,arg);
    dbgstr[1024 - 1] = '\0';

	if(createLogFileIfNeeded())
    {
	    cur_file_size += fprintf(logFp, "%s", dbgstr);
	    fflush(logFp);
    }
	else
	{
        printf("%s", dbgstr);
	}
    va_end(arg);

    if(!externalLogCycleCall)
    	cycleLogFileIfNeeded(time(NULL));

    return(1);
}

int Logger::cycleLogFileIfNeeded(const time_t cur_time)
{
    if(cur_file_size > logMaxLogFileSize || (cur_time - logMaxLogFileTime > cur_log_file_create_time))
    {
    	//reset sequence when the hour changes. See getNewLogFile() naming of the cycled file
   		if(!(cur_time / 3600 == cur_log_file_create_time / 3600))
			cur_seq_no = 0;
   		else
   			cur_seq_no++;

        fclose(logFp);
        logFp = NULL;

        return createLogFileIfNeeded();
    }
    return(1);
}

/*---
 * Get a new file pointer for new log file
 * ---*/
FILE* Logger::getNewLogFile()
{
    char cycledFileNamePath[PATH_MAX];
    struct tm file_tm;
    const time_t file_t = cur_log_file_create_time;
	localtime_r(&file_t, &file_tm);

    snprintf(cycledFileNamePath, PATH_MAX-1, "%s/%s.%04d%02d%02d%02d.%03d", logFilePath, logFileName, (file_tm.tm_year + 1900), (file_tm.tm_mon +1),
    		file_tm.tm_mday, file_tm.tm_hour, cur_seq_no);

    cycledFileNamePath[PATH_MAX - 1]='\0';

    if(access(logFilePathName, F_OK) != -1)
    {
    	rename(logFilePathName, cycledFileNamePath);
    }
	if(logFp == NULL)
	{
		makeLogFilePathName();
	}
    return(fopen(logFilePathName, "a"));
}

/*---
 * Gets current time to print in the logs
 * ---*/
char* Logger::nowTime()
{
    struct tm*  ptr_tm, thrSafeTm;
    static char str_time[50];
    long        the_time;

    /* Get the current time. */
    (void) time(&the_time);

    ptr_tm = localtime_r(&the_time, &thrSafeTm);

    (void) strftime(str_time, sizeof(str_time), "%d %b %y %H:%M:%S", ptr_tm);
    return(str_time);
}

/*---
 * Print buffer of given length in the log files
 * ---*/
int Logger::printHexDump(void *ptr, int buflen)
{
    char tempBuf[128];
    char *buf = (char*)ptr;
    char *tempPtr;
    int i, j;

    if(buflen)
    {
        if(ptr == NULL)
        {
            ERROR(("len = %d buff = 0x00\n", buflen));
            return 0;
        }
#if 0
        PRINT(("-------------------------------------------------------------------------\n"));
        PRINT(("Offset               Hex Dump                             Ascii Dump\n"));
        PRINT(("------  -----------------------------------------------  ----------------\n"));
#endif
        for (i=0; i<buflen; i+=16)
        {
            tempPtr = tempBuf;
            memset(tempPtr, 0, sizeof(tempBuf));
            tempPtr += sprintf(tempPtr, "%06x  ", i);
            for (j=0; j<16; j++)
            {
                if (i+j < buflen)
                    tempPtr += sprintf(tempPtr, "%02x ", (unsigned)(unsigned char)buf[i+j]);
                else
                    tempPtr += sprintf(tempPtr, "   ");
            }
            tempPtr += sprintf(tempPtr, " ");
            for (j=0; j<16; j++)
            {
                if (i+j < buflen)
                    tempPtr += sprintf(tempPtr, "%c", isprint(buf[i+j]) ? buf[i+j] : '.');
            }
            PRINT(("%s\n",tempBuf));
        }
#if 0
        PRINT(("-------------------------------------------------------------------------\n"));
#endif
    }
    return 1;
}

/*---
 * Lock to synchronize the log files.
 * ---*/
int Getlock()
{
#ifdef LOCK_ACCESS_WITH_PTHREAD_MUTEX
    pthread_mutex_lock(&_rti_app_log_lock_);
#endif
    return 1;
}

/*---
 * UnLock to synchronize the log files.
 * ---*/
int Unlock()
{
#ifdef LOCK_ACCESS_WITH_PTHREAD_MUTEX
    pthread_mutex_unlock(&_rti_app_log_lock_);
#endif
    return 1;
}



/* vty change loglevel*/
vty_cmd_match change_loglevel_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("change", "Allows to change process options"));
  m.nodes.push_back(new node_fixedstring("loglevel", "Change the loglevel"));
  m.nodes.push_back(new node_string              );
  return m;
}

void change_loglevel_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
	unsigned char currentloglevel = Logger::getDbgLevel();

	sh->Printf("Current log level %u - 0x%02X ->", currentloglevel, currentloglevel);

	sh->Printf(" Error:%s Mask:0x%02x",LOG_LEVEL_ERROR ? "On" : "Off", MSG_DBG_1);
	sh->Printf(" Warning:%s Mask:0x%02x",LOG_LEVEL_ERROR ? "On" : "Off", MSG_DBG_2);
	sh->Printf(" Info:%s Mask:0x%02x",LOG_LEVEL_INFO ? "On" : "Off", MSG_DBG_3);
	sh->Printf(" Debug:%s Mask:0x%02x",LOG_LEVEL_DEBUG ? "On" : "Off", MSG_DBG_4);
	sh->Printf(" Dump:%s Mask:0x%02x\r\n",LOG_LEVEL_HEXDUMP ? "On" : "Off", MSG_DBG_7);

	if(m->nodes.size() > 2)
	{
		std::string parameter = m->nodes[2]->get();
		if(parameter.find_first_not_of("0123456789") == std::string::npos)
		{
			unsigned char loglevel = (unsigned char)atoi(parameter.c_str());
			sh->Printf("Changed log level to %u - 0x%02X\r\n", loglevel, loglevel);
			Logger::setDbgLevel(loglevel);
			/* Writing to config will remove all comments
			Configuration Config;
			Config.writeLoggingConfig(Logger::getDbgLevel(),LogCategory::logCategory);
			*/
		}
		else
			sh->Printf("To change the level input numeric digits only\r\n");

		sh->Printf(" Error:%s",LOG_LEVEL_ERROR ? "On" : "Off");
		sh->Printf(" Warning:%s",LOG_LEVEL_WARN ? "On" : "Off");
		sh->Printf(" Info:%s",LOG_LEVEL_INFO ? "On" : "Off");
		sh->Printf(" Debug:%s",LOG_LEVEL_DEBUG ? "On" : "Off");
		sh->Printf(" Dump:%s\r\n",LOG_LEVEL_HEXDUMP ? "On" : "Off");
	}
}

/* vty change loglevel*/
vty_cmd_match change_logcategory_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("change", "Allows to change process options"));
  m.nodes.push_back(new node_fixedstring("logcategory", "Change the log category"));
  m.nodes.push_back(new node_string              );
  return m;
}

void change_logcategory_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
	unsigned char currentlogcategory = LogCategory::logCategory;

	sh->Printf("Current log category %u - 0x%02X ->", currentlogcategory, currentlogcategory);

	sh->Printf(" GTP-C:%s Mask:0x%02x",currentlogcategory & LOG_CATEGORY_1 ? "On" : "Off", LOG_CATEGORY_1);
	sh->Printf(" GTP-U:%s Mask:0x%02x",currentlogcategory & LOG_CATEGORY_2 ? "On" : "Off", LOG_CATEGORY_2);
	sh->Printf(" GTP-ECHO:%s Mask:0x%02x",currentlogcategory & LOG_CATEGORY_3 ? "On" : "Off", LOG_CATEGORY_3);
	sh->Printf(" GTP-CHECK:%s Mask:0x%02x\r\n",currentlogcategory & LOG_CATEGORY_4 ? "On" : "Off", LOG_CATEGORY_4);

	if(m->nodes.size() > 2)
	{
		std::string parameter = m->nodes[2]->get();
		if(parameter.find_first_not_of("0123456789") == std::string::npos)
		{
			unsigned char logcategory = (unsigned char)atoi(parameter.c_str());
			sh->Printf("Changed log category to %u - 0x%02X\r\n", logcategory, logcategory);
			LogCategory::logCategory = logcategory;
			/* Writing to config will remove all comments
			Configuration Config;
			Config.writeLoggingConfig(Logger::getDbgLevel(),LogCategory::logCategory);
			*/
		}
		else
			sh->Printf("To change the category input numeric digits only\r\n");

		sh->Printf(" GTP-C:%s", LOG_CATEGORY_GTP_C ? "On" : "Off");
		sh->Printf(" GTP-U:%s", LOG_CATEGORY_GTP_U ? "On" : "Off");
		sh->Printf(" GTP-ECHO:%s", LOG_CATEGORY_GTP_ECHO ? "On" : "Off");
		sh->Printf(" GTP-CHECK:%s\r\n", LOG_CATEGORY_GTP_CHECK ? "On" : "Off");
	}
}
/* End of file */

