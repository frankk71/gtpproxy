/*
 * pdpmanager.cpp
 *
 *  Created on: Feb 25, 2019
 *      Author: JpU FK
 */

#include "pdpmanager.h"
#include "logger.h"
#include "defs.h"
#include "globals.h"
#include "gtppacket.h"
#include "utils.h"

#include <stdlib.h>

using namespace gtppacket;

/////////////////////////////////////////////////////////////////////////////////
///////////////////////////// PDP Record Manager ////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

PdpManager::PdpManager()
{
	m_ThePgwManagerInterface = NULL;
	m_LastPdpRecordArrayMaintenanceTime = 0;

	m_PdpKeyNumberGenerator = (uint32_t)rand();
	m_PdpSequenceNumberGenerator = (uint16_t)rand();

	m_PdpRecordMaintenanceInterval = 30;//in seconds
	m_PdpRecordActivityTimeout = 2000;//in seconds
}

PdpManager::~PdpManager()
{
}


void PdpManager::UpdateConfigValues(Configuration& Config)
{
	Config.readIntConfig(CONFIG_TIMER_GROUP,"PdpRecordMaintenanceInterval",(int&)m_PdpRecordMaintenanceInterval);
	Config.readIntConfig(CONFIG_TIMER_GROUP,"PdpRecordActivityTimeout",(int&)m_PdpRecordActivityTimeout);
}

unsigned int PdpManager::RemoveDataPlaneReferences(const PdpRecord& pdpRecord)
{
	unsigned int SgwItemsRemoved = 0, PgwItemsRemoved = 0;

	for(PdpRecord::TeidMapTypeConstIter Iter = pdpRecord.DataPlaneContextMap.begin(), EndIter = pdpRecord.DataPlaneContextMap.end();Iter!=EndIter;Iter++)
	{
		const PdpRecord::DataPlaneTeids& DataPlaneTeids = Iter->second;
#ifdef	NO_SOURCE_CHECK
		SourceTeid SgwTeidKey = MakeTeidDataPlaneRecordKey(0,DataPlaneTeids.PgwTeidDataPlaneMapped);
#else
		SourceTeid SgwTeidKey = MakeTeidDataPlaneRecordKey(pdpRecord.Sgw.u_plane_addr.sin_addr.s_addr, DataPlaneTeids.PgwTeidDataPlaneMapped);
#endif

		SgwItemsRemoved = m_SgwTeidDataPlaneRecordKeyMap.erase(SgwTeidKey);
		if(SgwItemsRemoved)
		{
			if(LOG_CATEGORY_GTP_C)DEBUG(("Removed %u S-GW DataPlane References for PDP Record %u\n", SgwItemsRemoved, pdpRecord.KeyValue));
		}
		else
		{
			if(LOG_CATEGORY_GTP_C)DEBUG(("No S-GW DataPlane References found for PDP Record %u\n",pdpRecord.KeyValue));
		}

#ifdef	NO_SOURCE_CHECK
		SourceTeid PgwTeidKey = MakeTeidDataPlaneRecordKey(0,pdpRecord.SgwTeidDataPlaneMapped);
#else
		SourceTeid PgwTeidKey = MakeTeidDataPlaneRecordKey(pdpRecord.Pgw.u_plane_addr.sin_addr.s_addr, DataPlaneTeids.SgwTeidDataPlaneMapped);
#endif

		PgwItemsRemoved = m_PgwTeidDataPlaneRecordKeyMap.erase(PgwTeidKey);
		if(PgwItemsRemoved)
		{
			if(LOG_CATEGORY_GTP_C)DEBUG(("Removed %u P-GW DataPlane References for PDP Record %u\n", PgwItemsRemoved, pdpRecord.KeyValue));
		}
		else
		{
			if(LOG_CATEGORY_GTP_C)DEBUG(("No P-GW DataPlane References found for PDP Record %u\n",pdpRecord.KeyValue));
		}
	}
	return SgwItemsRemoved + PgwItemsRemoved;
}

void PdpManager::StopTimerOnRecord(void*& RecordTimerId)
{
	if(RecordTimerId != NULL)
		m_PdpTimerWheelManager.CancelTimer(RecordTimerId);
}

void PdpManager::StartTimerOnRecord(void*& RecordTimerId, const uint32_t RecordKeyValue, time_t DueTime)
{
	if(RecordTimerId != NULL)
		m_PdpTimerWheelManager.RescheduleTimer(RecordTimerId, DueTime);
	else
		RecordTimerId = m_PdpTimerWheelManager.CreateTimer(RecordKeyValue, DueTime);
}

void PdpManager::TimerMaintenance(time_t Now)
{
	ExpiredTimersType ExpiredTimers;
	m_PdpTimerWheelManager.AdvanceTimerWheel(Now, ExpiredTimers);

	for(unsigned int i=0;i<ExpiredTimers.size();i++)
	{
		uint32_t Key;
		void* TimerId;
		std::tie(TimerId, Key) = ExpiredTimers[i];
		INFO(("TimerId:%p of PDP Record Key:%u expired\n",TimerId, Key));

		PdpRecords::iterator Iter = m_PdpRecords.find(Key);
		if(Iter != m_PdpRecords.end())
		{
			if(Iter->second.TimerId == TimerId)
			{
				Iter->second.TimerId = NULL;
				INFO(("Removed expired timer from PDP Record:%u\n", Key));

				time_t LastActivity = std::max(Iter->second.GetLastSgwActivityTime(),Iter->second.GetLastPgwActivityTime());
				if(Iter->second.RemoveMeTime != 0 && Now > Iter->second.RemoveMeTime)
				{
					INFO(("Removing deleted PDP Record %s\n",Iter->second.ToString().c_str()));

					m_ThePgwManagerInterface->OnPdpRecordRemoved(Iter->second.PacketGateway);

					RemoveDataPlaneReferences(Iter->second);
					m_PdpRecords.erase(Iter);
				}
				else if(LastActivity != 0 && m_PdpRecordActivityTimeout != 0 && Now > (LastActivity + m_PdpRecordActivityTimeout))
				{
					INFO(("Removing inactive PDP Record %s - Last activity %d seconds ago\n",Iter->second.ToString().c_str(), Now - LastActivity));

					m_ThePgwManagerInterface->OnPdpRecordRemoved(Iter->second.PacketGateway);

					RemoveDataPlaneReferences(Iter->second);
					m_PdpRecords.erase(Iter);
				}
			}
			else
				WARN(("Failed to match PDP Record:%u TimerId:%p to expired timer\n", Key, Iter->second.TimerId));
		}
		else
		{
			WARN(("Failed to find PDP Record:%u for expired timer\n", Key));
		}
	}

	if(m_PdpRecordMaintenanceInterval && Now - m_LastPdpRecordArrayMaintenanceTime > m_PdpRecordMaintenanceInterval)
	{
		//TODO remove after ExpiredTimers debug
		for(PdpRecords::iterator Iter = m_PdpRecords.begin(); Iter != m_PdpRecords.end();)
		{
			//TODO: inefficient on large m_PdpRecords size; Implement timer wheel
			time_t LastActivity = std::max(Iter->second.GetLastSgwActivityTime(),Iter->second.GetLastPgwActivityTime());
			if(Iter->second.RemoveMeTime != 0 && Now > Iter->second.RemoveMeTime)
			{
				INFO(("Removing deleted PDP Record %s\n",Iter->second.ToString().c_str()));

				m_ThePgwManagerInterface->OnPdpRecordRemoved(Iter->second.PacketGateway);

				RemoveDataPlaneReferences(Iter->second);
				Iter = m_PdpRecords.erase(Iter);
			}
			else if(LastActivity != 0 && m_PdpRecordActivityTimeout != 0 && Now > (LastActivity + m_PdpRecordActivityTimeout))
			{
				INFO(("Removing inactive PDP Record %s - Last activity %d seconds ago\n",Iter->second.ToString().c_str(), Now - LastActivity));

				m_ThePgwManagerInterface->OnPdpRecordRemoved(Iter->second.PacketGateway);

				RemoveDataPlaneReferences(Iter->second);
				Iter = m_PdpRecords.erase(Iter);
			}
			else
			{
				Iter++;
			}
		}

		for(TeidRecordKeyMapIter SgwIter = m_SgwTeidDataPlaneRecordKeyMap.begin();SgwIter != m_SgwTeidDataPlaneRecordKeyMap.end();)
		{
			if(m_PdpRecords.find(SgwIter->second) == m_PdpRecords.end())
			{
				//TODO debug why we get here
				uint32_t sourceip,teid;
				std::tie(sourceip,teid) = SgwIter->first;

				INFO(("Removing SGW:%s TEID:0x%08x entry of PDPRecord:%u\r\n", GetIPv4AddressStr(sourceip), teid, SgwIter->second));
				SgwIter = m_SgwTeidDataPlaneRecordKeyMap.erase(SgwIter);
			}
			else
			{
				SgwIter++;
			}
		}

		for(TeidRecordKeyMapIter PgwIter = m_PgwTeidDataPlaneRecordKeyMap.begin();PgwIter != m_PgwTeidDataPlaneRecordKeyMap.end();)
		{
			if(m_PdpRecords.find(PgwIter->second) == m_PdpRecords.end())
			{
				//TODO debug why we get here
				uint32_t sourceip,teid;
				std::tie(sourceip,teid) = PgwIter->first;

				INFO(("Removing PGW:%s TEID:0x%08x entry of PDPRecord:%u\r\n", GetIPv4AddressStr(sourceip), teid, PgwIter->second));
				PgwIter = m_PgwTeidDataPlaneRecordKeyMap.erase(PgwIter);
			}
			else
			{
				PgwIter++;
			}
		}

		for(TeidRecordKeyMapIter PgwIter = m_PgwSequenceControlPlaneRecordKeyMap.begin();PgwIter != m_PgwSequenceControlPlaneRecordKeyMap.end();)
		{
			if(m_PdpRecords.find(PgwIter->second) == m_PdpRecords.end())
			{
				//TODO debug why we get here
				uint32_t sourceip,createseq;
				std::tie(sourceip,createseq) = PgwIter->first;

				INFO(("Removing PGW:%s CreateSeq:0x%08x entry of PDPRecord:%u\r\n", GetIPv4AddressStr(sourceip), createseq, PgwIter->second));
				PgwIter = m_PgwSequenceControlPlaneRecordKeyMap.erase(PgwIter);
			}
			else
			{
				PgwIter++;
			}
		}

		m_LastPdpRecordArrayMaintenanceTime = Now;
	}
}

//from PdpManagerInterface
/*virtual*/ unsigned int PdpManager::AddDataPlaneReferencesIfNeeded(const PdpRecord& pdpRecord, const PdpRecord::DataPlaneTeids* DataPlaneTeids)
{
	unsigned int SgwItemsAdded = 0, PgwItemsAdded = 0;

	if(DataPlaneTeids != NULL && DataPlaneTeids->PgwTeidDataPlaneMapped != (uint32_t)-1)
	{
#ifdef	NO_SOURCE_CHECK
		SourceTeid SgwTeidKey = MakeTeidDataPlaneRecordKey(0, DataPlaneTeids->PgwTeidDataPlaneMapped);
#else
		SourceTeid SgwTeidKey = MakeTeidDataPlaneRecordKey(pdpRecord.Sgw.u_plane_addr.sin_addr.s_addr, DataPlaneTeids->PgwTeidDataPlaneMapped);
#endif
		TeidRecordKeyMapIter Iter = m_SgwTeidDataPlaneRecordKeyMap.find(SgwTeidKey);
		if(Iter == m_SgwTeidDataPlaneRecordKeyMap.end())
		{
			m_SgwTeidDataPlaneRecordKeyMap.insert(std::pair<SourceTeid,uint32_t/*PdpRecord Key*/>(SgwTeidKey,pdpRecord.KeyValue));
			if(LOG_CATEGORY_GTP_C)DEBUG(("Added S-GW DataPlane References for PDP Record %u\n", pdpRecord.KeyValue));
			SgwItemsAdded++;
		}
		else
		{
#ifdef	NO_SOURCE_CHECK
			if(LOG_CATEGORY_GTP_C)WARN(("S-GW DataPlane References for PDP Record %u already has an entry P-GW mapped TEID 0x%08x\n", pdpRecord.KeyValue, DataPlaneTeids->PgwTeidDataPlaneMapped));
#else
			if(LOG_CATEGORY_GTP_C)DEBUG(("S-GW DataPlane References for PDP Record %u already has an entry P-GW mapped TEID 0x%08x\n", pdpRecord.KeyValue, DataPlaneTeids->PgwTeidDataPlaneMapped));
#endif
		}
	}

	if(DataPlaneTeids != NULL && DataPlaneTeids->SgwTeidDataPlaneMapped != (uint32_t)-1)
	{
#ifdef	NO_SOURCE_CHECK
		SourceTeid PgwTeidKey = MakeTeidDataPlaneRecordKey(0, DataPlaneTeids->SgwTeidDataPlaneMapped);
#else
		SourceTeid PgwTeidKey = MakeTeidDataPlaneRecordKey(pdpRecord.Pgw.u_plane_addr.sin_addr.s_addr, DataPlaneTeids->SgwTeidDataPlaneMapped);
#endif
		TeidRecordKeyMapIter Iter = m_PgwTeidDataPlaneRecordKeyMap.find(PgwTeidKey);
		if(Iter == m_PgwTeidDataPlaneRecordKeyMap.end())
		{
			m_PgwTeidDataPlaneRecordKeyMap.insert(std::pair<SourceTeid,uint32_t/*PdpRecord Key*/>(PgwTeidKey,pdpRecord.KeyValue));
			if(LOG_CATEGORY_GTP_C)DEBUG(("Added P-GW DataPlane References for PDP Record %u\n", pdpRecord.KeyValue));
			PgwItemsAdded++;
		}
		else
		{
#ifdef	NO_SOURCE_CHECK
			if(LOG_CATEGORY_GTP_C)WARN(("P-GW DataPlane References for PDP Record %u already has an entry S-GW mapped TEID 0x%08x\n", pdpRecord.KeyValue, DataPlaneTeids->SgwTeidDataPlaneMapped));
#else
			if(LOG_CATEGORY_GTP_C)DEBUG(("P-GW DataPlane References for PDP Record %u already has an entry S-GW mapped TEID 0x%08x\n", pdpRecord.KeyValue, DataPlaneTeids->SgwTeidDataPlaneMapped));
#endif
		}
	}

	return SgwItemsAdded + PgwItemsAdded;
}

//from PdpManagerInterface
/*virtual*/ void PdpManager::AddOrUpdatePdpRecord(uint32_t PdpRecordKey, const PdpRecord& Record)
{
	PdpRecords::iterator Iter = m_PdpRecords.find(PdpRecordKey);
	if(Iter != m_PdpRecords.end())
	{
		Iter->second = Record;
		if(LOG_CATEGORY_GTP_C)DEBUG(("Updated PDP Record %s\n",Iter->second.ToString().c_str()));
		return;
	}

	m_PdpRecords.insert(std::pair<uint32_t,PdpRecord>(PdpRecordKey, Record));
	if(LOG_CATEGORY_GTP_C)INFO(("Added PDP Record %s\n", Record.ToString().c_str()));
}

//from PdpManagerInterface
/*virtual*/ bool PdpManager::GetGpduDestinationAddress(uint32_t& teid, ProxyDirection Direction, struct sockaddr_in& Destination, struct sockaddr_in& Source, bool &ReceivedEchoResponseFromPgw, uint32_t GpduSize)
{
	if(Direction == ProxyDirection::TO_PGW_REMOTE)
	{
		if(!GetGpduPgwDstAddrForTeidDataPlane(teid, Destination, Source, ReceivedEchoResponseFromPgw, GpduSize))
		{
			//TODO DEBUG: remove the next lines after debug else we have a log file bomber when the there is no PDP context on record
			if(LOG_CATEGORY_GTP_U)ERROR(("GTP-U failed to find PDP Record for PGW TEID 0x%08x Source:%s\n", teid, inet_ntoa(Source.sin_addr)));
		}
	}
	else if(Direction == ProxyDirection::TO_SGW_REMOTE)
	{
		if(!GetGpduSgwDstAddrForTeidDataPlane(teid, Destination, Source, GpduSize))
		{
			//TODO DEBUG: remove the next lines after debug else we have a log file bomber when the there is no PDP context on record
			if(LOG_CATEGORY_GTP_U)ERROR(("GTP-U failed to find PDP Record for SGW TEID 0x%08x Source:%s\n", teid, inet_ntoa(Source.sin_addr)));
		}
	}
	return (Destination.sin_addr.s_addr != 0);
}

//from PdpManagerInterface
/*virtual*/ bool PdpManager::SetErrorIndicationOnPdp(uint32_t& Teid, int /*Direction*/, struct sockaddr_in& Source)
{
	//TODO dedicated map for fast lookup; TEID+source s_addr in tuple as key
	//TBD need for PDP record reference for activity and data accounting
	for(PdpRecords::iterator Iter = m_PdpRecords.begin(), EndIter = m_PdpRecords.end();Iter!=EndIter;Iter++)
	{
		PdpRecord::DataPlaneTeids* DataPlaneTeids = Iter->second.GetDataPlaneTeidsForDplaneTeid(Teid);

		if(DataPlaneTeids != NULL)
		{
#ifdef	NO_SOURCE_CHECK
			if(DataPlaneTeids->SgwTeidDataPlane == Teid)
#else
			if(Source.sin_addr.s_addr == Iter->second.Sgw.u_plane_addr.sin_addr.s_addr && DataPlaneTeids->SgwTeidDataPlane == Teid)
#endif
			{
				Teid = DataPlaneTeids->SgwTeidDataPlaneMapped;
				DataPlaneTeids->SgwTeidDataPlaneError++;
				return true;
			}

#ifdef	NO_SOURCE_CHECK
			if(Iter->second.PgwTeidDataPlane == Teid)
#else
			if(Source.sin_addr.s_addr == Iter->second.Pgw.u_plane_addr.sin_addr.s_addr && DataPlaneTeids->PgwTeidDataPlane == Teid)
#endif
			{
				Teid = DataPlaneTeids->PgwTeidDataPlaneMapped;
				DataPlaneTeids->PgwTeidDataPlaneError++;
				return true;
			}
		}
	}
	if(LOG_CATEGORY_GTP_U)WARN(("GTP-U failed to find D-TEID set on PDP Record for TEID 0x%08x Source:%s\n", Teid, inet_ntoa(Source.sin_addr)));
	return false;
}

//from PdpManagerInterface
/*virtual*/ void PdpManager::SetPgwAddressIfNeeded(PdpRecord& pdpRecord)
{
	//Lookup P-GW address based on IMSI or APN. IMSI can be 0 i.e. on emergency access or secondary PDP context
	if(pdpRecord.Pgw.c_plane_addr.sin_addr.s_addr == 0 || pdpRecord.Pgw.u_plane_addr.sin_addr.s_addr == 0)
	{
		std::string ImsiString;

		if(pdpRecord.IMSI != 0)
		{
			uint64_t ImsiNetworkOrder = htobe64(pdpRecord.IMSI);
			HexDumpToStr((unsigned char*)&ImsiNetworkOrder,sizeof(uint64_t),ImsiString,true/*reversenibble*/);
			ImsiString.erase(ImsiString.find_last_not_of(' ')+1);
		}

		m_ThePgwManagerInterface->AssignPgw(ImsiString, pdpRecord.APN, pdpRecord.Pgw, pdpRecord.PacketGateway);
	}
}

//from PdpManagerInterface
/*virtual*/ void PdpManager::SetPgwAddressForTeidNsapiIfNeeded(uint32_t Teid, uint8_t LinkedNSAPI, PdpRecord& pdpRecord, struct sockaddr_in& Source)
{
	//TODO prime candidate for debug

	//Secondary PDP context: Find P-GW address based on primary PDP context data TEID value presented by the S-GW
	//TBD what TEID of the P-GW would that be GTPV1_IE_TEI_DI or GTPV1_IE_TEI_C ? The S-GW should not provide GTPV1_IE_TEI_C IE but a new GTPV1_IE_TEI_DI
	if(pdpRecord.Pgw.c_plane_addr.sin_addr.s_addr == 0 || pdpRecord.Pgw.u_plane_addr.sin_addr.s_addr == 0)
	{
		//Initialize the pdpRecord with the default P-GW details
		m_ThePgwManagerInterface->AssignPgw("", "", pdpRecord.Pgw, pdpRecord.PacketGateway);

		PdpRecord PrimaryPdpRecord;
		if(GetPdpRecordSgwTeidControlPlane(Teid, PrimaryPdpRecord, Source))
		{
			//Inherit the P-GW details
			pdpRecord.Pgw = PrimaryPdpRecord.Pgw;
			pdpRecord.PacketGateway = PrimaryPdpRecord.PacketGateway;
		}

		//TBD: update the PrimaryPdpRecord with the LinkedNSAPI
	}
}

bool PdpManager::GetPdpRecordPgwTeidControlPlane(uint32_t Teid, PdpRecord& Record, struct sockaddr_in& Source) const
{
	//TODO dedicated map for fast lookup; TEID+source s_addr in tuple as key
	for(PdpRecords::const_iterator Iter = m_PdpRecords.begin(), EndIter = m_PdpRecords.end();Iter!=EndIter;Iter++)
	{
#ifdef	NO_SOURCE_CHECK
		if(Iter->second.PgwTeidControlPlaneMapped == Teid)
#else
		if(Source.sin_addr.s_addr == Iter->second.Sgw.c_plane_addr.sin_addr.s_addr && Iter->second.PgwTeidControlPlaneMapped == Teid)
#endif
		{
			Record = Iter->second;
			return true;
		}
	}
	return false;
}

bool PdpManager::GetPdpRecordSgwTeidControlPlane(uint32_t Teid, PdpRecord& Record, struct sockaddr_in& Source) const
{
	//TODO dedicated map for fast lookup; TEID+source s_addr in tuple as key
	for(PdpRecords::const_iterator Iter = m_PdpRecords.begin(), EndIter = m_PdpRecords.end();Iter!=EndIter;Iter++)
	{
#ifdef	NO_SOURCE_CHECK
		if(Iter->second.SgwTeidControlPlaneMapped == Teid)
#else
		if(Source.sin_addr.s_addr == Iter->second.Pgw.c_plane_addr.sin_addr.s_addr && Iter->second.SgwTeidControlPlaneMapped == Teid)
#endif
		{
			Record = Iter->second;
			return true;
		}
	}
	return false;
}

bool PdpManager::GetGpduPgwDstAddrForTeidDataPlane(uint32_t& Teid, struct sockaddr_in& Destination, struct sockaddr_in& Source, bool &ReceivedEchoResponseFromPgw, uint32_t GpduSize)
{
#ifdef	NO_SOURCE_CHECK
	SourceTeid Key = MakeTeidDataPlaneRecordKey(0,Teid);
#else
	SourceTeid Key = MakeTeidDataPlaneRecordKey(Source.sin_addr.s_addr,Teid);
#endif
	TeidRecordKeyMapConstIter TeidSourceIter = m_SgwTeidDataPlaneRecordKeyMap.find(Key);
	if(TeidSourceIter != m_SgwTeidDataPlaneRecordKeyMap.end())
	{
		PdpRecords::iterator PdpRecordsIter = m_PdpRecords.find(TeidSourceIter->second);
		if(PdpRecordsIter != m_PdpRecords.end())
		{
			//TODO DEBUG: remove the next lines after debug
			if(LOG_CATEGORY_GTP_U)DEBUG(("GTP-U found PDP Record %u for TEID 0x%08x Source:%s on TEID MAP\n", PdpRecordsIter->second.KeyValue, Teid, inet_ntoa(Source.sin_addr)));

			Destination = PdpRecordsIter->second.Pgw.u_plane_addr;

			PdpRecordsIter->second.ReportSgwData(GpduSize,this,m_PdpRecordActivityTimeout);

			PdpRecord::DataPlaneTeids* DataPlaneTeids = PdpRecordsIter->second.GetDataPlaneTeidsForDplaneMappedTeid(Teid);
			if(DataPlaneTeids != NULL)
			{
				DataPlaneTeids->SgwData += GpduSize;
				Teid = DataPlaneTeids->PgwTeidDataPlane;
			}
			else
				if(LOG_CATEGORY_GTP_U)WARN(("GTP-U failed to find D-TEID set on PDP Record for S-GW TEID 0x%08x Source:%s\n", Teid, inet_ntoa(Source.sin_addr)));

			m_ThePgwManagerInterface->GetReceivedEchoResponseFromPgw(PdpRecordsIter->second.PacketGateway, ReceivedEchoResponseFromPgw);
			return DataPlaneTeids != NULL;
		}
	}

	//TODO/TBD remove the fallback search through the entire PDP records
	//TBD need for PDP record reference for activity and data accounting
	for(PdpRecords::iterator Iter = m_PdpRecords.begin(), EndIter = m_PdpRecords.end();Iter!=EndIter;Iter++)
	{
		PdpRecord::DataPlaneTeids* DataPlaneTeids = Iter->second.GetDataPlaneTeidsForDplaneMappedTeid(Teid);
#ifdef	NO_SOURCE_CHECK
		if(DataPlaneTeids != NULL)
#else
		if(Source.sin_addr.s_addr == Iter->second.Sgw.u_plane_addr.sin_addr.s_addr && DataPlaneTeids != NULL)
#endif
		{
			//TODO DEBUG: remove the next lines after debug
			if(LOG_CATEGORY_GTP_U)DEBUG(("GTP-U found PDP Record %u for TEID 0x%08x Source:%s on PDP Records\n", Iter->second.KeyValue, Teid, inet_ntoa(Source.sin_addr)));

			Destination = Iter->second.Pgw.u_plane_addr;

			Iter->second.ReportSgwData(GpduSize,this,m_PdpRecordActivityTimeout);

			DataPlaneTeids->SgwData += GpduSize;
			Teid = DataPlaneTeids->PgwTeidDataPlane;

			m_ThePgwManagerInterface->GetReceivedEchoResponseFromPgw(Iter->second.PacketGateway, ReceivedEchoResponseFromPgw);
			return true;
		}
	}
	return false;
}

bool PdpManager::GetGpduSgwDstAddrForTeidDataPlane(uint32_t& Teid, struct sockaddr_in& Destination, struct sockaddr_in& Source, uint32_t GpduSize)
{
#ifdef	NO_SOURCE_CHECK
	SourceTeid Key = MakeTeidDataPlaneRecordKey(0,Teid);
#else
	SourceTeid Key = MakeTeidDataPlaneRecordKey(Source.sin_addr.s_addr,Teid);
#endif
	TeidRecordKeyMapConstIter TeidSourceIter = m_PgwTeidDataPlaneRecordKeyMap.find(Key);
	if(TeidSourceIter != m_PgwTeidDataPlaneRecordKeyMap.end())
	{
		PdpRecords::iterator PdpRecordsIter = m_PdpRecords.find(TeidSourceIter->second);
		if(PdpRecordsIter != m_PdpRecords.end())
		{
			//TODO DEBUG: remove the next lines after debug
			if(LOG_CATEGORY_GTP_U)DEBUG(("GTP-U found PDP Record %u for TEID 0x%08x Source:%s on TEID MAP\n", PdpRecordsIter->second.KeyValue, Teid, inet_ntoa(Source.sin_addr)));

			Destination = PdpRecordsIter->second.Sgw.u_plane_addr;

			PdpRecordsIter->second.ReportPgwData(GpduSize,this,m_PdpRecordActivityTimeout);

			PdpRecord::DataPlaneTeids* DataPlaneTeids = PdpRecordsIter->second.GetDataPlaneTeidsForDplaneMappedTeid(Teid);
			if(DataPlaneTeids != NULL)
			{
				DataPlaneTeids->PgwData += GpduSize;
				Teid = DataPlaneTeids->SgwTeidDataPlane;
			}
			else
				if(LOG_CATEGORY_GTP_U)WARN(("GTP-U failed to find D-TEID set on PDP Record for P-GW TEID 0x%08x Source:%s\n", Teid, inet_ntoa(Source.sin_addr)));

			return DataPlaneTeids != NULL;
		}
	}

	//TODO/TBD remove the fallback search through the entire PDP records
	//TBD need for PDP record reference for activity and data accounting
	for(PdpRecords::iterator Iter = m_PdpRecords.begin(), EndIter = m_PdpRecords.end();Iter!=EndIter;Iter++)
	{
		PdpRecord::DataPlaneTeids* DataPlaneTeids = Iter->second.GetDataPlaneTeidsForDplaneMappedTeid(Teid);
#ifdef	NO_SOURCE_CHECK
		if(DataPlaneTeids != NULL)
#else
		if(Source.sin_addr.s_addr == Iter->second.Pgw.u_plane_addr.sin_addr.s_addr && DataPlaneTeids != NULL)
#endif
		{
			//TODO DEBUG: remove the next lines after debug
			if(LOG_CATEGORY_GTP_U)DEBUG(("GTP-U found PDP Record %u for TEID 0x%08x Source:%s on PDP Records\n", Iter->second.KeyValue, Teid, inet_ntoa(Source.sin_addr)));

			Destination = Iter->second.Sgw.u_plane_addr;

			Iter->second.ReportPgwData(GpduSize,this,m_PdpRecordActivityTimeout);

			DataPlaneTeids->PgwData += GpduSize;
			Teid = DataPlaneTeids->SgwTeidDataPlane;
			return true;
		}
	}
	return false;
}

//from PdpManagerInterface
void PdpManager::AddPgwCreateSessionRequestReference(const PdpRecord& Record, struct sockaddr_in Dest)
{
#ifdef	NO_SOURCE_CHECK
		SourceTeid PgwSeqKey = MakeSequenceControlPlaneRecordKey(0, SequenceNumber);
#else
		SourceTeid PgwSeqKey = MakeSequenceControlPlaneRecordKey(Dest.sin_addr.s_addr, Record.PdpCreatePgwSequenceNumber);
#endif
		m_PgwSequenceControlPlaneRecordKeyMap.insert(std::pair<SourceTeid, uint32_t/*PdpRecord Key*/>(PgwSeqKey, Record.KeyValue));
		if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP create request added reference for record with Key:%u Seq:%u Dest:%s\n", Record.KeyValue, Record.PdpCreatePgwSequenceNumber, inet_ntoa(Dest.sin_addr)));
}

bool PdpManager::GetCreatePdpRecord(uint32_t SequenceNumber, PdpRecord& Record, struct sockaddr_in& Source, bool CreateRequest)
{
	//TODO dedicated map for fast lookup; SequenceNumber+source s_addr in tuple as key
	if(!CreateRequest)
	{
#ifdef	NO_SOURCE_CHECK
		SourceTeid PgwSeqKey = MakeSequenceControlPlaneRecordKey(0, SequenceNumber);
#else
		SourceTeid PgwSeqKey = MakeSequenceControlPlaneRecordKey(Source.sin_addr.s_addr, SequenceNumber);
#endif
		TeidRecordKeyMapIter Iter = m_PgwSequenceControlPlaneRecordKeyMap.find(PgwSeqKey);
		if(Iter != m_PgwSequenceControlPlaneRecordKeyMap.end())
		{
			PdpRecordsMapConstIter RecordIter = m_PdpRecords.find(Iter->second);
			if(RecordIter != m_PdpRecords.end())
			{
				Record = RecordIter->second;
				if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP create response located existing record with Key:%u Seq:%u Source:%s\n", Record.KeyValue, SequenceNumber, inet_ntoa(Source.sin_addr)));
			}
			m_PgwSequenceControlPlaneRecordKeyMap.erase(Iter);
		}
	}

	for(PdpRecords::const_iterator Iter = m_PdpRecords.begin(), EndIter = m_PdpRecords.end();Iter!=EndIter;Iter++)
	{
		if(CreateRequest)
		{
			//Searching for create request retransmissions/re-trials
#ifdef	NO_SOURCE_CHECK
			if(Iter->second.PdpCreateSgwSequenceNumber == SequenceNumber)
#else
			if(Source.sin_addr.s_addr == Iter->second.Sgw.c_plane_addr.sin_addr.s_addr && Iter->second.PdpCreateSgwSequenceNumber == SequenceNumber)
#endif
			{
				Record = Iter->second;
				return true;
			}
		}
		else
		{
			//TODO remove fallback searching as the record should have been located on m_PgwSequenceControlPlaneRecordKeyMap
#ifdef	NO_SOURCE_CHECK
			if(Iter->second.PdpCreatePgwSequenceNumber == SequenceNumber)
#else
			if(Source.sin_addr.s_addr == Iter->second.Pgw.c_plane_addr.sin_addr.s_addr && Iter->second.PdpCreatePgwSequenceNumber == SequenceNumber)
#endif
			{
				Record = Iter->second;
				//DEBUG
				if(LOG_CATEGORY_GTP_C)WARN(("GTP-C PDP create response located existing record with Key:%u Seq:%u Source:%s\n", Record.KeyValue, SequenceNumber, inet_ntoa(Source.sin_addr)));
				return true;
			}
		}
	}
	return false;
}

//from PdpManagerInterface
/*virtual*/ bool PdpManager::GetOrInitPdpRecord(PdpRecord& pdpRecord, uint32_t& PdpRecordKey, unsigned char* buffer, ssize_t rcvsize, struct sockaddr_in fromaddr, uint8_t GtpMsgType, ProxyDirection Direction, uint32_t PacketTeid)
{
	bool Result = false;
	if((GtpMsgType == GTPV2_CREATE_SESSION_REQUEST || GtpMsgType == GTPV1_CREATE_PDP_REQ) && Direction == ProxyDirection::TO_PGW_REMOTE)//TODO review behavior for PGW initiated PDP context activation
	{
		//TODO deal with GTPv2 seq. 0 and EBI (EPS Bearer Id) of existing session

		//TODO deal with duplicate requests
		uint32_t CreateSequenceNumber = getGtpHeaderSequenceNumber(buffer, rcvsize);

		if(GetCreatePdpRecord(CreateSequenceNumber, pdpRecord, fromaddr, true/*CreateRequest*/))
		{
			if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP create request located existing record with Key:%u Seq:%u Source:%s\n", pdpRecord.KeyValue, CreateSequenceNumber, inet_ntoa(fromaddr.sin_addr)));
		}
		else
		{
			pdpRecord.KeyValue = ++m_PdpKeyNumberGenerator;
		}

		pdpRecord.ReportSgwData(0,this,m_PdpRecordActivityTimeout);

		pdpRecord.PdpCreateSgwSequenceNumber = CreateSequenceNumber;

#ifdef NO_MAPPING
		PdpRecordKey = pdpRecord.KeyValue = pdpRecord.PdpCreatePgwSequenceNumber = pdpRecord.PdpCreateSgwSequenceNumber;
#else
		PdpRecordKey = pdpRecord.KeyValue;
		pdpRecord.PdpCreatePgwSequenceNumber = (uint16_t)PdpRecordKey;
#endif
		pdpRecord.SgwAddrPdpResponseAddress = fromaddr;

		//Set the sequence number towards the P-GW
		setGtpHeaderSequenceNumber(buffer, rcvsize, pdpRecord.PdpCreatePgwSequenceNumber);

		pdpRecord.SetPdpState(PDP_STATE_CREATE_REQESTED, this);

		if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP create request initialized with Key:%u Seq:%u Source:%s\n", pdpRecord.KeyValue, pdpRecord.PdpCreatePgwSequenceNumber, inet_ntoa(fromaddr.sin_addr)));

		Result = true;
	}
	else if((GtpMsgType == GTPV2_CREATE_SESSION_RESPONSE || GtpMsgType == GTPV1_CREATE_PDP_RSP) && Direction == ProxyDirection::TO_SGW_REMOTE)
	{
		uint32_t CreateSequenceNumber = getGtpHeaderSequenceNumber(buffer, rcvsize);

		if(GetCreatePdpRecord(CreateSequenceNumber, pdpRecord, fromaddr, false/*CreateRequest*/))
		{
			if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP create response located record with Key:%u Seq:%u Source:%s\n", pdpRecord.KeyValue, CreateSequenceNumber, inet_ntoa(fromaddr.sin_addr)));

			pdpRecord.ReportPgwData(0,this,m_PdpRecordActivityTimeout);

			PdpRecordKey = pdpRecord.KeyValue;

			//Re-write the sequence number to which this response relates to towards the S-GW
			setGtpHeaderSequenceNumber(buffer, rcvsize, pdpRecord.PdpCreateSgwSequenceNumber);

			//only call SetPdpState(PDP_STATE_CREATED) after setGtpHeaderSequenceNumber() since it deletes PdpCreateSgwSequenceNumber
			pdpRecord.SetPdpState(PDP_STATE_CREATED, this);

			//TODO deal with GTPv2 seq. 0 and EBI (EPS Bearer Id) of existing session; perhaps merge the sessions

			Result = true;
		}
		else
		{
			ERROR(("GTP-C PDP create response failed locate record with Key:0x%08x Seq:%u Source:%s\n", PdpRecordKey, CreateSequenceNumber, inet_ntoa(fromaddr.sin_addr)));
		}
	}
	else
	{
		if(Direction == ProxyDirection::TO_PGW_REMOTE)
		{
			if(GetPdpRecordPgwTeidControlPlane(PacketTeid, pdpRecord, fromaddr))
			{
				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP record located record with PGW TEID:0x%08x Source:%s\n", PacketTeid, inet_ntoa(fromaddr.sin_addr)));

				pdpRecord.ReportSgwData(0,this,m_PdpRecordActivityTimeout);

				//TODO: TBD can there be more than one outstanding request with different source ports?
				//TBD: same source port mechanism on GTPv2
				if(isRequestGtpMsgType(GtpMsgType))
					pdpRecord.SgwAddrPdpResponseAddress = fromaddr;
				PdpRecordKey = pdpRecord.KeyValue;

				pdpRecord.PdpLastSgwSequenceNumber = getGtpHeaderSequenceNumber(buffer, rcvsize);
#ifdef NO_MAPPING
				pdpRecord.PdpLastPgwSequenceNumber = pdpRecord.PdpLastSgwSequenceNumber;
#else
				pdpRecord.PdpLastPgwSequenceNumber = (uint16_t)++m_PdpSequenceNumberGenerator;
#endif
				//Set the sequence number towards the P-GW
				setGtpHeaderSequenceNumber(buffer, rcvsize, pdpRecord.PdpLastPgwSequenceNumber);

				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP PGW TEID:0x%08x Source:%s Tracking S-GW Seq:%u P-GW Seq:%u\n", PacketTeid, inet_ntoa(fromaddr.sin_addr), pdpRecord.PdpLastSgwSequenceNumber, pdpRecord.PdpLastPgwSequenceNumber));

				Result = true;
			}
			else
			{
				ERROR(("GTP-C failed to find PDP record for PGW TEID:0x%08x Source:%s\n", PacketTeid, inet_ntoa(fromaddr.sin_addr)));
			}
		}
		else if(Direction == ProxyDirection::TO_SGW_REMOTE)
		{
			uint32_t SequenceNumber = getGtpHeaderSequenceNumber(buffer, rcvsize);

			if(GetPdpRecordSgwTeidControlPlane(PacketTeid, pdpRecord, fromaddr))
			{
				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP record located record with SGW TEID:0x%08x Seq:%u Source:%s\n", PacketTeid, SequenceNumber, inet_ntoa(fromaddr.sin_addr)));

				pdpRecord.ReportPgwData(0,this,m_PdpRecordActivityTimeout);

				PdpRecordKey = pdpRecord.KeyValue;

				if(SequenceNumber == pdpRecord.PdpLastPgwSequenceNumber)
				{
					//Re-write the sequence number to which this response relates to towards the S-GW
					setGtpHeaderSequenceNumber(buffer, rcvsize, pdpRecord.PdpLastSgwSequenceNumber);

					if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C SGW TEID:0x%08x Source:%s resolved pending S-GW Seq:%u P-GW Seq:%u\n", PacketTeid, inet_ntoa(fromaddr.sin_addr), pdpRecord.PdpLastSgwSequenceNumber, pdpRecord.PdpLastPgwSequenceNumber));

					//TODO/TBD reset pdpRecord.PdpLastSgwSequenceNumber to mark the reception of the outstanding response
					pdpRecord.PdpLastSgwSequenceNumber = (uint32_t)-1;
					pdpRecord.PdpLastPgwSequenceNumber = (uint32_t)-1;
				}
				else
					ERROR(("GTP-C SGW TEID:0x%08x Source:%s received Seq:%u is not for pending S-GW Seq:%u P-GW Seq:%u\n", PacketTeid, inet_ntoa(fromaddr.sin_addr), SequenceNumber, pdpRecord.PdpLastSgwSequenceNumber, pdpRecord.PdpLastPgwSequenceNumber));

				Result = true;
			}
			else
			{
				//TODO find the request data with getGtpHeaderSequenceNumber(buffer, rcvsize); -> pdpRecord.PdpLastPgwSequenceNumber
				ERROR(("GTP-C failed to find PDP record for SGW TEID:0x%08x Source:%s\n", PacketTeid, inet_ntoa(fromaddr.sin_addr)));
			}
		}
	}
	return Result;
}


/* vty show PDP Records*/
vty_cmd_match show_pdp_records_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("show", "Display various information"));
  m.nodes.push_back(new node_fixedstring("pdp", "Display PDP context records"));
  m.nodes.push_back(new node_fixedstring("all", "Display all records"));
  return m;
}

void PdpManager::ShowPdpRecords(vty_client* sh) const
{
	size_t size = m_PdpRecords.size();
	time_t Now = 0;
	if(size == 0)
	{
		sh->Printf("No PDP Record on record\r\n");
	}
	else
	{
		sh->Printf("List of PDP Records:\r\n");
		Now = time(NULL);
	}

	unsigned int idx = 1;
	for(PdpRecords::const_iterator Iter = m_PdpRecords.begin(), EndIter = m_PdpRecords.end();Iter!=EndIter;Iter++,idx++)
	{
		sh->Printf("%u/%u\t%s\r\n",idx,size,Iter->second.ToVtyString(Now).c_str());
	}

	size = m_SgwTeidDataPlaneRecordKeyMap.size();
	if(size == 0)
	{
		sh->Printf("No SGW TEID Map on record\r\n");
	}
	else
	{
		sh->Printf("List of SGW TEID Map:\r\n");
		Now = time(NULL);
	}

	idx = 1;
	for(TeidRecordKeyMapConstIter Iter = m_SgwTeidDataPlaneRecordKeyMap.begin(), EndIter = m_SgwTeidDataPlaneRecordKeyMap.end();Iter!=EndIter;Iter++,idx++)
	{
		uint32_t sourceip,teid;
		std::tie(sourceip,teid) = Iter->first;
		sh->Printf("%u/%u\tSGW:%s TEID:0x%08x PDPRecord:%u\r\n",idx,size,GetIPv4AddressStr(sourceip),teid,Iter->second);
	}

	size = m_PgwTeidDataPlaneRecordKeyMap.size();
	if(size == 0)
	{
		sh->Printf("No PGW TEID Map on record\r\n");
	}
	else
	{
		sh->Printf("List of PGW TEID Map:\r\n");
		Now = time(NULL);
	}

	idx = 1;
	for(TeidRecordKeyMapConstIter Iter = m_PgwTeidDataPlaneRecordKeyMap.begin(), EndIter = m_PgwTeidDataPlaneRecordKeyMap.end();Iter!=EndIter;Iter++,idx++)
	{
		uint32_t sourceip,teid;
		std::tie(sourceip,teid) = Iter->first;
		sh->Printf("%u/%u\tPGW:%s TEID:0x%08x PDPRecord:%u\r\n",idx,size,GetIPv4AddressStr(sourceip),teid,Iter->second);
	}
}

void show_pdp_records_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
	PdpManager* ThePpdManager = (PdpManager*)arg;
	ThePpdManager->ShowPdpRecords(sh);
}

/* vty show PDP Records to logfile*/
vty_cmd_match show_pdp_records_tolog_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("show", "Display various information"));
  m.nodes.push_back(new node_fixedstring("pdp", "Display PDP context records"));
  m.nodes.push_back(new node_fixedstring("tolog", "Log all records"));
  return m;
}

void PdpManager::PdpRecordsToLog(vty_client* sh) const
{
	size_t size = m_PdpRecords.size();
	time_t Now = 0;
	if(size == 0)
		sh->Printf("No PDP Record on record\r\n");
	else
		Now = time(NULL);

	unsigned int idx = 1;
	for(PdpRecords::const_iterator Iter = m_PdpRecords.begin(), EndIter = m_PdpRecords.end();Iter!=EndIter;Iter++,idx++)
	{
		sh->Printf("%u/%u\t%s\r\n",idx,size,Iter->second.ToVtyString(Now).c_str());
		INFO(("%u/%u-%s\n",idx,size,Iter->second.ToString().c_str()));
	}
}

void show_pdp_records_tolog_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
	PdpManager* ThePpdManager = (PdpManager*)arg;
	ThePpdManager->PdpRecordsToLog(sh);
}

/* vty show PDP Records*/
vty_cmd_match show_pdp_records_imsi_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("show", "Display various information"));
  m.nodes.push_back(new node_fixedstring("pdp", "Display PDP context records"));
  m.nodes.push_back(new node_fixedstring("imsi", "Display records with given IMSI"));
  m.nodes.push_back(new node_string              );
  return m;
}

void PdpManager::ShowPdpRecordsByImsi(vty_client* sh, const std::string& Imsi) const
{
	size_t size = m_PdpRecords.size();
	size_t recordsfound = 0;
	time_t Now = 0;
	if(size == 0)
		sh->Printf("No PDP Record on record\r\n");
	else
		Now = time(NULL);

	unsigned int idx = 1;
	for(PdpRecords::const_iterator Iter = m_PdpRecords.begin(), EndIter = m_PdpRecords.end();Iter!=EndIter;Iter++,idx++)
	{
		std::string ImsiString;
		uint64_t ImsiNetworkOrder = htobe64(Iter->second.IMSI);
		HexDumpToStr((unsigned char*)&ImsiNetworkOrder,sizeof(uint64_t),ImsiString,true/*reversenibble*/);
		//Note: need to trim right whitespace for exact match. Instead we look for a matching prefix
		//ImsiString.erase(ImsiString.find_last_not_of(' ')+1);

		if(Imsi == ImsiString.substr(0,Imsi.size()))
		{
			sh->Printf("%u/%u\t%s\r\n",idx,size,Iter->second.ToVtyString(Now).c_str());
			recordsfound++;
		}
	}
	if(recordsfound == 0)
		sh->Printf("No PDP Records found for IMSI:%s\r\n",Imsi.c_str());
}

void show_pdp_records_imsi_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
	if(m->nodes.size() > 3)
	{
		std::string imsi = m->nodes[3]->get();
		PdpManager* ThePpdManager = (PdpManager*)arg;
		ThePpdManager->ShowPdpRecordsByImsi(sh,imsi);
	}
	else
		ERROR(("parameter error\r\n"));
}

/* vty change pdp record remove status*/
vty_cmd_match change_pdp_records_remove_key_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("change", "Allows to change process options"));
  m.nodes.push_back(new node_fixedstring("pdp", "PDP record data"));
  m.nodes.push_back(new node_fixedstring("removewithkey", "Set the removal time of a PDP record with given Key value of the record"));
  m.nodes.push_back(new node_string              );
  return m;
}

void PdpManager::ChangePdpRecordsRemoveByKeyValue(vty_client* sh, const std::string& KeyValue)
{
	size_t size = m_PdpRecords.size();
	time_t Now = 0;
	if(size == 0)
		sh->Printf("No PDP Record on record\r\n");
	else
		Now = time(NULL);

	uint32_t keyval = atoi(KeyValue.c_str());

	PdpRecords::iterator Iter = m_PdpRecords.find(keyval);
	if(Iter != m_PdpRecords.end())
	{
		sh->Printf("Set RemoveMeTime for record %u:\r\n\t%s\r\n",Iter->first,Iter->second.ToVtyString(Now).c_str());
		Iter->second.RemoveMeTime = Now;
		StartTimerOnRecord(Iter->second.TimerId, keyval, Now+1);
	}
	else
	{
		sh->Printf("No PDP Record found for Key:%s\r\n",KeyValue.c_str());

		for(TeidRecordKeyMapIter SgwIter = m_SgwTeidDataPlaneRecordKeyMap.begin();SgwIter != m_SgwTeidDataPlaneRecordKeyMap.end();)
		{
			if(SgwIter->second == keyval)
			{
				uint32_t sourceip,teid;
				std::tie(sourceip,teid) = SgwIter->first;

				sh->Printf("Removing SGW:%s TEID:0x%08x entry of PDPRecord:%s\r\n", GetIPv4AddressStr(sourceip), teid, KeyValue.c_str());
				SgwIter = m_SgwTeidDataPlaneRecordKeyMap.erase(SgwIter);
			}
			else
			{
				SgwIter++;
			}
		}

		for(TeidRecordKeyMapIter PgwIter = m_PgwTeidDataPlaneRecordKeyMap.begin();PgwIter != m_PgwTeidDataPlaneRecordKeyMap.end();)
		{
			if(PgwIter->second == keyval)
			{
				uint32_t sourceip,teid;
				std::tie(sourceip,teid) = PgwIter->first;

				sh->Printf("Removing PGW:%s TEID:0x%08x entry of PDPRecord:%s\r\n", GetIPv4AddressStr(sourceip), teid, KeyValue.c_str());
				PgwIter = m_PgwTeidDataPlaneRecordKeyMap.erase(PgwIter);
			}
			else
			{
				PgwIter++;
			}
		}
	}

}

void change_pdp_records_remove_key_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
	if(m->nodes.size() > 3)
	{
		std::string key = m->nodes[3]->get();
		PdpManager* ThePpdManager = (PdpManager*)arg;
		ThePpdManager->ChangePdpRecordsRemoveByKeyValue(sh, key);
	}
	else
		ERROR(("parameter error\r\n"));
}

/* vty change pdp record remove status*/
vty_cmd_match change_pdp_records_remove_imsi_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("change", "Allows to change process options"));
  m.nodes.push_back(new node_fixedstring("pdp", "PDP record data"));
  m.nodes.push_back(new node_fixedstring("removewithimsi", "Set the removal time of a PDP record with given IMSI value of the record"));
  m.nodes.push_back(new node_string              );
  return m;
}

void PdpManager::ChangePdpRecordsRemoveByImsi(vty_client* sh, const std::string& Imsi)
{
	size_t size = m_PdpRecords.size();
	size_t recordsfound = 0;
	time_t Now = 0;
	if(size == 0)
		sh->Printf("No PDP Record on record\r\n");
	else
		Now = time(NULL);

	unsigned int idx = 1;
	for(PdpRecords::iterator Iter = m_PdpRecords.begin(), EndIter = m_PdpRecords.end();Iter!=EndIter;Iter++,idx++)
	{
		std::string ImsiString;
		uint64_t ImsiNetworkOrder = htobe64(Iter->second.IMSI);
		HexDumpToStr((unsigned char*)&ImsiNetworkOrder,sizeof(uint64_t),ImsiString,true/*reversenibble*/);
		//Note: need to trim right whitespace for exact match. Instead we look for a matching prefix
		//ImsiString.erase(ImsiString.find_last_not_of(' ')+1);

		if(Imsi == ImsiString.substr(0,Imsi.size()))
		{
			sh->Printf("Set RemoveMeTime for %u/%u:\r\n\t%s\r\n",idx,size,Iter->second.ToVtyString(Now).c_str());
			Iter->second.RemoveMeTime = Now;
			StartTimerOnRecord(Iter->second.TimerId, Iter->first, Now+1);
			recordsfound++;
		}
	}
	if(recordsfound == 0)
		sh->Printf("No PDP Records found for IMSI:%s\r\n",Imsi.c_str());
}

void change_pdp_records_remove_imsi_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
	if(m->nodes.size() > 3)
	{
		std::string imsi = m->nodes[3]->get();
		PdpManager* ThePpdManager = (PdpManager*)arg;
		ThePpdManager->ChangePdpRecordsRemoveByImsi(sh, imsi);
	}
	else
		ERROR(("parameter error\r\n"));
}
