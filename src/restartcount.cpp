/*
 * restartcount.c
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#include <string>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>

#include "restartcount.h"

#include "utils.h"
#include "logger.h"

unsigned int RestartCounter::RecoveryCounter = 0;

#ifndef RESTART_FILE
#define RESTART_FILE "gsn_restart"
#endif

void  RestartCounter::WriteRecoveryCounter(FILE *fptr)
{
	std::string Filename = FormatStr("./%s", RESTART_FILE);
    if(fptr == NULL)
    {
    	//file must exist
    	fptr = fopen(Filename.c_str(), "rb+");
    }

    if(fptr != NULL)
    {
		fprintf(fptr, "%u\n", RecoveryCounter);
		if(fclose(fptr))
		{
			ERROR(("Failed to close %s file: %s\n", Filename.c_str(), strerror(errno)));
		}
		else
		{
			DEBUG(("Wrote recovery count value %u to %s\n", RecoveryCounter, Filename.c_str()));
		}

    }
}

void RestartCounter::InitRecoveryCounter()
{
	std::string Filename = FormatStr("./%s", RESTART_FILE);
    int mask = umask(022);

    FILE *fptr = NULL;
    fptr = fopen(Filename.c_str(), "rb+");

    if(fptr == NULL) //if file does not exist, create it
    {
        INFO(("Creating %s for recovery count persistence\n", Filename.c_str()));
        fptr = fopen(Filename.c_str(), "wb");
        if(fptr == NULL)
        	ERROR(("Failure to create %s file: %s\n", Filename.c_str(), strerror(errno)));
    }
    else
    {
    	unsigned int Value = 0;
        if(fscanf(fptr, "%u", &Value) > 0)
        {
        	RecoveryCounter = (unsigned char)Value;
            DEBUG(("Read recovery count value %u from %s\n", RecoveryCounter, Filename.c_str()));
            rewind(fptr);
        }
    }

    RecoveryCounter++;
    INFO(("Recovery count value set to %u\n", RecoveryCounter));

    umask(mask);

    WriteRecoveryCounter(fptr);
}


/* vty change g_recovery_counter*/
vty_cmd_match change_recovery_counter_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("change", "Allows to change process options"));
  m.nodes.push_back(new node_fixedstring("recoverycounter", "Change the local recovery counter value"));
  m.nodes.push_back(new node_string              );
  return m;
}

void change_recovery_counter_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
	sh->Printf("Current recovery counter %u\r\n", RestartCounter::RecoveryCounter);

	if(m->nodes.size() > 2)
	{
		std::string parameter = m->nodes[2]->get();
		if(parameter.find_first_not_of("0123456789") == std::string::npos)
		{
			unsigned char recovery_counter = (unsigned char)atoi(parameter.c_str());
			sh->Printf("Changed recovery counter to %u\r\n", recovery_counter);
			RestartCounter::RecoveryCounter = recovery_counter;

			RestartCounter::WriteRecoveryCounter(NULL);
		}
		else
			sh->Printf("To change the recovery counter input numeric digits only\r\n");
	}
}
