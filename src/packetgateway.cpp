/*
 * packetgateway.cpp
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#include "packetgateway.h"

#include "defs.h"
#include "restartcount.h"
#include "gtppacket.h"
#include "utils.h"
#include "globals.h"
#include "logger.h"

const char* GetRegexExceptionReason(const std::regex_error& e)
{
	switch(e.code())
	{
	case std::regex_constants::error_collate:
		return "the expression contains an invalid collating element name";
		break;
	case std::regex_constants::error_ctype:
		return "the expression contains an invalid character class name";
		break;
	case std::regex_constants::error_escape:
		return "the expression contains an invalid escaped character or a trailing escape";
		break;
	case std::regex_constants::error_backref:
		return "the expression contains an invalid back reference";
		break;
	case std::regex_constants::error_brack:
		return "the expression contains mismatched square brackets ('[' and ']')";
		break;
	case std::regex_constants::error_paren:
		return "the expression contains mismatched parentheses ('(' and ')')";
		break;
	case std::regex_constants::error_brace:
		return "the expression contains mismatched curly braces ('{' and '}')";
		break;
	case std::regex_constants::error_badbrace:
		return "the expression contains an invalid range in a {} expression";
		break;
	case std::regex_constants::error_range:
		return "the expression contains an invalid character range (e.g. [b-a])";
		break;
	case std::regex_constants::error_space:
		return "there was not enough memory to convert the expression into a finite state machine";
		break;
	case std::regex_constants::error_badrepeat:
		return "one of *?+{ was not preceded by a valid regular expression";
		break;
	case std::regex_constants::error_complexity:
		return "the complexity of an attempted match exceeded a predefined level";
		break;
	case std::regex_constants::error_stack:
		return "there was not enough memory to perform a match ";
		break;
	default:
		break;
	}
	return "unknown";
}

/////////////////////////////////////////////////////////////////////////////////////
PacketGateway::PacketGateway()
{
	NextCplaneSequenceNumber = rand();
	RemoteRecoveryCounter = 0;
	//LocalRecoveryCounter = g_recovery_counter;
	LastEchoRequestSend = LastEchoResponseReceived = 0;
	TeidCount = rand();
	GtpEchoVersion = gtppacket::GTP_VERSION_1;
	PdpCount = 0;
	ConfigIndex = (unsigned int)-1;
}

const char* PacketGateway::GetStatusString() const
{
	return LastEchoRequestSend ? RespondedToEchoRequest() ? "up" : "down" : "unknown";
}

bool PacketGateway::RespondedToEchoRequest() const
{
	if(LastEchoRequestSend)
	{
		//never got a response or more than two ticks in the past
		if(LastEchoResponseReceived == 0 ||
			(LastEchoResponseReceived + 1 + 2*g_PgwEchoRequestTimeout) < LastEchoRequestSend)
			return false;//P-GW is likely down
	}
	return true;//unknown or up
}

bool PacketGateway::IsConnected() const
{
#ifdef TESTONLY
	//if(ConfigIndex % 2)
		return true;
#endif
	return (LastEchoRequestSend && RespondedToEchoRequest());
}

std::string PacketGateway::ToString() const
{
	return FormatStr("Name:%s Address:%s RecoverCount Local:%u Remote:%u NextSeq:%u LastEchoTx:%s V%u LastEchoRx:%s ActivePdp:%u Status:%s",
			Name.c_str(), GatewayAddress::ToString().c_str(),
			RestartCounter::RecoveryCounter/*LocalRecoveryCounter*/, RemoteRecoveryCounter, NextCplaneSequenceNumber,
			LastEchoRequestSend ? FormatLocalTime(LastEchoRequestSend).c_str() : "never", GtpEchoVersion, LastEchoResponseReceived ? FormatLocalTime(LastEchoResponseReceived).c_str() : "never",
			PdpCount, GetStatusString());
}

std::string PacketGateway::ToVtyString() const
{
	return FormatStr("Name:%s Address:%s\r\n\tRecoverCount Local:%u Remote:%u NextSeq:%u\r\n\tLastEchoTx:%s V%u LastEchoRx:%s ActivePdp:%u Status:%s",
			Name.c_str(), GatewayAddress::ToString().c_str(),
			RestartCounter::RecoveryCounter/*LocalRecoveryCounter*/, RemoteRecoveryCounter, NextCplaneSequenceNumber,
			LastEchoRequestSend ? FormatLocalTime(LastEchoRequestSend).c_str() : "never", GtpEchoVersion, LastEchoResponseReceived ? FormatLocalTime(LastEchoResponseReceived).c_str() : "never",
			PdpCount, GetStatusString());
}


/////////////////////////////////////////////////////////////////////////////////////
MatchingDomain::MatchingDomain()
{
	ConfigIndex = (unsigned int)-1;
	MatchOnlyWhenConnected = false;
}


bool MatchingDomain::FindAvailablePgw(const std::vector<PacketGateway>& ThePacketGateways, int& PacketGatewayIndex) const
{
	bool FoundPgw = false;
	bool Once = true;
	unsigned int LowestPdpCount = (unsigned int)-1;

	const size_t PacketGatewaySize = ThePacketGateways.size();
	for(size_t i=0;i<PacketGatewaySize;i++)
	{
		if(std::find(PacketGatewayNames.begin(),PacketGatewayNames.end(),ThePacketGateways[i].Name) != PacketGatewayNames.end())
		{
			if(MatchOnlyWhenConnected == false && Once)
			{
				//we'll take the first P-GW with matching name once anything goes. Will be updated once there is something better.
				FoundPgw = true;
				PacketGatewayIndex = i;
				Once = false;
			}

			if(ThePacketGateways[i].IsConnected())
			{
				FoundPgw = true;
				if(ThePacketGateways[i].PdpCount <= LowestPdpCount)
				{
					LowestPdpCount = ThePacketGateways[i].PdpCount;
					PacketGatewayIndex = i;
				}
			}
		}
	}
	return FoundPgw;
}

bool MatchingDomain::IsMatchingImsi(const std::string& IMSI) const
{
	if(!IMSI.empty() && !ImsiMatchingString.empty() && std::regex_match(IMSI, ImsiMatchingRegEx))
	{
		if(LOG_CATEGORY_GTP_C)INFO(("MatchingDomain:%s Matched IMSI:%s\n", Domain.c_str(), IMSI.c_str()));
		return true;
	}
	return false;
}

bool MatchingDomain::IsMatchingApn(const std::string& APN) const
{
	if(!APN.empty() && !ApnMatchingString.empty() && std::regex_match(APN, ApnMatchingRegEx))
	{
		if(LOG_CATEGORY_GTP_C)INFO(("MatchingDomain:%s Matched APN:%s\n", Domain.c_str(), APN.c_str()));
		return true;
	}
	return false;
}

bool MatchingDomain::AssignPgw(std::vector<PacketGateway>& ThePacketGateways, GatewayAddress& Pgw, int& PacketGatewayIndex) const
{
	if(FindAvailablePgw(ThePacketGateways, PacketGatewayIndex) && PacketGatewayIndex >= 0 && PacketGatewayIndex < (int)ThePacketGateways.size())
	{
		Pgw = ThePacketGateways[PacketGatewayIndex];
		ThePacketGateways[PacketGatewayIndex].PdpCount++;
		if(LOG_CATEGORY_GTP_C)INFO(("MatchingDomain:%s Assigned P-GW%u\n", Domain.c_str(), PacketGatewayIndex+1));
		return true;
	}
	return false;
}

std::string MatchingDomain::ToString() const
{
	return FormatStr("MatchingDomain:%s MatchingIMSI:%s MatchingAPN:%s PGWs:%s MatchOnlyWhenConnected:%s",
			Domain.c_str(), ImsiMatchingString.c_str(), ApnMatchingString.c_str(), VectorOfStringsToCsv(PacketGatewayNames).c_str(), MatchOnlyWhenConnected ? "Yes" : "No");
}

std::string MatchingDomain::ToVtyString() const
{
	return FormatStr("MatchingDomain:%s\r\n\tMatchingIMSI:%s\r\n\tMatchingAPN:%s\r\n\tPGWs:%s MatchOnlyWhenConnected:%s",
			Domain.c_str(), ImsiMatchingString.c_str(), ApnMatchingString.c_str(), VectorOfStringsToCsv(PacketGatewayNames).c_str(), MatchOnlyWhenConnected ? "Yes" : "No");
}
