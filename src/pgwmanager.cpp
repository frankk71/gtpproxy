/*
 * pgwmanager.cpp
 *
 *  Created on: Feb 25, 2019
 *      Author: JpU FK
 */

#include "pgwmanager.h"
#include "gtppacket.h"
#include "logger.h"
#include "defs.h"
#include "globals.h"
#include "restartcount.h"

#include <stdlib.h>

PgwManager::PgwManager()
{
	m_LastPgwArrayMaintenanceTime = 0;
	m_TeidCount = (uint32_t)rand();

	//must be done before GetPgwConfiguration()
	RestartCounter::InitRecoveryCounter();
}

//from PgwManagerInterface
/*virtual*/ uint32_t PgwManager::MakeTeid(int PgwIndex, ProxyDirection Direction)
{
	//ETSI 123 107 chapter 10.0 "The GGSN should ensure as far as possible that previously
	//used TEID values are not immediately reused after a GGSN restart, in order to avoid inconsistent TEID allocation throughout the network."
	// -> m_TeidCount and ThePacketGateways[PgwIndex].TeidCount are initialized with a random number

	uint32_t teid = Direction == ProxyDirection::TO_PGW_REMOTE ? 0x80000000 : 0;
	teid |= ((PgwIndex << 24) & 0x7F000000);
	teid |= rand() & 0x00FF0000;//a bit less likely to predict the next one
	if(PgwIndex != -1 && PgwIndex < (signed)ThePacketGateways.size())
		teid |= ThePacketGateways[PgwIndex].TeidCount++ & 0x0000FFFF;//more likely to be unique per PGW
	else
	{
		ERROR(("No valid PGW index for MakeTeid() call\n"));
		teid |= m_TeidCount++ & 0x0000FFFF;
	}
	return teid;
}

//from PgwManagerInterface
/*virtual*/ void PgwManager::UpdatePgwRemoteCounter(gtppacket::GTP_VERSION Version, uint32_t SequenceNumber, unsigned char RemoteRecoveryCounter, struct sockaddr_in& src)
{
	bool UpdatedRemoteCounter = false;

	for(PacketGatewayArray::iterator Iter = ThePacketGateways.begin();Iter!=ThePacketGateways.end();Iter++)
	{
		//TODO this requires unique IP addresses on ThePacketGateways instances.
		//Perhaps we keep a list of (unique) sequence numbers per P-GW instance with TTL. Can be part of TODO Handle retransmissions
		if(Iter->c_plane_addr.sin_addr.s_addr == src.sin_addr.s_addr)
		{
			if(Version == gtppacket::GTP_VERSION_1)
			{
				//Iter->NextCplaneSequenceNumber got incremented on successful sent
				if(Iter->NextCplaneSequenceNumber == (uint16_t)(SequenceNumber+1))
				{
					if(LOG_CATEGORY_GTP_C && LOG_CATEGORY_GTP_ECHO)INFO(("GTPv1 Echo Request/Response Sequence Number %u from P-GW %s RecoveryCount %u matches request\n",
							(uint16_t)(SequenceNumber+1), inet_ntoa(src.sin_addr), RemoteRecoveryCounter));
				}
				else
				{
					WARN(("GTPv1 Echo Request/Response Sequence Number mismatch %u != %u from P-GW %s RecoveryCount %u\n",
							Iter->NextCplaneSequenceNumber, (uint16_t)(SequenceNumber + 1), inet_ntoa(src.sin_addr), RemoteRecoveryCounter));
				}
			}
			else if(Version == gtppacket::GTP_VERSION_2)
			{
				//Iter->NextCplaneSequenceNumber got incremented on successful sent
				if(Iter->NextCplaneSequenceNumber == ((SequenceNumber + 1) & 0x00FFFFFF))
				{
					if(LOG_CATEGORY_GTP_C && LOG_CATEGORY_GTP_ECHO)INFO(("GTPv2 Echo Request/Response Sequence Number %u from P-GW %s RecoveryCount %u matches request\n",
							((SequenceNumber + 1) & 0x00FFFFFF), inet_ntoa(src.sin_addr), RemoteRecoveryCounter));
				}
				else
				{
					WARN(("GTPv2 Echo Request/Response Sequence Number mismatch %u != %u from P-GW %s RecoveryCount %u\n",
							Iter->NextCplaneSequenceNumber, ((SequenceNumber + 1) & 0x00FFFFFF), inet_ntoa(src.sin_addr), RemoteRecoveryCounter));
				}
			}

			//TODO handle remote counter increments of this P-GW towards S-GW instances that have affected sessions
			if(RemoteRecoveryCounter != 0)//TBD wrap-around through 0
			{
				if(Iter->RemoteRecoveryCounter && Iter->RemoteRecoveryCounter+1 <= RemoteRecoveryCounter)
					WARN(("Echo request response from %s Sequence Number %u RecoveryCount %u larger than previous recorded RecoveryCount %u\n", inet_ntoa(src.sin_addr), SequenceNumber, RemoteRecoveryCounter, Iter->RemoteRecoveryCounter));
				Iter->RemoteRecoveryCounter = RemoteRecoveryCounter;
			}
			Iter->LastEchoResponseReceived = time(NULL);
			UpdatedRemoteCounter = true;
			break;
		}
	}

	if(!UpdatedRemoteCounter)
		ERROR(("Echo request response failed to find the P-GW instance for response from %s Sequence Number %u RecoveryCount %u\n", inet_ntoa(src.sin_addr), SequenceNumber, RemoteRecoveryCounter));
}


//from PgwManagerInterface
/*virtual*/ void PgwManager::AssignPgw(const std::string& IMSI, const std::string& APN, GatewayAddress& Pgw, int& PacketGatewayIndex)
{
	//the default P-GW is on the last index. m_ThePgwManagerInterface->ThePacketGateways has at least one entry
	size_t PgwCount =  ThePacketGateways.size();
	Pgw = ThePacketGateways[PgwCount-1];
	PacketGatewayIndex = PgwCount-1;

	if(!IMSI.empty() || !APN.empty())
	{
		const size_t MatchingDomainCount = TheMatchingDomains.size();
		for(unsigned int i=0;i<MatchingDomainCount;i++)
		{
			if( (TheMatchingDomains[i].IsMatchingImsi(IMSI) || TheMatchingDomains[i].IsMatchingApn(APN)) &&
					TheMatchingDomains[i].AssignPgw(ThePacketGateways, Pgw, PacketGatewayIndex) )
			{
				return;
			}
		}
	}

	//Default P-GW selected
	ThePacketGateways[PgwCount-1].PdpCount++;

	if(LOG_CATEGORY_GTP_C)
	{
		if(!IMSI.empty() || !APN.empty())
		{
			INFO(("Assigned default P-GW%u for IMSI:%s APN:%s\n", PgwCount, IMSI.c_str(), APN.c_str()));
		}
		else
		{
			INFO(("Assigned default P-GW%u\n", PgwCount));
		}
	}
	return;
}

/*virtual*/ void PgwManager::OnPdpRecordRemoved(const int PacketGateway)
{
	if(PacketGateway >= 0 && PacketGateway < (signed)ThePacketGateways.size())
		ThePacketGateways[PacketGateway].PdpCount--;
}

/*virtual*/
void PgwManager::GetReceivedEchoResponseFromPgw(const int PacketGateway, bool& ReceivedEchoResponseFromPgw)
{
	if(PacketGateway != -1 && ThePacketGateways[PacketGateway].LastEchoRequestSend)
	{
		ReceivedEchoResponseFromPgw = ThePacketGateways[PacketGateway].RespondedToEchoRequest();
	}
}

void PgwManager::LogPgwConfiguration(const char* Context, struct sockaddr_in& localaddr, struct sockaddr_in& remoteaddr) const
{
	std::string Source = FormatStr("local address %s port %u",inet_ntoa(localaddr.sin_addr), htons(localaddr.sin_port));
	std::string Destination = FormatStr("remote address %s port %u",inet_ntoa(remoteaddr.sin_addr), htons(remoteaddr.sin_port));

	DEBUG(("%s %s <-> %s configured\n", Context, Source.c_str(), Destination.c_str()));
}

void PgwManager::GetPgwConfiguration(unsigned int ConfigIndex, const pgwParams& Params)
{
	PacketGateway Pgw;

	Pgw.GtpEchoVersion = (gtppacket::GTP_VERSION)Params.version;
	inet_aton(Params.remoteaddr_c.c_str(), &(Pgw.c_plane_addr.sin_addr));
	Pgw.c_plane_addr.sin_family = AF_INET;
	Pgw.c_plane_addr.sin_port = htons((short)Params.remoteport_c);

	inet_aton(Params.remoteaddr_u.c_str(), &(Pgw.u_plane_addr.sin_addr));
	Pgw.u_plane_addr.sin_family = AF_INET;
	Pgw.u_plane_addr.sin_port = htons((short)Params.remoteport_u);

	Pgw.ConfigIndex = ConfigIndex;
	Pgw.Name = Params.name;

	//TODO check that no other Pgw instance on ThePacketGateways has the same pgw_c_remoteaddr or pgw_u_remoteaddr on any remote address parameter
	ThePacketGateways.push_back(Pgw);
}

void PgwManager::GetMdConfiguration(unsigned int ConfigIndex, const matchingDomainParams& Params)
{
	MatchingDomain Md;

	try
	{
		Md.ApnMatchingRegEx = Md.ApnMatchingString = Params.APN_regex;
	}
	catch (const std::regex_error& e)
	{
		ERROR(("%s Exception regex_error for APN:%s What:%s Code:%d %s - Check the configuration\n", Params.domain.c_str(),  Params.APN_regex, e.what(), e.code(), GetRegexExceptionReason(e)));
		exit(0);
	}

	try
	{
		Md.ImsiMatchingRegEx = Md.ImsiMatchingString = Params.IMSI_regex;
	}
	catch (const std::regex_error& e)
	{
		ERROR(("%s Exception regex_error for IMSI:%s What:%s Code:%d %s - Check the configuration\n", Params.domain.c_str(), Params.IMSI_regex, e.what(), e.code(), GetRegexExceptionReason(e)));
		exit(0);
	}

	Md.ConfigIndex = ConfigIndex;
	Md.Domain = Params.domain;
	Md.MatchOnlyWhenConnected = Params.matchonlywhenconnected != 0;
	Md.PacketGatewayNames = Params.pgw_names;

	TheMatchingDomains.push_back(Md);
}

bool PgwManager::Configure(Configuration& Config)
{
	unsigned int Count = Config.getPgwConfigCount();

	for(unsigned int i = 0; i < Count; ++i)
	{
		pgwParams Params;

		if(!Config.readPgwConfig(i, Params))
		{
			ERROR(("Incomplete PacketGateway configuration at Index:%d\n", i));
			return false;
		}
		else
		{
			GetPgwConfiguration(i, Params);
		}
	}

	for(unsigned int i = 0; i<ThePacketGateways.size();i++)
	{
		LogPgwConfiguration(FormatStr("PGW%u-C",i+1).c_str(), pgw_c_local_addr, ThePacketGateways[i].c_plane_addr);
		LogPgwConfiguration(FormatStr("PGW%u-U",i+1).c_str(), pgw_u_local_addr, ThePacketGateways[i].u_plane_addr);
	}

	if(Count > 0 && Count != ThePacketGateways.size())
		return false;

	Count = Config.getMatchingDomainConfigCount();

	for(unsigned int i = 0; i < Count; ++i)
	{
		matchingDomainParams Params;

		if(!Config.readMatchingDomainConfig(i, Params))
		{
			ERROR(("Incomplete MatchingDomain configuration at Index:%d\n", i));
			return false;
		}
		else
		{
			GetMdConfiguration(i, Params);
		}
	}

	if(Count > 0 && Count != TheMatchingDomains.size())
		return false;

	return true;
}

void PgwManager::UpdateConfigValues(Configuration& Config)
{
	unsigned int Count = Config.getPgwConfigCount();

	if(Count != ThePacketGateways.size())
	{
		ERROR(("Change of %s amount not supported during runtime\n", CONFIG_PGW_GROUP));
		return;
	}

	for(unsigned int i = 0; i < Count; ++i)
	{
		pgwParams Params;
		if(Config.readPgwConfig(i, Params))
		{
			//TBD check (i == ThePacketGateways[i].ConfigIndex)
			if(Params.name == ThePacketGateways[i].Name)
			{
				if(Params.version != ThePacketGateways[i].GtpEchoVersion)
				{
					ThePacketGateways[i].GtpEchoVersion = (gtppacket::GTP_VERSION)Params.version;
					INFO(("%s PGW EchoVersion set to %u\n", Params.name.c_str(), Params.version));
				}
			}
			else
			{
				WARN(("Change of PGW name not supported during runtime\n"));
			}
		}
		else
		{
			ERROR(("Incomplete %s configuration at Index:%d\n", CONFIG_PGW_GROUP, i));
			return;
		}
	}

	Count = Config.getMatchingDomainConfigCount();

	if(Count != TheMatchingDomains.size())
	{
		ERROR(("Change of %s amount not supported during runtime\n", CONFIG_MD_GROUP));
		return;
	}

	for(unsigned int i = 0; i < Count; ++i)
	{
		matchingDomainParams Params;
		if(Config.readMatchingDomainConfig(i, Params))
		{
			//TBD check (i == TheMatchingDomains[i].ConfigIndex)
			if(Params.domain == TheMatchingDomains[i].Domain)
			{
				if(Params.matchonlywhenconnected != TheMatchingDomains[i].MatchOnlyWhenConnected)
				{
					TheMatchingDomains[i].MatchOnlyWhenConnected = Params.matchonlywhenconnected != 0;
					INFO(("%s MatchOnlyWhenConnected set to %s\n", Params.domain.c_str(), Params.matchonlywhenconnected != 0 ? "enabled" : "disabled"));
				}

				if(Params.IMSI_regex != TheMatchingDomains[i].ImsiMatchingString)
				{
					try
					{
						std::regex testRegex;
						testRegex = Params.IMSI_regex;

						TheMatchingDomains[i].ImsiMatchingRegEx = TheMatchingDomains[i].ImsiMatchingString = Params.IMSI_regex;
						INFO(("%s IMSI matching set to %s\n", Params.domain.c_str(), Params.IMSI_regex.c_str()));
					}
					catch (const std::regex_error& e)
					{
						ERROR(("%s Exception regex_error for IMSI:%s What:%s Code:%d %s - Check the configuration\n", Params.domain.c_str(), Params.IMSI_regex.c_str(), e.what(), e.code(), GetRegexExceptionReason(e)));
					}
				}

				if(Params.APN_regex != TheMatchingDomains[i].ApnMatchingString)
				{
					try
					{
						std::regex testRegex;
						testRegex = Params.APN_regex;

						TheMatchingDomains[i].ApnMatchingRegEx = TheMatchingDomains[i].ApnMatchingString = Params.APN_regex;
						INFO(("%s APN matching set to %s\n", Params.domain.c_str(), Params.APN_regex.c_str()));
					}
					catch (const std::regex_error& e)
					{
						ERROR(("%s Exception regex_error for APN:%s What:%s Code:%d %s - Check the configuration\n", Params.domain.c_str(), Params.APN_regex.c_str(), e.what(), e.code(), GetRegexExceptionReason(e)));
					}
				}

				if(Params.pgw_names != TheMatchingDomains[i].PacketGatewayNames)
				{
					TheMatchingDomains[i].PacketGatewayNames = Params.pgw_names;
					INFO(("%s PGW Names set to %s\n", Params.domain.c_str(), VectorOfStringsToCsv(Params.pgw_names).c_str()));
				}

			}
			else
			{
				WARN(("Change of Domain variable not supported during runtime\n"));
			}
		}
		else
		{
			ERROR(("Incomplete %s configuration at Index:%d\n", CONFIG_MD_GROUP, i));
			return;
		}
	}

}

void PgwManager::TimerMaintenance(time_t Now, int pgwc_fd, const MessageProcessor& TheMessageProcessor)
{
	if(g_PgwEchoRequestTimeout != 0 && Now - m_LastPgwArrayMaintenanceTime > g_PgwEchoRequestTimeout)
	{
		for(PacketGatewayArray::iterator Iter = ThePacketGateways.begin();Iter!=ThePacketGateways.end();Iter++)
		{
			if(TheMessageProcessor.SendEchoRequestMessage(Iter->GtpEchoVersion, Iter->NextCplaneSequenceNumber, RestartCounter::RecoveryCounter/*Iter->LocalRecoveryCounter*/, pgwc_fd, pgw_c_local_addr, Iter->c_plane_addr))
			{
				Iter->NextCplaneSequenceNumber++;
				if(Iter->GtpEchoVersion == gtppacket::GTP_VERSION_1)
				{
					//for GTP_VERSION_1 16 bit
					Iter->NextCplaneSequenceNumber &= 0x0000FFFF;
				}
				else
				{
					//for GTP_VERSION_2 24 bit
					Iter->NextCplaneSequenceNumber &= 0x00FFFFFF;
				}
				Iter->LastEchoRequestSend = Now;
			}
		}
		m_LastPgwArrayMaintenanceTime = Now;
	}
}


/* vty show P-GW*/
vty_cmd_match show_pgw_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("show", "Display various information"));
  m.nodes.push_back(new node_fixedstring("pgw", "Display P-GW info"));
  return m;
}

void PgwManager::ShowPgwList(vty_client* sh) const
{
	size_t size = TheMatchingDomains.size();

	if(size == 0)
		sh->Printf("No MatchingDomain configured\r\n");
	else
		sh->Printf("List of MatchingDomain items:\r\n");


	unsigned int idx = 1;
	for(MatchingDomainArray::const_iterator Iter = TheMatchingDomains.begin(), EndIter = TheMatchingDomains.end();Iter!=EndIter;Iter++,idx++)
	{
		sh->Printf("%u/%u\t%s\r\n",idx,size,Iter->ToVtyString().c_str());
	}

	size = ThePacketGateways.size();

	if(size == 0)
		sh->Printf("No P-GW configured\r\n");
	else
		sh->Printf("List of P-GW items:\r\n");

	idx = 1;
	for(PacketGatewayArray::const_iterator Iter = ThePacketGateways.begin(), EndIter = ThePacketGateways.end();Iter!=EndIter;Iter++,idx++)
	{
		sh->Printf("%u/%u\t%s\r\n",idx,size,Iter->ToVtyString().c_str());
	}
}

void show_pgw_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
	PgwManager* ThePgwManager = (PgwManager*)arg;
	ThePgwManager->ShowPgwList(sh);
}



/* vty util find matching P-GW*/
vty_cmd_match util_match_query_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("util", "Utility function"));
  m.nodes.push_back(new node_fixedstring("matchpgw", "Find matching P-GW"));
  m.nodes.push_back(new node_string              );
  return m;
}

void PgwManager::FindMatchingPgw(vty_client* sh, const std::string& matchingToken) const
{
	if(!matchingToken.empty())
	{
		unsigned int matchingPgw = 0;
		const size_t MatchingDomainCount = TheMatchingDomains.size();
		for(unsigned int i=0;i<MatchingDomainCount;i++)
		{
			if( TheMatchingDomains[i].IsMatchingImsi(matchingToken) )
			{
				sh->Printf("Token: %s matches IMSI regex of Domain %u|%s\r\n", matchingToken.c_str(), i, TheMatchingDomains[i].ToString().c_str());
				matchingPgw++;
			}

			if(TheMatchingDomains[i].IsMatchingApn(matchingToken))
			{
				sh->Printf("Token: %s matches APN regex of Domain %u|%s\r\n", matchingToken.c_str(), i, TheMatchingDomains[i].ToString().c_str());
				matchingPgw++;
			}
		}

		if(matchingPgw == 0)
		{
			sh->Printf("Token: %s matches only the Default P-GW definition\r\n", matchingToken.c_str());
		}
	}
}

void util_match_query_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
	if(m->nodes.size() > 2)
	{
		std::string matchingToken = m->nodes[2]->get();
		PgwManager* ThePgwManager = (PgwManager*)arg;
		ThePgwManager->FindMatchingPgw(sh, matchingToken);
	}
	else
		ERROR(("parameter error\r\n"));
}

