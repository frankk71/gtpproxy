//============================================================================
// Name        : gtpproxy.cpp
// Author      : FK
// Version     : 0.1
// Copyright   : Copyright JpU 2018
// Description : Application to proxy GTP frames between S-GW and P-GW
//============================================================================

//  UE upstream
//	S-GW C/U remote addr --> S-GW C/U local (bind & recv) >--- proxy ---> P-GW C/U local (bind)        --> P-GW C/U remote addr
//  UE downstream
//	S-GW C/U remote addr <-- S-GW C/U local (bind)        <--- proxy ---< P-GW C/U local (bind & recv) <-- P-GW C/U remote addr
#include "defs.h"

#include <sys/timerfd.h>
#include <signal.h>
#include "globals.h"
#include "utils.h"
#include "logger.h"
#include "cmdline.h"

#include "gtppacket.h"
#include "pdprecord.h"
#include "restartcount.h"
#include "messageprocessor.h"
#include "pgwmanager.h"
#include "pdpmanager.h"
#include "configuration.h"
#include "vty.h"

using namespace std;

bool Test();

#define SET_FD(X) if(X){FD_SET(X, &fds);if(X>maxfd)maxfd=X;}

/////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// Helper Functions ////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

bool OpenUdpReceivers(const localAddresses& addresses, int& fd_sgwc, int& fd_sgwu, int& fd_pgwc, int& fd_pgwu)
{
	//defined in globals.h
	//-------------------setting global vars: S-GW Local------------------------
	sgw_c_local_addr = addresses.sgw_c_local_addr;
	sgw_u_local_addr = addresses.sgw_u_local_addr;

	//-------------------setting global vars: P-GW Local------------------------
	pgw_c_local_addr = addresses.pgw_c_local_addr;
	pgw_u_local_addr = addresses.pgw_u_local_addr;

	//bind to local address
	int Result;
	bool Success = true;

	fd_sgwc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	sgw_c_local_addr.sin_family = AF_INET;
	Result = bind(fd_sgwc,(struct sockaddr *)&sgw_c_local_addr, sizeof(sgw_c_local_addr));
	CheckSocketErrorNo(Result,sgw_c_local_addr,"bind SGW facing local address for GTP-C");
	if(Result != 0)
		Success = false;

	fd_sgwu = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	sgw_u_local_addr.sin_family = AF_INET;
	Result = bind(fd_sgwu, (struct sockaddr *)&sgw_u_local_addr, sizeof(sgw_u_local_addr));
	CheckSocketErrorNo(Result,sgw_u_local_addr,"bind SGW facing local address for GTP-U");
	if(Result != 0)
		Success = false;

	fd_pgwc = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	pgw_c_local_addr.sin_family = AF_INET;
	Result = bind(fd_pgwc, (struct sockaddr *)&pgw_c_local_addr, sizeof(pgw_c_local_addr));
	CheckSocketErrorNo(Result,pgw_c_local_addr,"bind PGW facing local address for GTP-C");
	if(Result != 0)
		Success = false;

	fd_pgwu = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	pgw_u_local_addr.sin_family = AF_INET;
	Result = bind(fd_pgwu, (struct sockaddr *)&pgw_u_local_addr, sizeof(pgw_u_local_addr));
	CheckSocketErrorNo(Result,pgw_u_local_addr,"bind PGW facing local address for GTP-U");
	if(Result != 0)
		Success = false;

	return Success;
}

void UpdateLogLevelFromConfiguration(Configuration& Config)
{
	unsigned char DebugLogLevel, DebugCategory;
	if(Config.readLoggingConfig(DebugLogLevel, DebugCategory))
	{
		if(Logger::getDbgLevel() != DebugLogLevel)
		{
			INFO(("Set log level to 0x%02X\n", DebugLogLevel));
			Logger::setDbgLevel(DebugLogLevel);
		}

		if(LogCategory::logCategory != DebugCategory)
		{
			INFO(("Set log category to 0x%02X\n", DebugCategory));
			LogCategory::logCategory = DebugCategory;
		}
	}

	unsigned int CurrentVal;
	if(Config.readIntConfig(CONFIG_LOGGING_GROUP, "MaxFileSize", (int&)CurrentVal) && CurrentVal != Logger::getLogfileMaxSize())
	{
		Logger::setLogfileMaxSize(CurrentVal);
	}
	if(Config.readIntConfig(CONFIG_LOGGING_GROUP, "MaxFileTime", (int&)CurrentVal) && CurrentVal != Logger::getLogfileMaxTime())
	{
		Logger::setLogfileMaxTime(CurrentVal);
	}
}

void UpdateGlobalsFromConfiguration(Configuration& Config)
{
	Config.readIntConfig(CONFIG_TIMER_GROUP, "LogStatisticsTimeout", (int&)g_LogStatisticsTimeout);
	Config.readIntConfig(CONFIG_TIMER_GROUP, "PgwEchoRequestTimeout", (int&)g_PgwEchoRequestTimeout);
	Config.readIntConfig(CONFIG_TIMER_GROUP, "GtpSessionCreateRequestTimeout", (int&)g_GtpSessionCreateRequestTimeout);
	Config.readIntConfig(CONFIG_TIMER_GROUP, "GtpSessionCreateRequestRejectTimeout", (int&)g_GtpSessionCreateRequestRejectTimeout);
	Config.readIntConfig(CONFIG_TIMER_GROUP, "GtpSessionDeleteRequestTimeout", (int&)g_GtpSessionDeleteRequestTimeout);
}

bool init_timer(int& fd_timer)
{
	fd_timer = timerfd_create(CLOCK_MONOTONIC,TFD_NONBLOCK);
	if(-1 == fd_timer)
	{
		ERROR(("timerfd_create() problem %d - %s \n", errno, strerror(errno)));
		return false;
	}

	itimerspec timerspec;
	// 1-second periodic timer
	timerspec.it_interval.tv_sec = 1;
	timerspec.it_interval.tv_nsec = 0;
	timerspec.it_value.tv_sec = 1;
	timerspec.it_value.tv_nsec = 0;
	if(-1 == timerfd_settime(fd_timer, 0, &timerspec, NULL))
	{
		ERROR(("timerfd_settime() problem %d - %s \n", errno, strerror(errno)));
		return false;
	}
	return true;
}

void HandleTimer(unsigned int& PacketCount, int pgwc_fd,
		MessageProcessor& TheMessageProcessor, PdpManager& ThePpdManager, PgwManager& ThePgwManager, const char* ConfigFileName)
{
	static time_t LastStatisticsPrintTime = time(NULL);
	time_t Now = time(NULL);

	if(g_LogStatisticsTimeout)
	{
		unsigned int TimeDiff = Now - LastStatisticsPrintTime;

		if(TimeDiff && TimeDiff > g_LogStatisticsTimeout)//avoid div by 0
		{
			if(PacketCount)
				INFO(("PacketCount %u - %u packets/sec\n", PacketCount, PacketCount/TimeDiff));

			if(sgw_2_pgw_c_data_count)
			{
				INFO(("SGW->PGW C-plane data count %u - %u bytes/sec\n", sgw_2_pgw_c_data_count, sgw_2_pgw_c_data_count/TimeDiff));
				sgw_2_pgw_c_data_count = 0;
			}
			if(sgw_2_pgw_u_data_count)
			{
				INFO(("SGW->PGW U-plane data count %u - %u bytes/sec\n", sgw_2_pgw_u_data_count,sgw_2_pgw_u_data_count/TimeDiff));
				sgw_2_pgw_u_data_count = 0;
			}
			if(pgw_2_sgw_c_data_count)
			{
				INFO(("PGW->SGW C-plane data count %u - %u bytes/sec\n", pgw_2_sgw_c_data_count,pgw_2_sgw_c_data_count/TimeDiff));
				pgw_2_sgw_c_data_count = 0;
			}
			if(pgw_2_sgw_u_data_count)
			{
				INFO(("PGW->SGW U-plane data count %u - %u bytes/sec\n", pgw_2_sgw_u_data_count,pgw_2_sgw_u_data_count/TimeDiff));
				pgw_2_sgw_u_data_count = 0;
			}

			PacketCount = 0;
			LastStatisticsPrintTime = Now;
		}
	}

	ThePpdManager.TimerMaintenance(Now);

	ThePgwManager.TimerMaintenance(Now, pgwc_fd, TheMessageProcessor);

	Logger::cycleLogFileIfNeeded(Now);

	Configuration Config(ConfigFileName);
	if(Config.HasConfigFileChanged())
	{
		UpdateLogLevelFromConfiguration(Config);
		UpdateGlobalsFromConfiguration(Config);

		ThePpdManager.UpdateConfigValues(Config);
		ThePgwManager.UpdateConfigValues(Config);

		Config.ConfigFileChangeHandled();
	}

}


bool RunProxyRun = true;

void sigint(int signal)
{
    INFO(("---------------------------- %s received signal %d----------------------\n", CMDLINE_PARSER_PACKAGE, signal));
    RunProxyRun = false;
}

int main(int argc, char **argv)
{
    srand(time(NULL));
    time(&g_ApplicationStartTime);

	fd_set fds;
	int fd_sgwc, fd_sgwu, fd_pgwc, fd_pgwu, fd_timer, maxfd;
	fd_sgwc = fd_sgwu = fd_pgwc = fd_pgwu = -1;
	vty* Vty = NULL;

	struct timeval idleTime;
	unsigned int PacketCount = 0;

	signal(SIGINT, sigint);//signal no. 2
    signal(SIGQUIT, sigint);//signal no. 3

    //Parse command line arguments
	gengetopt_args_info ai;
	cmdline_parser_init(&ai);
	if(cmdline_parser (argc, argv, &ai) != 0)
		return -1;

	Logger::setExternalLogCycleCall(true);
	Logger::setLogfilePath(ai.logfolder_arg);

	if(!init_timer(fd_timer))
		return -1;

	PgwManager ThePgwManager;
	PdpManager ThePpdManager;

	ThePpdManager.Advise(&ThePgwManager);
	MessageProcessor TheMessageProcessor(&ThePgwManager, &ThePpdManager, &ThePpdManager);

	Configuration Config(ai.configfile_arg);

	//Initial log configuration
	UpdateLogLevelFromConfiguration(Config);
	UpdateGlobalsFromConfiguration(Config);

	if((Config.readInterfaceConfig() && OpenUdpReceivers(Config.m_localAddresses, fd_sgwc, fd_sgwu, fd_pgwc, fd_pgwu)) == false)
	{
		if(fd_sgwc != -1)
			close(fd_sgwc);
		if(fd_sgwu != -1)
			close(fd_sgwu);
		if(fd_pgwc != -1)
			close(fd_pgwc);
		if(fd_pgwu != -1)
			close(fd_pgwu);
		return -1;
	}

	//must be done after OpenUdpReceivers() due to global vars initialized in that function
	if(!ThePgwManager.Configure(Config))
		return -1;

	//Initial log configuration
	ThePpdManager.UpdateConfigValues(Config);

	unsigned short vtyport = VTI_DEFAULT_PORT;
	Config.readVtyConfig(vtyport);
	INFO(("Creating VTY on port %u\n",vtyport));
	Vty = new vty(0x7F000001,vtyport);//binding on localhost. TODO: implement access restriction on VTY

	Vty->install_command(show_pgw_mt(),show_pgw_f,&ThePgwManager);
	Vty->install_command(util_match_query_mt(),util_match_query_f,&ThePgwManager);
	Vty->install_command(show_pdp_records_mt(), show_pdp_records_f, &ThePpdManager);
	Vty->install_command(show_pdp_records_tolog_mt(), show_pdp_records_tolog_f, &ThePpdManager);
	Vty->install_command(show_pdp_records_imsi_mt(), show_pdp_records_imsi_f, &ThePpdManager);
	Vty->install_command(change_loglevel_mt(), change_loglevel_f, nullptr);
	Vty->install_command(change_logcategory_mt(), change_logcategory_f, nullptr);
	Vty->install_command(change_pdp_records_remove_key_mt(), change_pdp_records_remove_key_f, &ThePpdManager);
	Vty->install_command(change_pdp_records_remove_imsi_mt(), change_pdp_records_remove_imsi_f, &ThePpdManager);
	Vty->install_command(change_recovery_counter_mt(), change_recovery_counter_f, nullptr);

	if(!Test())
		return -1;

    INFO(("---------------------------- Starting %s %s ----------------------\n", CMDLINE_PARSER_PACKAGE, CMDLINE_PARSER_VERSION));

    while(RunProxyRun)
    {
    	idleTime.tv_sec = SOCKET_IDLE_TIMOUT_SEC;
    	idleTime.tv_usec = SOCKET_IDLE_TIMOUT_MUSEC;
    	maxfd = 0;

    	FD_ZERO(&fds);

    	SET_FD(fd_sgwc);
    	SET_FD(fd_sgwu);
    	SET_FD(fd_pgwc);
    	SET_FD(fd_pgwu);
    	SET_FD(fd_timer)

    	std::vector<int> fd_arr;
    	Vty->fill_fd_array(fd_arr);
    	for(unsigned int i=0;i<fd_arr.size();i++)
    		SET_FD(fd_arr[i]);

    	if(maxfd)
    	{
			switch (select(maxfd + 1, &fds, NULL, NULL, &idleTime))
			{
				case -1:	//EINTR; unblocked signal
					ERROR(("select() problem %d - %s \n", errno, strerror(errno)));
					FD_ZERO(&fds);//make sure not to handle anything
					break;
				case 0:	//select timeout on SOCKET_IDLE_TIMOUT_SEC & SOCKET_IDLE_TIMOUT_MUSEC
					continue;
					break;
				default:	//pending data, vty or timer
					break;
			}
    	}

    	//Handle reception on the S-GW side that need to be proxy'd to the P-GW remote side
        if(FD_ISSET(fd_sgwc, &fds))
        {
        	TheMessageProcessor.HandleControlMessage(fd_sgwc, sgw_c_local_addr, fd_pgwc, pgw_c_local_addr, ProxyDirection::TO_PGW_REMOTE);
        	PacketCount++;
        }
        if(FD_ISSET(fd_sgwu, &fds))
        {
        	TheMessageProcessor.HandleUserDataMessage(fd_sgwu, sgw_u_local_addr, fd_pgwu, pgw_u_local_addr, ProxyDirection::TO_PGW_REMOTE);
			PacketCount++;
		}
    	//Handle reception on the P-GW side that need to be proxy'd to the S-GW remote side
        if(FD_ISSET(fd_pgwc, &fds))
        {
        	TheMessageProcessor.HandleControlMessage(fd_pgwc, pgw_c_local_addr, fd_sgwc, sgw_c_local_addr, ProxyDirection::TO_SGW_REMOTE);
        	PacketCount++;
        }
        if(FD_ISSET(fd_pgwu, &fds))
        {
        	TheMessageProcessor.HandleUserDataMessage(fd_pgwu, pgw_u_local_addr, fd_sgwu, sgw_u_local_addr, ProxyDirection::TO_SGW_REMOTE);
        	PacketCount++;
        }
        if(FD_ISSET(fd_timer, &fds))
        {
            size_t cnt = 0;
            if(-1 != read(fd_timer, &cnt, sizeof(cnt)))
            {
				//DEBUG(("Periodic timer triggered %u\n",cnt));
				HandleTimer(PacketCount, fd_pgwc, TheMessageProcessor, ThePpdManager, ThePgwManager, ai.configfile_arg);
			}
        }
    	for(unsigned int i=0;i<fd_arr.size();i++)
    	{
    		if(FD_ISSET(fd_arr[i],&fds))
    		{
    			Vty->poll_dispatch();
    			break;//one call to Vty->poll_dispatch() is sufficient
    		}
    	}
    }

    cmdline_parser_free(&ai);
    close(fd_sgwc);
    close(fd_sgwu);
    close(fd_pgwc);
    close(fd_pgwu);
    close(fd_timer);
    delete Vty;

    INFO(("---------------------------- Terminated %s ----------------------\n", CMDLINE_PARSER_PACKAGE));
	return 0;
}


#ifdef TESTONLY
#include <assert.h>
#include "gtppacket.h"
using namespace gtppacket;

bool Test()
{
	PgwManager ThePgwManager;
	Configuration Config("./config/gtpproxy.cfg");
	if(!ThePgwManager.Configure(Config))
		return false;

	GatewayAddress Pgw;
	int PacketGatewayIndex;
	ThePgwManager.AssignPgw("1234567890123", "apn1", Pgw, PacketGatewayIndex);
	ThePgwManager.AssignPgw("2234567890123", "apn1", Pgw, PacketGatewayIndex);
	ThePgwManager.AssignPgw("3234567890123", "apn1", Pgw, PacketGatewayIndex);
	ThePgwManager.AssignPgw("4234567890123", "apn1", Pgw, PacketGatewayIndex);
	ThePgwManager.AssignPgw("5234567890123", "apn1", Pgw, PacketGatewayIndex);

	PdpManager ThePpdManager;

	ThePpdManager.Advise(&ThePgwManager);
	MessageProcessor TheMessageProcessor(&ThePgwManager, &ThePpdManager);

	PdpRecord record;

	if(!record.SetVersion(GTP_VERSION_1))
		INFO(("Can't set version\n",record.ToString().c_str()));

	INFO(("%s\n",record.ToString().c_str()));
	INFO(("%s\n",record.ToVtyString(0).c_str()));

	//inet_ntoa can't be used more than once as parameter providing a string to a string format function
	std::string A,B;
	A = FormatStr("%s port %u",inet_ntoa(pgw_c_local_addr.sin_addr), htons(pgw_c_local_addr.sin_port));
	B = FormatStr("%s port %u",inet_ntoa(pgw_u_local_addr.sin_addr), htons(pgw_u_local_addr.sin_port));
	INFO(("Proxy P-GW local address C plane %s - U Plane %s\n", A.c_str(), B.c_str()));


	unsigned char* buffer = NULL;
	size_t buffersize = 0;
	unsigned char* iedata = NULL;
	size_t ielength = 0;
	int instance = 0;

	unsigned char pdnaddress[] = { 0x01, 0x01, 0x01, 0x01 };
	memcpy(pdnaddress , &(sgw_u_local_addr.sin_addr), sizeof(pdnaddress));

	pdnaddress[0] = 0xff;
	memcpy(pdnaddress , &(pgw_u_local_addr.sin_addr), sizeof(pdnaddress));

	uint32_t Teid = 0x12345678;
	TheMessageProcessor.SendDataPlaneErrorIndicationMessage(Teid, -1, sgw_u_local_addr, pgw_u_local_addr);

	TheMessageProcessor.SendDataPlaneErrorIndicationMessage(Teid, -1, sgw_u_local_addr, pgw_u_local_addr);
	TheMessageProcessor.SendErrorResponseMessage(-1, sgw_u_local_addr, gtppacket::GTP_VERSION_1, GTPV1_CREATE_PDP_REQ, 456/*Sequence*/, 65/*CauseValue*/);
	TheMessageProcessor.SendErrorResponseMessage(-1, sgw_u_local_addr, gtppacket::GTP_VERSION_2, GTPV2_CREATE_SESSION_REQUEST, 123/*Sequence*/, 32/*CauseValue*/);

	//////
	unsigned char gtpu_msg_err_ind[] = {
			0x32, 0x1a, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x15, 0x2c, 0x85, 0x00, 0x04, 0x0a, 0x63, 0x05, 0xf3
	};

	buffer = gtpu_msg_err_ind;
	buffersize = sizeof(gtpu_msg_err_ind);

	if(checkGtpPacket(buffer, buffersize, true/*log*/) == false)
	{
		ERROR(("checkGtpPacket failed\n"));
	}
	else
	{
		INFO(("checkGtpPacket passed\n"));
	}
	PRINT_DUMP((buffer,buffersize));

	TheMessageProcessor.ProcessErrorIndicationMessage(Teid, ProxyDirection::TO_PGW_REMOTE, buffer, buffersize, -1, sgw_u_local_addr);


	//////
	unsigned char gtpv1_echo_req_resp[] = {
			0x32, 0x02, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0xea, 0x13, 0x00, 0x00, 0x0e, 0xac
	};

	buffer = gtpv1_echo_req_resp;
	buffersize = sizeof(gtpv1_echo_req_resp);

	if(checkGtpPacket(buffer, buffersize, true/*log*/) == false)
	{
		ERROR(("checkGtpPacket failed\n"));
	}
	else
	{
		INFO(("checkGtpPacket passed\n"));
	}
	PRINT_DUMP((buffer,buffersize));

	TheMessageProcessor.ProcessEchoResponseMessage(GTP_VERSION_1, buffer, buffersize, sgw_u_local_addr);

	//////
	unsigned char gtpc_msg_cre_pdp_ctx_req[] = {
			0x32, 0x10, 0x00, 0xab, 0x00, 0x00, 0x00, 0x00, 0x1a, 0x9f, 0x00, 0x00, 0x02, 0x42, 0x00, 0x67,
			0x05, 0x09, 0x00, 0x00, 0xf4, 0x03, 0x24, 0xf5, 0x10, 0xff, 0xfe, 0xff, 0x0e, 0x0f, 0x0f, 0xfc,
			0x10, 0x13, 0xde, 0x20, 0x99, 0x11, 0x13, 0xde, 0x20, 0x97, 0x14, 0x05, 0x1a, 0x02, 0x00, 0x80,
			0x00, 0x02, 0xf1, 0x21, 0x83, 0x00, 0x12, 0x09, 0x74, 0x32, 0x74, 0x68, 0x75, 0x6e, 0x64, 0x65,
			0x72, 0x02, 0x63, 0x63, 0x04, 0x74, 0x6d, 0x67, 0x73, 0x84, 0x00, 0x14, 0x80, 0x80, 0x21, 0x10,
			0x01, 0x01, 0x00, 0x10, 0x81, 0x06, 0x00, 0x00, 0x00, 0x00, 0x83, 0x06, 0x00, 0x00, 0x00, 0x00,
			0x85, 0x00, 0x04, 0x52, 0x66, 0xbf, 0x0e, 0x85, 0x00, 0x04, 0x52, 0x66, 0xbf, 0x0b, 0x86, 0x00,
			0x09, 0x91, 0x64, 0x17, 0x19, 0x00, 0x53, 0x42, 0x97, 0xf2, 0x87, 0x00, 0x0f, 0x01, 0x0b, 0x91,
			0x1f, 0x73, 0x96, 0xfe, 0xfe, 0x74, 0x81, 0x78, 0x78, 0x00, 0x4f, 0x00, 0x94, 0x00, 0x01, 0x40,
			0x97, 0x00, 0x01, 0x01, 0x98, 0x00, 0x08, 0x01, 0x24, 0xf5, 0x10, 0x27, 0xd1, 0x87, 0x1c, 0x99,
			0x00, 0x02, 0x21, 0x21, 0x9a, 0x00, 0x08, 0x53, 0x17, 0x69, 0x50, 0x64, 0x94, 0x44, 0x10, 0xbf,
			0x00, 0x01, 0x44};

	buffer = gtpc_msg_cre_pdp_ctx_req;
	buffersize = sizeof(gtpc_msg_cre_pdp_ctx_req);

	if(checkGtpPacket(buffer, buffersize, true/*log*/) == false)
	{
		ERROR(("checkGtpPacket failed\n"));
	}
	else
	{
		INFO(("checkGtpPacket passed\n"));
	}
	PRINT_DUMP((buffer,buffersize));

	for(unsigned int instance=0;instance<2;instance++)
	{
		iedata = NULL;
		ielength = 0;
		if(getGtpv1Ie(buffer, buffersize, GTPV1_IE_GSN_ADDR, instance, iedata, ielength))
		{
			std::string Value;
			HexDumpToStr(iedata,(unsigned int)ielength,Value);
			DEBUG(("Get GSN Address Instance %d Val:%s\n", instance, Value.c_str()));

			mempcpy(iedata,pdnaddress,ielength);
		}
		else
			DEBUG(("Could not get GSN Address Instance %d\n", instance));
	}
	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));

	if(getGtpv1Ie(buffer, buffersize, GTPV1_IE_IMSI, 0, iedata, ielength) && ielength == 8)
	{
		PdpRecord pdpRecord;
		pdpRecord.IMSI = htobe64(*(uint64_t*)iedata);
		ThePpdManager.SetPgwAddressIfNeeded(pdpRecord);
	}

	//////
	unsigned char gtpc_msg_cre_pdp_ctx_rsp[] = {
			//0x32, 0x11, 0x00, 0x59, 0x13, 0xde, 0x20, 0x97, 0x1a, 0x9f, 0x00, 0x00, 0x01, 0x80, 0x08, 0x00,
			//With header extensions PDCP PDU Number & MBMS support indication
			0x36, 0x11, 0x00, 0x61, 0x13, 0xde, 0x20, 0x97, 0x1a, 0x9f, 0x00, 0xC0, 0x01, 0x00, 0x01, 0x01, 0x01, 0xFF, 0xFF, 0x00, 0x01, 0x80, 0x08, 0x00,
			0x0e, 0x01, 0x10, 0x00, 0x00, 0x00, 0x01, 0x11, 0x00, 0x00, 0x00, 0x01, 0x7f, 0x00, 0x00, 0x00,
			0x01, 0x80, 0x00, 0x06, 0xf1, 0x21, 0x0a, 0x96, 0xff, 0xc0, 0x84, 0x00, 0x14, 0x80, 0x80, 0x21,
			0x10, 0x02, 0x00, 0x00, 0x10, 0x81, 0x06, 0x0a, 0x02, 0x00, 0x2e, 0x83, 0x06, 0x0a, 0x02, 0x00,
			0x2e, 0x85, 0x00, 0x04, 0x7f, 0x00, 0x00, 0x01, 0x85, 0x00, 0x04, 0x7f, 0x00, 0x00, 0x01, 0x87,
			0x00, 0x0f, 0x01, 0x0b, 0x91, 0x1f, 0x73, 0x96, 0xfe, 0xfe, 0x74, 0x81, 0x78, 0x78, 0x00, 0x4f,
			0x00 };

	buffer = gtpc_msg_cre_pdp_ctx_rsp;
	buffersize = sizeof(gtpc_msg_cre_pdp_ctx_rsp);

	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));

	for(unsigned int instance=0;instance<2;instance++)
	{
		iedata = NULL;
		ielength = 0;
		if(getGtpv1Ie(buffer, buffersize, GTPV1_IE_GSN_ADDR, instance, iedata, ielength))
		{
			std::string Value;
			HexDumpToStr(iedata,(unsigned int)ielength,Value);
			DEBUG(("Get GSN Address Instance %d Val:%s\n", instance, Value.c_str()));

			mempcpy(iedata,pdnaddress,ielength);
		}
		else
			DEBUG(("Could not get GSN Address Instance %d\n", instance));
	}

	//re-write GTPV1_IE_RECOVERY
	iedata = NULL;
	ielength = 0;
	if(getGtpv1Ie(buffer, buffersize, GTPV1_IE_RECOVERY, 0, iedata, ielength))
	{
		*iedata = (unsigned char)RestartCounter::RecoveryCounter;
	}

	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));

	//////
	unsigned char gtp_msg_del_pdp_ctx_req[] = {
			0x32, 0x14, 0x00, 0x08, 0x00, 0x00, 0x00, 0x86,
			0x72, 0x03, 0xff, 0x00, 0x13, 0xff, 0x14, 0x05};

	buffer = gtp_msg_del_pdp_ctx_req;
	buffersize = sizeof(gtp_msg_del_pdp_ctx_req);

	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));

	//////
	unsigned char gtp_msg_del_pdp_ctx_rsp[] = {
			0x32, 0x15, 0x00, 0x06, 0x7b, 0x04, 0x3d, 0xcc,
			0x72, 0x03, 0x00, 0x00, 0x01, 0x80};

	buffer = gtp_msg_del_pdp_ctx_rsp;
	buffersize = sizeof(gtp_msg_del_pdp_ctx_rsp);

	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));

	//////
	unsigned char gtp_msg_upd_pdp_ctx_req[] = {
	  0x32, 0x12, 0x00, 0x41, 0x00, 0x00, 0x00, 0x01,
	  0xd3, 0x6a, 0x00, 0x00, 0x10, 0x80, 0xb2, 0x60,
	  0xf3, 0x14, 0x05, 0x85, 0x00, 0x04, 0xd9, 0x74,
	  0x60, 0x21, 0x85, 0x00, 0x04, 0xd9, 0x74, 0x60,
	  0x21, 0x87, 0x00, 0x0d, 0x01, 0x0b, 0x13, 0x01,
	  0x73, 0x96, 0x73, 0x73, 0x44, 0x42, 0xff, 0xff,
	  0x00, 0x94, 0x00, 0x01, 0x20, 0x97, 0x00, 0x01,
	  0x02, 0x98, 0x00, 0x08, 0x00, 0x62, 0xf0, 0x30,
	  0xce, 0xa5, 0x00, 0xbf, 0x99, 0x00, 0x02, 0x80,
	  0x01
	};

	buffer = gtp_msg_upd_pdp_ctx_req;
	buffersize = sizeof(gtp_msg_upd_pdp_ctx_req);

	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));

	//////
	unsigned char gtp_msg_upd_pdp_ctx_rsp[] = {
	  0x32, 0x13, 0x00, 0x37, 0x80, 0xe1, 0x40, 0xf3,
	  0xd3, 0x6a, 0x00, 0x00, 0x01, 0x80, 0x0e, 0x4f,
	  0x10, 0x00, 0x00, 0x00, 0x01, 0x11, 0x00, 0x00,
	  0x00, 0x01, 0x7f, 0x00, 0x00, 0x00, 0x01, 0x85,
	  0x00, 0x04, 0x3e, 0x5a, 0x43, 0x55, 0x85, 0x00,
	  0x04, 0x3e, 0x5a, 0x43, 0x55, 0x87, 0x00, 0x0f,
	  0x01, 0x0a, 0x13, 0x01, 0x73, 0x96, 0xfe, 0xfe,
	  0x44, 0x42, 0xff, 0xff, 0x00, 0x4a, 0x00
	};

	buffer = gtp_msg_upd_pdp_ctx_rsp;
	buffersize = sizeof(gtp_msg_upd_pdp_ctx_rsp);

	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));

	//////
	unsigned char gtp2_msg_create_sess_req[] = {
	  0x48, 0x20, 0x00, 0xf7, 0x00, 0x00, 0x00, 0x00,
	  0x50, 0xcd, 0x60, 0x00, 0x01, 0x00, 0x08, 0x00,
	  0x24, 0x05, 0x02, 0x93, 0x06, 0x00, 0x00, 0xf5,
	  0x4c, 0x00, 0x06, 0x00, 0x79, 0x52, 0x22, 0x10,
	  0x20, 0x05, 0x4b, 0x00, 0x08, 0x00, 0x68, 0x05,
	  0x06, 0x30, 0x26, 0x93, 0x70, 0x00, 0x56, 0x00,
	  0x0d, 0x00, 0x18, 0x24, 0xf5, 0x20, 0x03, 0x53,
	  0x24, 0xf5, 0x20, 0x00, 0x3a, 0x12, 0x21, 0x53,
	  0x00, 0x03, 0x00, 0x24, 0xf5, 0x20, 0x52, 0x00,
	  0x01, 0x00, 0x06, 0x57, 0x00, 0x09, 0x00, 0x8a,
	  0x80, 0x0b, 0xd2, 0xbe, 0xac, 0x11, 0xe0, 0x14,
	  0x57, 0x00, 0x09, 0x01, 0x87, 0x00, 0x00, 0x00,
	  0x00, 0xb0, 0x0c, 0x9e, 0x03, 0x47, 0x00, 0x1b,
	  0x00, 0x07, 0x73, 0x70, 0x68, 0x6f, 0x6e, 0x65,
	  0x72, 0x06, 0x6d, 0x6e, 0x63, 0x30, 0x30, 0x32,
	  0x06, 0x6d, 0x63, 0x63, 0x34, 0x32, 0x35, 0x04,
	  0x67, 0x70, 0x72, 0x73, 0x80, 0x00, 0x01, 0x00,
	  0xfd, 0x63, 0x00, 0x01, 0x00, 0x01, 0x4f, 0x00,
	  0x05, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x7f,
	  0x00, 0x01, 0x00, 0x00, 0x48, 0x00, 0x08, 0x00,
	  0x00, 0x1e, 0x84, 0x80, 0x00, 0x1e, 0x84, 0x80,
	  0x4e, 0x00, 0x20, 0x00, 0x80, 0x80, 0x21, 0x10,
	  0x01, 0x00, 0x00, 0x10, 0x81, 0x06, 0x00, 0x00,
	  0x00, 0x00, 0x83, 0x06, 0x00, 0x00, 0x00, 0x00,
	  0x00, 0x0d, 0x00, 0x00, 0x0a, 0x00, 0x00, 0x05,
	  0x00, 0x00, 0x10, 0x00, 0x5d, 0x00, 0x1f, 0x00,
	  0x49, 0x00, 0x01, 0x00, 0x05, 0x50, 0x00, 0x16,
	  0x00, 0x45, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x72,
	  0x00, 0x02, 0x00, 0x21, 0x01, 0x5f, 0x00, 0x02,
	  0x00, 0x04, 0x00
	};

	buffer = gtp2_msg_create_sess_req;
	buffersize = sizeof(gtp2_msg_create_sess_req);

	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));

	u_int32_t SequenceNumber = getGtpHeaderSequenceNumber(buffer, buffersize);
	setGtpHeaderSequenceNumber(buffer, buffersize, SequenceNumber);
	assert(SequenceNumber == getGtpHeaderSequenceNumber(buffer, buffersize));

	for(unsigned int i=0;i<9;i++)
	{
		iedata = NULL;
		ielength = 0;
		instance = i;
		if(getGtpv2Ie(buffer, buffersize, GTPV2_IE_FTEID, instance, iedata, ielength))
		{
			std::string Value;
			HexDumpToStr(iedata,(unsigned int)ielength,Value);
			DEBUG(("Get FTEID Instance %d Val:%s\n", instance, Value.c_str()));
			iedata++;//jump over type byte
			*(uint32_t*)iedata = ntohl(ThePgwManager.MakeTeid(-1, ProxyDirection::TO_PGW_REMOTE));
		}
		else
			DEBUG(("Could not get FTEID Instance %d\n",instance));
	}

	iedata = NULL;
	ielength = 0;
	instance = -1;//get the first
	if(getGtpv2Ie(buffer, buffersize, GTPV2_IE_BEARER_CONTEXT, instance, iedata, ielength))
	{
		std::string Value;
		HexDumpToStr(iedata,(unsigned int)ielength,Value);
		DEBUG(("Get BEARER CONTEXT Instance %d Val:%s\n", instance, Value.c_str()));

		unsigned char* iedataNested = NULL;
		size_t ielengthNested = 0;
		int instanceNested = 0;

		for(unsigned int i=0;i<9;i++)
		{
			iedataNested = NULL;
			ielengthNested = 0;
			instanceNested = i;

			if(getGtpv2Ie(iedata, ielength, GTPV2_IE_FTEID, instanceNested, iedataNested, ielengthNested))
			{
				HexDumpToStr(iedataNested,(unsigned int)ielengthNested,Value);
				DEBUG(("Get FTEID Instance %d Val:%s\n", instanceNested, Value.c_str()));
				iedataNested++;//jump over type byte
				*(uint32_t*)iedataNested = ntohl(ThePgwManager.MakeTeid(-1, ProxyDirection::TO_PGW_REMOTE));
			}
			else
				DEBUG(("Could not get FTEID Instance %d\n",instanceNested));
		}
	}
	else
		DEBUG(("Could not get BEARER CONTEXT Instance %d\n",instance));

	checkGtpPacket(buffer, buffersize, true/*log*/);

	//////
	unsigned char gtp2_msg_create_sess_rsp[] = {
	  0x48, 0x21, 0x00, 0x8e, 0x80, 0x0b, 0xd2, 0xbe,
	  0x50, 0xcd, 0x60, 0x00, 0x02, 0x00, 0x02, 0x00,
	  0x10, 0x00, 0x57, 0x00, 0x09, 0x00, 0x8b, 0x0c,
	  0x71, 0x5c, 0x83, 0xb0, 0x0c, 0x9e, 0x03, 0x57,
	  0x00, 0x09, 0x01, 0x87, 0x0c, 0xb0, 0xe6, 0x7f,
	  0xb0, 0x0c, 0x9e, 0x03, 0x4f, 0x00, 0x05, 0x00,
	  0x01, 0xac, 0x11, 0xbf, 0x1b, 0x7f, 0x00, 0x01,
	  0x00, 0x00, 0x4e, 0x00, 0x2b, 0x00, 0x80, 0x80,
	  0x21, 0x10, 0x03, 0x00, 0x00, 0x10, 0x81, 0x06,
	  0xd4, 0x4c, 0x7f, 0x98, 0x83, 0x06, 0xd4, 0x4c,
	  0x7f, 0x99, 0x00, 0x0d, 0x04, 0xd4, 0x4c, 0x7f,
	  0x98, 0x00, 0x0d, 0x04, 0xd4, 0x4c, 0x7f, 0x99,
	  0x00, 0x05, 0x01, 0x02, 0x00, 0x10, 0x02, 0x05,
	  0xdc, 0x5d, 0x00, 0x25, 0x00, 0x49, 0x00, 0x01,
	  0x00, 0x05, 0x02, 0x00, 0x02, 0x00, 0x10, 0x00,
	  0x57, 0x00, 0x09, 0x00, 0x81, 0x62, 0xa8, 0x3e,
	  0xb3, 0xb0, 0x0c, 0x9e, 0x0d, 0x57, 0x00, 0x09,
	  0x02, 0x85, 0x62, 0xa8, 0x3e, 0xb3, 0xb0, 0x0c,
	  0x9e, 0x03
	};

	buffer = gtp2_msg_create_sess_rsp;
	buffersize = sizeof(gtp2_msg_create_sess_rsp);

	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));

	for(unsigned int i=0;i<9;i++)
	{
		iedata = NULL;
		ielength = 0;
		instance = i;
		if(getGtpv2Ie(buffer, buffersize, GTPV2_IE_FTEID, instance, iedata, ielength))
		{
			std::string Value;
			HexDumpToStr(iedata,(unsigned int)ielength,Value);
			DEBUG(("Get FTEID Instance %d Val:%s\n", instance, Value.c_str()));
			iedata++;//jump over type byte
			*(uint32_t*)iedata = ntohl(ThePgwManager.MakeTeid(-1, ProxyDirection::TO_SGW_REMOTE));
		}
		else
			DEBUG(("Could not get FTEID Instance %d\n",instance));
	}

	iedata = NULL;
	ielength = 0;
	instance = -1;//get the first
	if(getGtpv2Ie(buffer, buffersize, GTPV2_IE_BEARER_CONTEXT, instance, iedata, ielength))
	{
		std::string Value;
		HexDumpToStr(iedata,(unsigned int)ielength,Value);
		DEBUG(("Get BEARER CONTEXT Instance %d Val:%s\n", instance, Value.c_str()));

		unsigned char* iedataNested = NULL;
		size_t ielengthNested = 0;
		int instanceNested = 0;

		for(unsigned int i=0;i<9;i++)
		{
			iedataNested = NULL;
			ielengthNested = 0;
			instanceNested = i;

			if(getGtpv2Ie(iedata, ielength, GTPV2_IE_FTEID, instanceNested, iedataNested, ielengthNested))
			{
				HexDumpToStr(iedataNested,(unsigned int)ielengthNested,Value);
				DEBUG(("Get FTEID Instance %d Val:%s\n", instanceNested, Value.c_str()));
				iedataNested++;//jump over type byte
				*(uint32_t*)iedataNested = ntohl(ThePgwManager.MakeTeid(-1, ProxyDirection::TO_SGW_REMOTE));
			}
			else
				DEBUG(("Could not get FTEID Instance %d\n",instanceNested));
		}
	}
	else
		DEBUG(("Could not get BEARER CONTEXT Instance %d\n",instance));

	checkGtpPacket(buffer, buffersize, true/*log*/);

	//////
	unsigned char gtp2_msg_mod_bearer_req[] = {
	  0x48, 0x22, 0x00, 0x2b, 0x0c, 0x71, 0x5c, 0x83,
	  0x50, 0xde, 0x5b, 0x00, 0x57, 0x00, 0x09, 0x00,
	  0x8a, 0x80, 0x0b, 0xd2, 0xbe, 0xac, 0x11, 0xe0,
	  0x14, 0x5d, 0x00, 0x12, 0x00, 0x49, 0x00, 0x01,
	  0x00, 0x05, 0x57, 0x00, 0x09, 0x00, 0x80, 0x00,
	  0x02, 0x7b, 0xe9, 0xac, 0x1a, 0x08, 0x31
	};

	buffer = gtp2_msg_mod_bearer_req;
	buffersize = sizeof(gtp2_msg_mod_bearer_req);

	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));

	//////
	unsigned char gtp2_msg_mod_bearer_rsp[] = {
	  0x48, 0x23, 0x00, 0x2a, 0x80, 0x0b, 0xd2, 0xbe,
	  0x50, 0xde, 0x5b, 0x00, 0x02, 0x00, 0x02, 0x00,
	  0x10, 0x00, 0x5d, 0x00, 0x18, 0x00, 0x49, 0x00,
	  0x01, 0x00, 0x05, 0x02, 0x00, 0x02, 0x00, 0x10,
	  0x00, 0x57, 0x00, 0x09, 0x00, 0x81, 0x62, 0xa8,
	  0x3e, 0xb3, 0xb0, 0x0c, 0x9e, 0x0d
	};

	buffer = gtp2_msg_mod_bearer_rsp;
	buffersize = sizeof(gtp2_msg_mod_bearer_rsp);

	checkGtpPacket(buffer, buffersize, true/*log*/);
	PRINT_DUMP((buffer,buffersize));


	//return false;
	return true;
}
#else
bool Test()
{
	return true;
}
#endif
