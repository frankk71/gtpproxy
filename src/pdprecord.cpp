/*
 * packetgateway.cpp
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#include <string.h>

#include "pdprecord.h"

#include "defs.h"
#include "utils.h"
#include "globals.h"
#include "logger.h"
#include "interfaces.h"

PdpRecord::PdpRecord()
{
	m_PdpCreateRequestTime = 0;

	PdpCreateSgwSequenceNumber = (uint32_t)-1;
	PdpCreatePgwSequenceNumber = (uint32_t)-1;
	PdpLastSgwSequenceNumber = (uint32_t)-1;
	PdpLastPgwSequenceNumber = (uint32_t)-1;

	LastSgwActivityTime = 0;
	LastPgwActivityTime = 0;
	SgwData = PgwData = 0;

	memset(&SgwAddrPdpResponseAddress,0,sizeof(struct sockaddr_in));

	SgwTeidControlPlane = (uint32_t)-1;//inbound Create Request from SGW
	SgwTeidControlPlaneMapped = (uint32_t)-1;//outbound Create Request to PGW

	PgwTeidControlPlane = (uint32_t)-1;//inbound Create Response from PGW
	PgwTeidControlPlaneMapped = (uint32_t)-1;//outbound Create Response to SGW

	IMSI = 0;

	RemoveMeTime = 0;

	PacketGateway = -1;

	m_GtpVersion = gtppacket::GTP_VERSION_UNKNOWN;
	KeyValue = -1;

	m_PdpState = PDP_STATE_UNKNOWN;
	m_PdpStateTime = 0;
	RatType = 0;

	ModificationCount = 0;

	TimerId = 0;
}

void PdpRecord::SetPdpState(PDP_STATE NewState, PdpManagerTimerInterface* PdpTimerManager)
{
	m_PdpState = NewState;
	time(&m_PdpStateTime);

	switch(m_PdpState)
	{
	default:
	case PDP_STATE_CREATED:
		//TODO/TBD whether to reset pdpRecord.PdpCreateSgwSequenceNumber to mark the reception of the outstanding response
		PdpCreateSgwSequenceNumber = (uint32_t)-1;
		//PdpCreatePgwSequenceNumber = (uint32_t)-1;//used for tracking of PGW D-TEID
		RemoveMeTime = 0;
		//((PdpManagerTimerInterface*)PdpTimerManager)->StopTimerOnRecord(TimerId);
		//timer ideal-wise bumped on activity
		if(PdpTimerManager)PdpTimerManager->StartTimerOnRecord(TimerId, KeyValue, time(NULL) + std::max((unsigned)1,g_GtpSessionCreateRequestTimeout));
		break;
	case PDP_STATE_CREATE_REQESTED:
		time(&m_PdpCreateRequestTime);
		//Remove session after linger timeout when there is no response for the create request
		RemoveMeTime = m_PdpCreateRequestTime;
		RemoveMeTime += std::max((unsigned)1,g_GtpSessionCreateRequestTimeout);
		if(PdpTimerManager)PdpTimerManager->StartTimerOnRecord(TimerId, KeyValue, RemoveMeTime);
		break;
	case PDP_STATE_CREATE_REJECTED:
		time(&RemoveMeTime);
		RemoveMeTime += std::max((unsigned)1,g_GtpSessionCreateRequestRejectTimeout);
		if(PdpTimerManager)PdpTimerManager->StartTimerOnRecord(TimerId, KeyValue, RemoveMeTime);
		break;
	case PDP_STATE_DELETE_REQUESTED:
	case PDP_STATE_DELETED:
		time(&RemoveMeTime);
		RemoveMeTime += std::max((unsigned)1,g_GtpSessionDeleteRequestTimeout);
		if(PdpTimerManager)PdpTimerManager->StartTimerOnRecord(TimerId, KeyValue, RemoveMeTime);
		break;
	}
}

void PdpRecord::ReportSgwData(uint32_t ByteCount, PdpManagerTimerInterface* PdpTimerManager, time_t AcivityTimeout)
{
	SgwData += ByteCount;
	time(&LastSgwActivityTime);
	if(PdpTimerManager)PdpTimerManager->StartTimerOnRecord(TimerId, KeyValue, LastSgwActivityTime + AcivityTimeout);
}

void PdpRecord::ReportPgwData(uint32_t ByteCount, PdpManagerTimerInterface* PdpTimerManager, time_t AcivityTimeout)
{
	PgwData += ByteCount;
	time(&LastPgwActivityTime);
	if(PdpTimerManager)PdpTimerManager->StartTimerOnRecord(TimerId, KeyValue, LastPgwActivityTime + AcivityTimeout);
}

const char* PdpRecord::GetPdpStateStr(const PDP_STATE Val) const
{
	switch(Val)
	{
	case PDP_STATE_CREATE_REQESTED:
		return "PDP_STATE_CREATE_REQESTED";
		break;
	case PDP_STATE_CREATE_REJECTED:
		return "PDP_STATE_CREATE_REJECTED";
		break;
	case PDP_STATE_CREATED:
		return "PDP_STATE_CREATED";
		break;
	case PDP_STATE_DELETE_REQUESTED:
		return "PDP_STATE_DELETE_REQUESTED";
		break;
	case PDP_STATE_DELETED:
		return "PDP_STATE_DELETED";
		break;
	default:
		return "PDP_STATE_UNKNOWN";
		break;
	}
}

std::string PdpRecord::ToString() const
{
	std::string SgwControlPlaneAddr = inet_ntoa(Sgw.c_plane_addr.sin_addr);
	std::string SgwDataPlaneAddr = inet_ntoa(Sgw.u_plane_addr.sin_addr);
	std::string PgwControlPlaneAddr = inet_ntoa(Pgw.c_plane_addr.sin_addr);
	std::string PgwDataPlaneAddr = inet_ntoa(Pgw.u_plane_addr.sin_addr);
	std::string ImsiString;
	uint64_t ImsiNetworkOrder = htobe64(IMSI);
	HexDumpToStr((unsigned char*)&ImsiNetworkOrder,sizeof(uint64_t),ImsiString,true/*reversenibble*/);
	time_t Now = time(NULL);

	std::string SequenceTrack = "";
	if(PdpCreateSgwSequenceNumber != (uint32_t)-1 || PdpCreatePgwSequenceNumber != (uint32_t)-1)
		SequenceTrack += FormatStr("S-GW CSeq:%u-0x%06x P-GW CSeq:%u-0x%06x ",
			PdpCreateSgwSequenceNumber, PdpCreateSgwSequenceNumber, PdpCreatePgwSequenceNumber, PdpCreatePgwSequenceNumber);
	if(PdpLastSgwSequenceNumber != (uint32_t)-1 || PdpLastPgwSequenceNumber != (uint32_t)-1)
		SequenceTrack += FormatStr("S-GW Seq:%u-0x%06x P-GW Seq:%u-0x%06x ",
				PdpLastSgwSequenceNumber, PdpLastSgwSequenceNumber, PdpLastPgwSequenceNumber, PdpLastPgwSequenceNumber);
	if(SequenceTrack.empty())
		SequenceTrack += "No pending response messages ";

	std::string DPlaneTeidInfo = DataPlaneContextMap.empty() ? "No D-TEID Set" : "";
	for(TeidMapTypeConstIter Iter = DataPlaneContextMap.begin(), EndIter = DataPlaneContextMap.end();Iter!=EndIter;Iter++)
	{
		DPlaneTeidInfo += FormatStr("NSAPI_EBI:%u S-GW TEID:0x%08x<->0x%08x Data:%u Err:%u P-GW TEID:0x%08x<->0x%08x Data:%u Err:%u ",Iter->first,
				Iter->second.SgwTeidDataPlane, Iter->second.SgwTeidDataPlaneMapped, Iter->second.SgwData, Iter->second.SgwTeidDataPlaneError,
				Iter->second.PgwTeidDataPlane, Iter->second.PgwTeidDataPlaneMapped, Iter->second.PgwData, Iter->second.PgwTeidDataPlaneError);
	}

	return FormatStr("Key:%u IMSI:%08llx->%s APN:%s EUA:%s RAT:%u-%s"
			"CreateReq:%s State:%s Since:%s ModCnt[%u] PGW#%d %s "
			"RemoveTime:%u %d sec ActivityTime S-GW/P-GW:%u[%d sec] %u[%d sec] "
			"SeqTrack:%s"
			"C-Plane: S-GW:%s-%u<->P-GW:%s-%u "
			"S-GW TEID:0x%08x<->0x%08x P-GW TEID:0x%08x<->0x%08x "
			"D-Plane: S-GW:%s-%u Data:%u <-> P-GW:%s-%u Data:%u "
			"%s",
			KeyValue, IMSI, ImsiString.c_str(), APN.c_str(), EUA.c_str(), RatType, gtppacket::getRatTypeStr(RatType),
			FormatLocalTime(m_PdpCreateRequestTime).c_str(), GetPdpStateStr(m_PdpState), FormatLocalTime(m_PdpStateTime).c_str(),
			ModificationCount, PacketGateway+1, gtppacket::getGtpVersionStr(m_GtpVersion),
			RemoveMeTime, RemoveMeTime ? RemoveMeTime - Now : 0,
			LastSgwActivityTime, LastSgwActivityTime ? LastSgwActivityTime - Now : 0,
			LastPgwActivityTime, LastPgwActivityTime ? LastPgwActivityTime - Now : 0,
			SequenceTrack.c_str(),
			SgwControlPlaneAddr.c_str(), htons(Sgw.c_plane_addr.sin_port), PgwControlPlaneAddr.c_str(), htons(Pgw.c_plane_addr.sin_port),
			SgwTeidControlPlane, SgwTeidControlPlaneMapped, PgwTeidControlPlane, PgwTeidControlPlaneMapped,
			SgwDataPlaneAddr.c_str(), htons(Sgw.u_plane_addr.sin_port), SgwData, PgwDataPlaneAddr.c_str(), htons(Pgw.u_plane_addr.sin_port), PgwData,
			DPlaneTeidInfo.c_str()
	);
}

std::string PdpRecord::ToVtyString(time_t Now) const
{
	std::string SgwControlPlaneAddr = inet_ntoa(Sgw.c_plane_addr.sin_addr);
	std::string SgwDataPlaneAddr = inet_ntoa(Sgw.u_plane_addr.sin_addr);
	std::string PgwControlPlaneAddr = inet_ntoa(Pgw.c_plane_addr.sin_addr);
	std::string PgwDataPlaneAddr = inet_ntoa(Pgw.u_plane_addr.sin_addr);
	std::string ImsiString;
	uint64_t ImsiNetworkOrder = htobe64(IMSI);
	HexDumpToStr((unsigned char*)&ImsiNetworkOrder,sizeof(uint64_t),ImsiString,true/*reversenibble*/);

	std::string SequenceTrack = "";
	if(PdpCreateSgwSequenceNumber != (uint32_t)-1 || PdpCreatePgwSequenceNumber != (uint32_t)-1)
		SequenceTrack += FormatStr("S-GW CSeq:%u-0x%06x P-GW CSeq:%u-0x%06x",
			PdpCreateSgwSequenceNumber, PdpCreateSgwSequenceNumber, PdpCreatePgwSequenceNumber, PdpCreatePgwSequenceNumber);
	if(PdpLastSgwSequenceNumber != (uint32_t)-1 || PdpLastPgwSequenceNumber != (uint32_t)-1)
		SequenceTrack += FormatStr(" S-GW Seq:%u-0x%06x P-GW Seq:%u-0x%06x",
				PdpLastSgwSequenceNumber, PdpLastSgwSequenceNumber, PdpLastPgwSequenceNumber, PdpLastPgwSequenceNumber);
	if(SequenceTrack.empty())
		SequenceTrack += "No pending response messages";

	std::string DPlaneTeidInfo = DataPlaneContextMap.empty() ? "\r\n\tNo D-TEID Set" : "";
	for(TeidMapTypeConstIter Iter = DataPlaneContextMap.begin(), EndIter = DataPlaneContextMap.end();Iter!=EndIter;Iter++)
	{
		DPlaneTeidInfo += FormatStr("\r\n\t\tNSAPI_EBI:%u\r\n\t\t\tS-GW TEID:0x%08x<->0x%08x Data:%u Err:%u\tP-GW TEID:0x%08x<->0x%08x Data:%u Err:%u",Iter->first,
				Iter->second.SgwTeidDataPlane, Iter->second.SgwTeidDataPlaneMapped, Iter->second.SgwData, Iter->second.SgwTeidDataPlaneError,
				Iter->second.PgwTeidDataPlane, Iter->second.PgwTeidDataPlaneMapped, Iter->second.PgwData, Iter->second.PgwTeidDataPlaneError);
	}

	return FormatStr("Key:%u IMSI:%08llx->%s APN:%s EUA:%s RAT:%u-%s\r\n\t"
			"CreateReq:%s State:%s Since:%s ModCnt[%u] PGW#%d %s\r\n\t"
			"RemoveTime:%s %d sec ActivityTime S-GW/P-GW:%s[%d sec] %s[%d sec]\r\n\t"
			"SeqTrack:%s\r\n\t"
			"C-Plane:\tS-GW:%s-%u<->P-GW:%s-%u\r\n\t"
			"\tS-GW TEID:0x%08x<->0x%08x P-GW TEID:0x%08x<->0x%08x\r\n\t"
			"D-Plane:\tS-GW:%s-%u Data:%u <-> P-GW:%s-%u Data:%u"
			"%s",
			KeyValue, IMSI, ImsiString.c_str(), APN.c_str(), EUA.c_str(), RatType, gtppacket::getRatTypeStr(RatType),
			FormatLocalTime(m_PdpCreateRequestTime).c_str(), GetPdpStateStr(m_PdpState), FormatLocalTime(m_PdpStateTime).c_str(),
			ModificationCount, PacketGateway+1, gtppacket::getGtpVersionStr(m_GtpVersion),
			RemoveMeTime ? FormatLocalTime(RemoveMeTime).c_str() : "not set", RemoveMeTime ? RemoveMeTime - Now : 0,
			LastSgwActivityTime ? FormatLocalTime(LastSgwActivityTime).c_str() : "never", LastSgwActivityTime ? LastSgwActivityTime - Now : 0,
			LastPgwActivityTime ? FormatLocalTime(LastPgwActivityTime).c_str() : "never", LastPgwActivityTime ? LastPgwActivityTime - Now : 0,
			SequenceTrack.c_str(),
			SgwControlPlaneAddr.c_str(), htons(Sgw.c_plane_addr.sin_port),
			PgwControlPlaneAddr.c_str(), htons(Pgw.c_plane_addr.sin_port),
			SgwTeidControlPlane, SgwTeidControlPlaneMapped, PgwTeidControlPlane, PgwTeidControlPlaneMapped,
			SgwDataPlaneAddr.c_str(), htons(Sgw.u_plane_addr.sin_port),
			SgwData,
			PgwDataPlaneAddr.c_str(), htons(Pgw.u_plane_addr.sin_port),
			PgwData,
			DPlaneTeidInfo.c_str()
	);
}
