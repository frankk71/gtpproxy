/*
 * configuration.cpp
 *
 *  Created on: Mar 16, 2019
 *      Author: JpU FK
 */


#include "configuration.h"

#include "logger.h"

//inet_aton
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <time.h>
#include <sys/stat.h>

struct stat ConfigFileStatAttr = {0};


#define MANDATORY_CFG_LOOKUP(Object,Token,Value,Index) \
	if(!Object.lookupValue(Token, Value)) \
	{ \
	    WARN(("Can't parse mandatory configuration parameter:%s in instance %d definition - ignoring instance\n", Token, Index)); \
	    Success = false; \
	}

#define LOCAL_ADDR_LOOKUP(Object,Token,Value,Destination) \
	if(Object.lookupValue(Token, Value))	\
	{ \
		DEBUG(("Read Interfaces configuration group %s:%s\n", Token,Value.c_str())); \
		inet_aton(Value.c_str(), &(Destination.sin_addr)); \
	} \
	else \
	{ \
		return false; \
	}

#define LOCAL_ADDR_PORT_LOOKUP(Object,Token,Value,Destination) \
	if(Object.lookupValue(Token, Value)) \
	{ \
		DEBUG(("Read Interfaces configuration group %s:%d\n", Token, Value)); \
		Destination.sin_port = htons((in_port_t)Value); \
	} \
	else \
	{ \
		return false; \
	}


Configuration::Configuration(const char* FileName /*= CONFIG_FILENAME*/)
{
	m_IsOpen = OpenConfig(FileName);
}

bool Configuration::OpenConfig(const char* FileName)
{
	try
	{
		cfg.readFile(FileName);
	}
	catch(const FileIOException &fioex)
	{
		ERROR(("I/O error while reading file %s\n", FileName));
		return false;
	}
#ifdef LIBCONFIGXX_VER_MAJOR
	catch(const ParseException &pex)
	{
		ERROR(("Parse error at %s Line %d - %s\n", pex.getFile(), pex.getLine(), pex.getError()));
		return false;
	}
#else
	//ParseException.getFile() doesn't exist and ParseException.getLine() is not const in libconfig-devel-1.3.2-1.1.2.amzn1.x86_64.rpm
	catch(ParseException &pex)
	{
		ERROR(("Parse error at %s Line %d - %s\n", FileName, pex.getLine(), pex.getError()));
		return false;
	}
#endif

	if(ConfigFileStatAttr.st_mtime == 0)//do this once
	{
		stat(FileName, &ConfigFileStatAttr);
		INFO(("%s Last modified time: %s", FileName, ctime(&ConfigFileStatAttr.st_mtime)));
	}

	m_FileName = FileName;

	return true;
}

bool Configuration::HasConfigFileChanged()
{
	if(!m_FileName.empty())
	{
		struct stat CurrentConfigFileStatAttr;
		stat(m_FileName.c_str(), &CurrentConfigFileStatAttr);

		return ConfigFileStatAttr.st_mtime != CurrentConfigFileStatAttr.st_mtime;
	}
	return false;
}

void Configuration::ConfigFileChangeHandled()
{
	if(!m_FileName.empty())
		stat(m_FileName.c_str(), &ConfigFileStatAttr);
}

unsigned int Configuration::getMatchingDomainConfigCount()
{
	if(m_IsOpen == false)
		return 0;

	Setting &root = cfg.getRoot();
	try
	{
		const Setting &mds = root[CONFIG_MD_GROUP];
		return (unsigned int)mds.getLength();
	}
	catch(const SettingNotFoundException &nfex)
	{
		WARN(("Can't find mandatory %s configuration group\n", CONFIG_MD_GROUP));
	}
	return 0;
}

bool Configuration::readMatchingDomainConfig(int Index, matchingDomainParams& Params)
{
	if(m_IsOpen == false)
		return false;

	Setting &root = cfg.getRoot();
	try
	{
		const Setting &mds = root[CONFIG_MD_GROUP];
		const Setting &md = mds[Index];

		// Only output the record if all of the expected fields are present.
		bool Success = true;

		//All parameters must exist. The macros set the Success variable to false should there be a missing parameter. Parsing can continue.
		MANDATORY_CFG_LOOKUP(md,"Domain", Params.domain, Index)
		MANDATORY_CFG_LOOKUP(md,"RegExMatchingAPN", Params.APN_regex, Index)
		MANDATORY_CFG_LOOKUP(md,"RegExMatchingIMSI", Params.IMSI_regex, Index)
		MANDATORY_CFG_LOOKUP(md,"MatchOnlyWhenConnected", Params.matchonlywhenconnected, Index)

		const Setting &pgwnames = md["PgwNames"];
		unsigned int pgwnamesLength = pgwnames.getLength();
		for(unsigned int i=0;i<pgwnamesLength;i++)
		{
			Params.pgw_names.push_back(pgwnames[i]);
		}

		DEBUG(("Read MatchingDomain:%s RegExMatchingAPN:%s RegExMatchingIMSI:%s MatchOnlyWhenConnected:%u PGWNames:%s\n",
				Params.domain.c_str(), Params.APN_regex.c_str(), Params.IMSI_regex.c_str(), Params.matchonlywhenconnected, VectorOfStringsToCsv(Params.pgw_names).c_str()));

		return Success;
	}
	catch(const SettingNotFoundException &nfex)
	{
		WARN(("Can't find MatchingDomain definition for Index:%d\n", Index));
	}
	return false;
};

unsigned int Configuration::getPgwConfigCount()
{
	if(m_IsOpen == false)
		return 0;

	Setting &root = cfg.getRoot();
	try
	{
		const Setting &pgws = root[CONFIG_PGW_GROUP];
		return (unsigned int)pgws.getLength();
	}
	catch(const SettingNotFoundException &nfex)
	{
		WARN(("Can't find mandatory %s configuration group\n", CONFIG_PGW_GROUP));
	}
	return 0;
}

bool Configuration::readPgwConfig(int Index, pgwParams& Params)
{
	if(m_IsOpen == false)
		return false;

	Setting &root = cfg.getRoot();
	try
	{
		const Setting &pgws = root[CONFIG_PGW_GROUP];
		const Setting &pgw = pgws[Index];

		// Only output the record if all of the expected fields are present.
		bool Success = true;

		//All parameters must exist. The macros set the Success variable to false should there be a missing parameter. Parsing can continue.
		MANDATORY_CFG_LOOKUP(pgw,"Name", Params.name, Index)
		MANDATORY_CFG_LOOKUP(pgw,"GTPEchoVersion", Params.version, Index)
		MANDATORY_CFG_LOOKUP(pgw,"GTPC-RemoteAddr", Params.remoteaddr_c, Index)
		MANDATORY_CFG_LOOKUP(pgw,"GTPC-RemotePort", Params.remoteport_c, Index)
		MANDATORY_CFG_LOOKUP(pgw,"GTPU-RemoteAddr", Params.remoteaddr_u, Index)
		MANDATORY_CFG_LOOKUP(pgw,"GTPU-RemotePort", Params.remoteport_u, Index)

		DEBUG(("Read Name:%s GTPC-RemoteAddress:%s GTPC-RemotePort:%d GTPU-RemoteAddress:%s GTPU-RemotePort:%d GTPEchoVersion:%d \n",
				Params.name.c_str(), Params.remoteaddr_c.c_str(), Params.remoteport_c, Params.remoteaddr_u.c_str(), Params.remoteport_u, Params.version));

		return Success;
	}
	catch(const SettingNotFoundException &nfex)
	{
		WARN(("Can't find PacketGateway definition for Index:%d\n", Index));
	}
	return false;
}

bool Configuration::readInterfaceConfig()
{
	if(m_IsOpen == false)
		return false;

	Setting &root = cfg.getRoot();

	try
	{
		const Setting &InterfaceParams = root[CONFIG_INTERFACES_GROUP];
		int intValue;
		std::string strValue;

		//All parameters must exist. The macros return false should there be a missing parameter
		LOCAL_ADDR_LOOKUP(InterfaceParams, "SGW-GTPC-BindIpAddr", strValue, m_localAddresses.sgw_c_local_addr)
		LOCAL_ADDR_PORT_LOOKUP(InterfaceParams, "SGW-GTPC-BindPort", intValue, m_localAddresses.sgw_c_local_addr)
		LOCAL_ADDR_LOOKUP(InterfaceParams, "SGW-GTPU-BindIpAddr", strValue, m_localAddresses.sgw_u_local_addr)
		LOCAL_ADDR_PORT_LOOKUP(InterfaceParams, "SGW-GTPU-BindPort", intValue, m_localAddresses.sgw_u_local_addr)
		LOCAL_ADDR_LOOKUP(InterfaceParams, "PGW-GTPC-BindIpAddr", strValue, m_localAddresses.pgw_c_local_addr)
		LOCAL_ADDR_PORT_LOOKUP(InterfaceParams, "PGW-GTPC-BindPort", intValue, m_localAddresses.pgw_c_local_addr)
		LOCAL_ADDR_LOOKUP(InterfaceParams, "PGW-GTPU-BindIpAddr", strValue, m_localAddresses.pgw_u_local_addr)
		LOCAL_ADDR_PORT_LOOKUP(InterfaceParams, "PGW-GTPU-BindPort", intValue, m_localAddresses.pgw_u_local_addr)

		return true;
	}
	catch(const SettingNotFoundException &nfex)
	{
		WARN(("Can't find mandatory Interfaces configuration group\n"));
	}
	return false;
}

bool Configuration::readLoggingConfig(unsigned char& DebugLogLevel, unsigned char& DebugCategory)
{
	int Value1, Value2;
	if(readIntConfig(CONFIG_LOGGING_GROUP, "LevelMask", Value1) && readIntConfig(CONFIG_LOGGING_GROUP, "CategoryMask", Value2))
	{
		DebugLogLevel = (unsigned char)Value1;
		DebugCategory = (unsigned char)Value2;
		return true;
	}
	return false;
}

bool Configuration::writeLoggingConfig(unsigned char DebugLogLevel, unsigned char DebugCategory)
{
	bool IsConfigFileChanged = HasConfigFileChanged();
	if(writeIntConfig(CONFIG_LOGGING_GROUP, "LevelMask", (int)DebugLogLevel) && writeIntConfig(CONFIG_LOGGING_GROUP, "CategoryMask", (int)DebugCategory))
	{
		//Note: this will remove all comments from the file
		cfg.writeFile(m_FileName.c_str());
		if(!IsConfigFileChanged)//not a pending external change that need to be handled
			ConfigFileChangeHandled();
		return true;
	}
	return false;
}

bool Configuration::readVtyConfig(unsigned short& VtyPort)
{
	int Value;
	if(readIntConfig(CONFIG_VTY_GROUP, "Port", Value))
	{
		VtyPort = (unsigned short) Value;
		return true;
	}
	return false;
}

bool Configuration::readStringConfig(const char* GroupName, const char* ValueName, std::string& Value)
{
	if(m_IsOpen == false)
		return false;

	Setting &root = cfg.getRoot();

	try
	{
		const Setting &vtyParams = root[GroupName];
		if(vtyParams.lookupValue(ValueName, Value))
			DEBUG(("Read %s configuration group %s:%s\n", GroupName, ValueName, Value.c_str()));
	}
	catch(const SettingNotFoundException &nfex)
	{
		INFO(("%s configuration group not present for reading\n", GroupName));
		return false;
	}
	return true;
}

bool Configuration::readIntConfig(const char* GroupName, const char* ValueName, int& Value)
{
	if(m_IsOpen == false)
		return false;

	Setting &root = cfg.getRoot();

	try
	{
		const Setting &vtyParams = root[GroupName];
		if(vtyParams.lookupValue(ValueName, Value))
			DEBUG(("Read %s configuration group %s:%d\n", GroupName, ValueName, Value));
	}
	catch(const SettingNotFoundException &nfex)
	{
		INFO(("%s configuration group not present for reading\n", GroupName));
		return false;
	}
	return true;
}

bool Configuration::writeIntConfig(const char* GroupName, const char* ValueName, int Value)
{
	if(m_IsOpen == false)
		return false;

	Setting &root = cfg.getRoot();

	try
	{
		const Setting &vtyParams = root[GroupName];
		vtyParams[ValueName] = Value;
		INFO(("Assigned %s configuration group %s:%d\n", GroupName, ValueName, Value));
	}
	catch(const SettingNotFoundException &nfex)
	{
		WARN(("%s configuration group %s not present for assignment\n", GroupName, ValueName));
		return false;
	}
	return true;
}
