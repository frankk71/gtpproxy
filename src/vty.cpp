/*
 * vty.cpp
 *
 *  Created on: Feb 25, 2019
 *      Author: JpU FK
 */

#include "vty.h"
#include "defs.h"
#include "globals.h"

vty::vty(uint32_t addr, uint16_t port)
{
	std::string str = FormatStr("\r\n"
	"Welcome to the %s %s of JpU - Copyright 2018,2019\r\n"
	"The application started at %s\r\n"
	"Type 'list' or '?' to see available commands and description\r\n"
	"Type <tab> to expand the command and use arrow keys to review command history\r\n"
	"NOTE: You can't paste into this terminal connection!\r\n"
	"\r\n",CMDLINE_PARSER_PACKAGE, CMDLINE_PARSER_VERSION, FormatLocalTime(g_ApplicationStartTime).c_str());

	v = new vty_server(addr, port, str.c_str(), "gtpp> ");
	install_command(exit_mt        (), exit_f        , nullptr);
	install_command(clear_mt       (), clear_f       , nullptr);
	install_command(list_mt        (), list_f        , nullptr);
	install_command(show_version_mt(), show_version_f, nullptr);
}

/* show_version */
vty_cmd_match show_version_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("show", "Display various information"));
  m.nodes.push_back(new node_fixedstring("version", "Shows the application version"));
  return m;
}
void show_version_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
  sh->Printf("%s %s\r\n", CMDLINE_PARSER_PACKAGE, CMDLINE_PARSER_VERSION);
  sh->Printf("Copyright 2018 JpU\r\n");
  sh->Printf("Started at %s\r\n",FormatLocalTime(g_ApplicationStartTime).c_str());
}

/* exit */
vty_cmd_match exit_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("exit", "Terminates the connection"));
  return m;
}
void exit_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
  sh->close();
}

/* clear */
vty_cmd_match clear_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("clear", "Clears the console"));
  return m;
}
void clear_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
  sh->Printf("\033[2J\r\n");
}

/* list */
vty_cmd_match list_mt()
{
  vty_cmd_match m;
  m.nodes.push_back(new node_fixedstring("list", "List commands"));
  return m;
}
void list_f(vty_cmd_match* m, vty_client* sh, void* arg)
{
  const std::vector<vty_cmd*>& commands = *sh->commands;
  for (vty_cmd* cmd : commands) {
    std::string s = "";
    for (node* nd : cmd->match.nodes) {
      s += nd->to_string();
      s += " ";
    }
    sh->Printf("  %s\r\n", s.c_str());
  }
}


