/*
 * globals.cpp
 *
 *  Created on: Feb 25, 2019
 *      Author: JpU FK
 */

#include  "globals.h"

//globals - for the time being
struct sockaddr_in sgw_c_local_addr, sgw_u_local_addr, pgw_c_local_addr, pgw_u_local_addr;

long long sgw_2_pgw_c_data_count = 0;
long long sgw_2_pgw_u_data_count = 0;
long long pgw_2_sgw_c_data_count = 0;
long long pgw_2_sgw_u_data_count = 0;

time_t g_ApplicationStartTime = 0;

unsigned int g_LogStatisticsTimeout = 60;//in seconds
unsigned int g_PgwEchoRequestTimeout = 10;//in seconds
unsigned int g_GtpSessionCreateRequestTimeout = 20;//in seconds
unsigned int g_GtpSessionCreateRequestRejectTimeout = 5;//in seconds
unsigned int g_GtpSessionDeleteRequestTimeout = 10;//in seconds
