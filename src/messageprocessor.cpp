/*
 * messageprocessor.cpp
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#include <string>
#include <vector>
#include <string.h>

#include "messageprocessor.h"

#include "restartcount.h"
#include "logger.h"
#include "defs.h"
#include "utils.h"
#include "packetgateway.h"
#include "globals.h"

using namespace gtppacket;

//TODO/TBD: Retrieve the highest P-GW recovery counter to report to the S-GW side
void MessageProcessor::ProcessEchoRequestMessage(gtppacket::GTP_VERSION Version, void* Buffer, size_t BufferSize, int fd_echo, struct sockaddr_in& echo)
{
	union gtpPacketHelper *packet = (union gtpPacketHelper *)Buffer;
	size_t PacketSize = 0;
	unsigned char* iedata = NULL;
	size_t ielength = 0;
	int instance = 0;

	if(Version == GTP_VERSION_1)
	{
		//TODO/TBD: presence of private extension IE present in the message
		packet->gtp1s.header.type = GTPV1_ECHO_RSP;

	    unsigned int RecoveryIePosition = std::min(MAX_UDP_PACKET_RCV_SIZE/2,GTPV1_HEADER_MIN + htons(packet->gtp1s.header.length));
	    ((unsigned char*)Buffer)[RecoveryIePosition++] = GTPV1_IE_RECOVERY;
	    ((unsigned char*)Buffer)[RecoveryIePosition] = (unsigned char)RestartCounter::RecoveryCounter;
	    packet->gtp1s.header.length = htons(htons(packet->gtp1s.header.length) + 2);

		PacketSize = BufferSize + 2;
	}
	else if(Version == GTP_VERSION_2)
	{
		packet->gtp2s.header.type = GTPV2_ECHO_RSP;

		//TODO: TS 129 274 chapter 8.5 Recovery IE "In the first release of GTPv2 spec n = 1. That is, the overall length of the IE is 5 octets. In future releases of the spec	additional octets may be specified."
		if(getGtpv2Ie((const unsigned char*)Buffer, BufferSize, GTPV2_IE_RECOVERY, instance, iedata, ielength) && ielength == 1)
		{
			if(LOG_CATEGORY_GTP_C && LOG_CATEGORY_GTP_ECHO)
				INFO(("GTPv2 Received RecoveryCount %u in echo request message\n",*iedata));

			*iedata = (unsigned char)RestartCounter::RecoveryCounter;
			PacketSize = BufferSize;
		}
		else
		{
			ERROR(("GTPv2 failed to find mandatory Recovery IE in echo request message\n"));
			return;
		}
	}

	if(LOG_CATEGORY_GTP_C && LOG_CATEGORY_GTP_ECHO)
	{
		INFO(("Sending GTPv%u Echo Response with RecoveryCount %u to dst %s port %u\n", Version, (unsigned char)RestartCounter::RecoveryCounter, inet_ntoa(echo.sin_addr), htons(echo.sin_port)));
		PRINT_DUMP((Buffer, PacketSize));
	}

	ssize_t sndsize = sendto(fd_echo, Buffer, PacketSize, MSG_NOSIGNAL, (const struct sockaddr*)&echo, sizeof(echo));

	if(sndsize == -1)
		ERROR(("GTPv%u failed to send echo response packet to address %s port %u\n", Version, inet_ntoa(echo.sin_addr), htons(echo.sin_port)));

}

void MessageProcessor::ProcessEchoResponseMessage(gtppacket::GTP_VERSION Version, void* Buffer, size_t BufferSize, struct sockaddr_in& src)
{
	union gtpPacketHelper *packet = (union gtpPacketHelper *)Buffer;
	unsigned char* iedata = NULL;
	size_t ielength = 0;
	int instance = 0;
	unsigned char remote_recovery_counter = 0;

	if(Version == GTP_VERSION_1)
	{
		//TODO/TBD: presence of private extension IE present in the message
	    unsigned int RecoveryIePosition = GTPV1_HEADER_MIN + htons(packet->gtp1s.header.length) - 2;

	    if(RecoveryIePosition+1 < BufferSize && ((unsigned char*)Buffer)[RecoveryIePosition] == GTPV1_IE_RECOVERY)
	    {
	    	remote_recovery_counter = ((unsigned char*)Buffer)[++RecoveryIePosition];
	    }
		else
		{
			if(LOG_CATEGORY_GTP_ECHO)WARN(("GTPv1 failed to find recovery counter value in echo request response message\n"));
			//return;//not mandatory
		}
	}
	else if(Version == GTP_VERSION_2)
	{
		//TODO: TS 129 274 chapter 8.5 Recovery IE "In the first release of GTPv2 spec n = 1. That is, the overall length of the IE is 5 octets. In future releases of the spec	additional octets may be specified."
		if(getGtpv2Ie((const unsigned char*)Buffer, BufferSize, GTPV2_IE_RECOVERY, instance, iedata, ielength) && ielength == 1)
		{
			remote_recovery_counter = *iedata;
		}
		else
		{
			ERROR(("GTPv2 failed to find mandatory Recovery IE in echo request response message\n"));
			return;
		}
	}

	uint32_t SequenceNumber = getGtpHeaderSequenceNumber(Buffer, BufferSize);

	m_ThePgwManagerInterface->UpdatePgwRemoteCounter(Version, SequenceNumber, remote_recovery_counter, src);
}

bool MessageProcessor::ProcessErrorIndicationMessage(uint32_t& Teid, ProxyDirection Direction, const unsigned char* Buffer, size_t BufferSize, int fd, struct sockaddr_in& from)
{
	unsigned char* iedata = NULL;
	size_t ielength = 0;
	int instance = 0;

	uint32_t GsnAddr = 0;
	struct sockaddr_in gsn_addr;

	if(getGtpv1Ie(Buffer, BufferSize, GTPV1_IE_TEI_DI, instance, iedata, ielength) && ielength == 4)
	{
		Teid = ntohl(*(uint32_t*)iedata);
	}
	else
	{
		ERROR(("GTPv1-U failed to find mandatory TEI_DI IE in error indication message\n"));
		return false;
	}

	//ETSI TS 129 281 chapter 7.3.1 "The information element GTP-U Peer Address shall be the destination address (e.g. destination IP address, MBMS
	//Bearer Context) fetched from the original user data message that triggered this procedure. A GTP-U Peer Address can
	//be a GGSN, SGSN, RNC, PGW, SGW or eNodeB address. The TEID and GTP-U peer Address together uniquely
	//identify the related PDP context, RAB or EPS bearer in the receiving node."
	iedata = NULL;
	ielength = 0;
	if(getGtpv1Ie(Buffer, BufferSize, GTPV1_IE_GSN_ADDR, instance, iedata, ielength) && ielength == 4)//IPv4 expected - length 4
	{
		GsnAddr = *(uint32_t*)iedata;
		mempcpy(&gsn_addr.sin_addr,iedata,std::min(ielength,sizeof(gsn_addr.sin_addr)));
	}
	else
	{
		ERROR(("GTPv1-U failed to find mandatory GSN-ADDR IE in error indication message\n"));
		return false;
	}

	uint32_t UnmappedTeid = Teid;
	bool ErrorMarkedOnPdp = m_ThePdpManagerInterface->SetErrorIndicationOnPdp(Teid, Direction, from);
	if(LOG_CATEGORY_GTP_U)INFO(("GTPv1-U Error Indication with TEID:0x%08x GSN-Addr:%08x->%s Mapped TEID:0x%08x. Marked on PDP:%s\n",
			UnmappedTeid, GsnAddr, inet_ntoa(gsn_addr.sin_addr), Teid, ErrorMarkedOnPdp ? "Yes" : "No"));

	return true;//OffendingGsn;
}

bool MessageProcessor::SendEchoRequestMessage(gtppacket::GTP_VERSION Version, uint32_t Sequence, uint8_t RestartCount, int fd, struct sockaddr_in& src, struct sockaddr_in& dst) const
{
	gtpPacketHelper packet;
	size_t PacketSize = 0;

	if(Version == GTP_VERSION_1)
	{
		packet.flags = 0x32;//GTPv1 with sequence
		packet.gtp1l.header.type = GTPV1_ECHO_REQ;
		packet.gtp1l.header.length = ntohs((uint16_t)4);//length of sequence, npdu & next byte
		packet.gtp1l.header.teid = 0;
		packet.gtp1l.header.seq = ntohs((uint16_t)Sequence);
		packet.gtp1l.header.npdu = 0;
		packet.gtp1l.header.next = 0;

		PacketSize = sizeof(struct gtpv1HeaderExtendedVersion);
	}
	else if(Version == GTP_VERSION_2)
	{
		packet.flags = 0x40;//GTPv2 without TEID
		packet.gtp2s.header.type = GTPV2_ECHO_REQ;
		packet.gtp2s.header.length = 0;
		packet.gtp2s.header.setseq(Sequence);

		size_t idx = 0;//should not exceed GTP_MAX_PL size
		packet.gtp2s.payload[idx++] = GTPV2_IE_RECOVERY;
		packet.gtp2s.payload[idx++] = 0;//length MSB
		packet.gtp2s.payload[idx++] = 1;//length LSB; length of the payload after CR-Flag & Instance byte
		packet.gtp2s.payload[idx++] = 0;//CR flag and instance
		packet.gtp2s.payload[idx++] = RestartCount;

		packet.gtp2s.header.length = ntohs((uint16_t)(4+idx));//length of sequence & spare plus the IE CAUSE length
		PacketSize = sizeof(struct gtpv2HeaderShortVersion) + idx;
	}

	std::string SourceStr = FormatStr("%s port %u",inet_ntoa(src.sin_addr), htons(src.sin_port));
	std::string DestinationStr = FormatStr("%s port %u",inet_ntoa(dst.sin_addr), htons(dst.sin_port));


	if(LOG_CATEGORY_GTP_C && LOG_CATEGORY_GTP_ECHO)
	{
		INFO(("Sending GTPv%u Echo Request Message with RecoveryCount %u from src %s to dst %s\n", Version, RestartCount, SourceStr.c_str(), DestinationStr.c_str()));
		PRINT_DUMP((&packet, PacketSize));
		checkGtpPacket(&packet, PacketSize, LOG_LEVEL_INFO && LOG_CATEGORY_GTP_CHECK/*log*/);
	}

#ifndef TESTONLY
	ssize_t sndsize = sendto(fd, &packet, PacketSize, MSG_NOSIGNAL, (const struct sockaddr*)&dst, sizeof(dst));

	if(sndsize == -1)
		ERROR(("GTPv%u failed to send Echo Request Message with RecoveryCount %u from src %s to dst %s\n", Version, RestartCount, SourceStr.c_str(), DestinationStr.c_str()));

	return sndsize != -1;
#else
	return false;
#endif
}

void MessageProcessor::SendDataPlaneErrorIndicationMessage(uint32_t teid, int fd, struct sockaddr_in& dst, struct sockaddr_in& src) const
{
	//ETSI TS 129 281 chapter 7.3.1
	gtpv1PacketLongVersion packet;

	packet.header.flags = 0x32;//GTPv1 with sequence (ETSI TS 129 281 chapter 5.1 "the S flag shall be set to '1'")
	packet.header.type = GTPV1_ERROR;
    packet.header.length = 0;
    packet.header.teid = ntohl(teid);//temp value. The source TEID is set to 0 later
    packet.header.seq = 0;//ETSI TS 129 281 chapter 5.1 "the Sequence Number shall be ignored by the receiver, even though the S flag is set to '1'."
    packet.header.npdu = 0;
    packet.header.next = 0;

    //Mandatory IE TEID-I
    unsigned idx = 0;//should not exceed GTP_MAX_PL size
    packet.payload[idx++] = GTPV1_IE_TEI_DI;
    memcpy(&packet.payload[idx], &packet.header.teid, sizeof(packet.header.teid));
    idx += sizeof(packet.header.teid);
    packet.header.teid = 0;

    //Mandatory GTP-U peer Address - destination address of the received packet
    packet.payload[idx++] = GTPV1_IE_GSN_ADDR;
    packet.payload[idx++] = 0;//IPv4 length MSB
    packet.payload[idx++] = 4;//IPv4 length LSB
    memcpy(&packet.payload[idx], &(src.sin_addr), 4);
    idx += 4;

    packet.header.length = ntohs(4 + idx);//size of sequence, mpdu and next elements (4 bytes) and IEs
	size_t PacketSize = sizeof(struct gtpv1HeaderExtendedVersion) + idx;

	//TS 129 281 chapter 4.4.2.4 Error Indication: The UDP destination port for the Error Indication shall be the user plane UDP port (2152).
	struct sockaddr_in Destination = dst;
	Destination.sin_port = htons(GTPU_DEFAULT_PORT);

	std::string SourceStr = FormatStr("%s",inet_ntoa(src.sin_addr));
	std::string DestinationStr = FormatStr("%s port %u",inet_ntoa(Destination.sin_addr), htons(Destination.sin_port));

	if(LOG_CATEGORY_GTP_U)
	{
		INFO(("Sending GTPv1 Error Indication Message for TEID:0x%08x src %s to dst %s\n", teid, SourceStr.c_str(), DestinationStr.c_str()));
		PRINT_DUMP((&packet, PacketSize));
		checkGtpPacket(&packet, PacketSize, LOG_LEVEL_INFO && LOG_CATEGORY_GTP_CHECK/*log*/);
	}

#ifndef TESTONLY
	ssize_t sndsize = sendto(fd, &packet, PacketSize, MSG_NOSIGNAL, (const struct sockaddr*)&Destination, sizeof(Destination));

	if(sndsize == -1)
		ERROR(("GTPv1 failed to send Error Indication Message for TEID:0x%08x src %s to dst %s\n", teid, SourceStr.c_str(), DestinationStr.c_str()));
#endif
}

bool MessageProcessor::SendErrorResponseMessage(int fd, struct sockaddr_in& dst, gtppacket::GTP_VERSION Version, uint8_t GtpMsgType, uint32_t Sequence, uint8_t CauseValue) const
{
	gtpPacketHelper packet;
	size_t PacketSize = 0;

	uint8_t RspMsgType = getResponseMessageType(Version, GtpMsgType);

	if(RspMsgType == 0)
	{
		ERROR(("GTPv%u SendErrorResponseMessage failed to find response message for message type:%u-%s\n", Version, GtpMsgType,
				Version == GTP_VERSION_2 ? getGtpv2MessageTypeStr(GtpMsgType).c_str() : getGtpv1MessageTypeStr(GtpMsgType).c_str()));
		return false;
	}

	if(Version == GTP_VERSION_1)
	{
		packet.flags = 0x32;//GTPv1 with sequence
		packet.gtp1l.header.type = RspMsgType;
		packet.gtp1l.header.length = 0;
		packet.gtp1l.header.teid = 0;
		packet.gtp1l.header.seq = ntohs((uint16_t)Sequence);
		packet.gtp1l.header.npdu = 0;
		packet.gtp1l.header.next = 0;

		size_t idx = 0;//should not exceed GTP_MAX_PL size
		packet.gtp1l.payload[idx++] = GTPV1_IE_CAUSE;//TV type length 1
		packet.gtp1l.payload[idx++] = CauseValue;

		packet.gtp1l.header.length = ntohs((uint16_t)(4+idx));//length of sequence & spare plus the IE CAUSE length
		PacketSize = sizeof(struct gtpv1HeaderExtendedVersion) + idx;
	}
	else if(Version == GTP_VERSION_2)
	{
		packet.flags = 0x40;//GTPv2 without TEID
		packet.gtp2s.header.type = RspMsgType;
		packet.gtp2s.header.length = 0;
		packet.gtp2s.header.setseq(Sequence);

		size_t idx = 0;//should not exceed GTP_MAX_PL size
		packet.gtp2s.payload[idx++] = GTPV2_IE_CAUSE;
		packet.gtp2s.payload[idx++] = 0;//length MSB
		packet.gtp2s.payload[idx++] = 2;//length LSB; length of the payload after CR-Flag & Instance byte
		packet.gtp2s.payload[idx++] = 0;//CR flag and instance
		packet.gtp2s.payload[idx++] = CauseValue;
		packet.gtp2s.payload[idx++] = 0x01;//Cause source remote; No BCE or PCE flag set
		//TODO include the offending IE if any

		packet.gtp2s.header.length = ntohs((uint16_t)(4+idx));//length of sequence & spare plus the IE CAUSE length
		PacketSize = sizeof(struct gtpv2HeaderShortVersion) + idx;
	}

	std::string DestinationStr = FormatStr("%s port %u",inet_ntoa(dst.sin_addr), htons(dst.sin_port));

	if(LOG_CATEGORY_GTP_C)
	{
		INFO(("Sending GTPv%u Error Response Message to dst %s\n", Version, DestinationStr.c_str()));
		PRINT_DUMP((&packet, PacketSize));
		checkGtpPacket(&packet, PacketSize, LOG_LEVEL_INFO && LOG_CATEGORY_GTP_CHECK/*log*/);
	}

#ifndef TESTONLY
	ssize_t sndsize = sendto(fd, &packet, PacketSize, MSG_NOSIGNAL, (const struct sockaddr*)&dst, sizeof(dst));

	if(sndsize == -1)
		ERROR(("GTPv%u failed to send Error Response Message to dst %s\n", Version, DestinationStr.c_str()));

	return sndsize != -1;
#else
	return false;
#endif

}

bool MessageProcessor::HandleVersion1ControlMessage(PdpRecord &pdpRecord, unsigned char* buffer, ssize_t rcvsize, struct sockaddr_in fromaddr, uint8_t GtpMsgType, ProxyDirection Direction, uint32_t PacketTeid)
{
	bool UpdateRecord = (GtpMsgType == GTPV1_CREATE_PDP_RSP ||
						 GtpMsgType == GTPV1_CREATE_PDP_REQ ||
						 GtpMsgType == GTPV1_UPDATE_PDP_REQ ||
						 GtpMsgType == GTPV1_UPDATE_PDP_RSP);//TODO additional GtpMsgType values to update

	uint8_t ExtractedNSAPI = (uint8_t)-1;
	uint8_t LinkedNSAPI = (uint8_t)-1;

	PdpRecord::DataPlaneTeids* DataPlaneTeids = NULL;

	unsigned char* iedata = NULL;
	size_t ielength = 0;
	if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_NSAPI, 0, iedata, ielength) && ielength == 1)
	{
		ExtractedNSAPI = *(uint8_t*)iedata;
		if(!pdpRecord.HaveNsapiEbi(ExtractedNSAPI))
		{
			if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP Context set new NSAPI:0x%02x\n", ExtractedNSAPI));
			DataPlaneTeids = pdpRecord.GetDataPlaneTeidsForNsapiEbi(ExtractedNSAPI);
			//Initialize the PgwTeidDataPlane with the create sequence number for tracking as the response does not deliver the NSAPI IE
			if(GtpMsgType == GTPV1_CREATE_PDP_REQ)//TBD handling of new NSAPI when this isn't a GTPV1_CREATE_PDP_REQ
				DataPlaneTeids->PgwTeidDataPlane = pdpRecord.PdpCreatePgwSequenceNumber;
		}
		else
		{
			if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context existing NSAPI:0x%02x\n", ExtractedNSAPI));
			DataPlaneTeids = pdpRecord.GetDataPlaneTeidsForNsapiEbi(ExtractedNSAPI);
		}
	}

	if(UpdateRecord)
	{
		//TODO impact of additional GtpMsgType values for UpdateRecord variable
		//TODO handling collision of TEID presented towards the P-GW; TEID shall be unique for a source IP & NSAPI (Could be secondary PDP context)

		if(GtpMsgType == GTPV1_CREATE_PDP_REQ)
		{
			//the NSAPI is mandatory on GTPV1_CREATE_PDP_REQ
			if(ExtractedNSAPI == (uint8_t)-1)
			{
				if(LOG_CATEGORY_GTP_C)ERROR(("GTP-C PDP Context Create Request can't find mandatory NSAPI\n"));
				return false;
			}

			//Set the (default/initial) IP destination of the P-GW for a given IMSI value; Update P-GW addresses record on P-GW response
			//The IMSI is not present on secondary PDP context activation and emergency access; TBD: Use IMEI for emergency access?

			iedata = NULL;
			ielength = 0;
			if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_IMSI, 0, iedata, ielength) && ielength == 8)
				pdpRecord.IMSI = htobe64(*(uint64_t*)iedata);

			iedata = NULL;
			ielength = 0;
			std::string apn;
			if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_APN, 0, iedata, ielength) && ielength != 0)
			{
				ApnToStr(iedata, ielength, apn);
				if(!apn.empty())
					pdpRecord.APN = apn;
			}

			iedata = NULL;
			ielength = 0;
			if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_RAT_TYPE, 0, iedata, ielength) && ielength == 1)
			{
				pdpRecord.RatType = *(uint8_t*)iedata;
			}

			//This is a secondary PDP context related request if a second NSAPI is present
			//The second NSAPI is a linked NSAPI. Linked NSAPI indicates the NSAPI assigned to any one of the already activated PDP contexts for this PDN connection.
			iedata = NULL;
			ielength = 0;
			if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_NSAPI, 1, iedata, ielength) && ielength == 1)
				LinkedNSAPI = *(uint8_t*)iedata;

			if(LinkedNSAPI == (uint8_t)-1)
			{
				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context request with NSAPI:0x%02x TEID:0x%08x\n", ExtractedNSAPI, PacketTeid));
				m_ThePdpManagerInterface->SetPgwAddressIfNeeded(pdpRecord);
			}
			else
			{
				if(!pdpRecord.HaveNsapiEbi(LinkedNSAPI))
				{
					if(LOG_CATEGORY_GTP_C)WARN(("GTP-C PDP Context request with second NSAPI:0x%02x TEID:0x%08x doesn't exist on PDP record\n", LinkedNSAPI, PacketTeid));
				}
				else
				{
					if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context request with linked NSAPI:0x%02x TEID:0x%08x found on PDP context\n", LinkedNSAPI, PacketTeid));
				}
			}
		}

		//TODO:TBD on GTPv2 we rewrite any GTPV2_IE_FTEID regardless of the UpdateRecord boolean/GtpMsgType
		iedata = NULL;
		ielength = 0;
		if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_TEI_C, 0, iedata, ielength) && ielength == 4)
		{
			uint32_t ExtractedCplaneTeid = ntohl(*(uint32_t*)iedata);

			//not present on secondary PDP context
			if(Direction == ProxyDirection::TO_SGW_REMOTE)
			{
				if(pdpRecord.PgwTeidControlPlane != (uint32_t)-1 && pdpRecord.PgwTeidControlPlane != ExtractedCplaneTeid)
					if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP Context PgwTeidControlPlane:0x%08x change to TEID:0x%08x\n", pdpRecord.PgwTeidControlPlane, ExtractedCplaneTeid));

				pdpRecord.PgwTeidControlPlane = ExtractedCplaneTeid;
#ifdef NO_MAPPING
				pdpRecord.PgwTeidControlPlaneMapped = pdpRecord.PgwTeidControlPlane;
#else
				if(GtpMsgType == GTPV1_CREATE_PDP_RSP)//TBD also for update?
					pdpRecord.PgwTeidControlPlaneMapped = m_ThePgwManagerInterface->MakeTeid(pdpRecord.PacketGateway, Direction);
#endif
				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context PgwTeidControlPlane:0x%08x Mapped:0x%08x\n", pdpRecord.PgwTeidControlPlane, pdpRecord.PgwTeidControlPlaneMapped));
				*(uint32_t*)iedata = ntohl(pdpRecord.PgwTeidControlPlaneMapped);
			}
			else if(Direction == ProxyDirection::TO_PGW_REMOTE)
			{
				if(pdpRecord.SgwTeidControlPlane != (uint32_t)-1 && pdpRecord.SgwTeidControlPlane != ExtractedCplaneTeid)
					if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP Context SgwTeidControlPlane:0x%08x change to TEID:0x%08x\n", pdpRecord.SgwTeidControlPlane, ExtractedCplaneTeid));

				pdpRecord.SgwTeidControlPlane = ExtractedCplaneTeid;
#ifdef NO_MAPPING
				pdpRecord.SgwTeidControlPlaneMapped = pdpRecord.SgwTeidControlPlane;
#else
				if(GtpMsgType == GTPV1_CREATE_PDP_REQ)//TBD also for update?
					pdpRecord.SgwTeidControlPlaneMapped = m_ThePgwManagerInterface->MakeTeid(pdpRecord.PacketGateway, Direction);
#endif
				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context SgwTeidControlPlane:0x%08x Mapped:0x%08x\n", pdpRecord.SgwTeidControlPlane, pdpRecord.SgwTeidControlPlaneMapped));
				*(uint32_t*)iedata = ntohl(pdpRecord.SgwTeidControlPlaneMapped);
			}

			if(GtpMsgType == GTPV1_UPDATE_PDP_REQ)
				pdpRecord.ModificationCount++;

		}
		else if(PacketTeid != 0)
		{
			//TBD secondary PDP context - shouldn't change the C-Plane TEID
		}

		//TODO:TBD on GTPv2 we rewrite any GTPV2_IE_FTEID  of a GTPV2_IE_BEARER_CONTEXT regardless of the UpdateRecord boolean/GtpMsgType
		iedata = NULL;
		ielength = 0;
		if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_TEI_DI, 0, iedata, ielength) && ielength == 4)
		{
			uint32_t ExtractedDplaneTeid = ntohl(*(uint32_t*)iedata);

			if(DataPlaneTeids == NULL)
			{
				if(GtpMsgType == GTPV1_CREATE_PDP_RSP && Direction == ProxyDirection::TO_SGW_REMOTE)
				{
					//the ExtractedDplaneTeid of the PGW site is seen for the first time so we locate the DataPlaneTeids with the sequence number
					DataPlaneTeids = pdpRecord.GetDataPlaneTeidsForDplaneTeid(pdpRecord.PdpCreatePgwSequenceNumber,/*PgwOnly*/true);
					if(DataPlaneTeids)
					{
						if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context Create Response located D-TEID set for re-mapping with Sequence Number D-TEID:0x%08x\n", pdpRecord.PdpCreatePgwSequenceNumber));
						DataPlaneTeids->PgwTeidDataPlane = (uint32_t)-1;
						pdpRecord.PdpCreatePgwSequenceNumber = (uint32_t)-1;
					}
					else
					{
						if(LOG_CATEGORY_GTP_C)WARN(("GTP-C PDP Context Create Response can't locate D-TEID set for re-mapping with Sequence Number D-TEID:0x%08x\n", pdpRecord.PdpCreatePgwSequenceNumber));
					}
				}
				else
				{
					DataPlaneTeids = pdpRecord.GetDataPlaneTeidsForDplaneTeid(ExtractedDplaneTeid);
				}

				if(DataPlaneTeids != NULL)
				{
					if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP Context located D-TEID set with D-TEID:0x%08x\n", ExtractedDplaneTeid));
				}
				else
				{
					if(LOG_CATEGORY_GTP_C)WARN(("GTP-C PDP Context can't locate D-TEID set for re-mapping with D-TEID:0x%08x\n",ExtractedDplaneTeid));
				}
			}

			if(DataPlaneTeids != NULL)
			{
				if(Direction == ProxyDirection::TO_SGW_REMOTE)
				{
					if(DataPlaneTeids->PgwTeidDataPlane != (uint32_t)-1 && DataPlaneTeids->PgwTeidDataPlane != ExtractedDplaneTeid)
						if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP Context PgwTeidDataPlane:0x%08x change to TEID:0x%08x\n", DataPlaneTeids->PgwTeidDataPlane, ExtractedDplaneTeid));

					DataPlaneTeids->PgwTeidDataPlane = ExtractedDplaneTeid;
#ifdef NO_MAPPING
					DataPlaneTeids->PgwTeidDataPlaneMapped = DataPlaneTeids->PgwTeidDataPlane;
#else
					if(GtpMsgType == GTPV1_CREATE_PDP_RSP)//TBD also for update? Check whether the GTP-U GSN address has changed
						DataPlaneTeids->PgwTeidDataPlaneMapped = m_ThePgwManagerInterface->MakeTeid(pdpRecord.PacketGateway, Direction);
#endif
					if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context PgwTeidDataPlane:0x%08x Mapped:0x%08x\n", DataPlaneTeids->PgwTeidDataPlane, DataPlaneTeids->PgwTeidDataPlaneMapped));
					*(uint32_t*)iedata = ntohl(DataPlaneTeids->PgwTeidDataPlaneMapped);
				}
				else if(Direction == ProxyDirection::TO_PGW_REMOTE)
				{
					if(DataPlaneTeids->SgwTeidDataPlane != (uint32_t)-1 && DataPlaneTeids->SgwTeidDataPlane != ExtractedDplaneTeid)
						if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP Context SgwTeidDataPlane:0x%08x change to TEID:0x%08x\n", DataPlaneTeids->SgwTeidDataPlane, ExtractedDplaneTeid));

					DataPlaneTeids->SgwTeidDataPlane = ExtractedDplaneTeid;
#ifdef NO_MAPPING
					DataPlaneTeids->SgwTeidDataPlaneMapped = DataPlaneTeids->SgwTeidDataPlane;
#else
					if(GtpMsgType == GTPV1_CREATE_PDP_REQ)//TBD also for update? Check whether the GTP-U GSN address has changed
						DataPlaneTeids->SgwTeidDataPlaneMapped = m_ThePgwManagerInterface->MakeTeid(pdpRecord.PacketGateway, Direction);
#endif
					if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context SgwTeidDataPlane:0x%08x Mapped:0x%08x\n", DataPlaneTeids->SgwTeidDataPlane, DataPlaneTeids->SgwTeidDataPlaneMapped));
					*(uint32_t*)iedata = ntohl(DataPlaneTeids->SgwTeidDataPlaneMapped);
				}
			}
		}

		if(GtpMsgType == GTPV1_CREATE_PDP_RSP)
		{
			iedata = NULL;
			ielength = 0;

			if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_CAUSE, 0, iedata, ielength) && ielength == 1)
			{
				if(*(unsigned char*)iedata != GTPV1_CAUSE_ACC_REQ)
				{
					if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP create request rejected with V1 cause:%u\n", *(unsigned char*)iedata));

					pdpRecord.SetPdpState(PDP_STATE_CREATE_REJECTED, m_ThePdpManagerTimerInterface);
				}
			}

			iedata = NULL;
			ielength = 0;
			if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_EUA, 0, iedata, ielength) && ielength == 6)
			{
				//skip the first two bytes which are type organization and address type
				pdpRecord.EUA = GetIPv4AddressStr(*(uint32_t*)(iedata+2));
			}
		}
	}

	//re-write any GSN address if found in the control message
	for(unsigned int instance=0;instance<2;instance++)
	{
		//TODO additional GtpMsgType values for UpdateRecord variable: what are the GSN instance numbers for those?

		iedata = NULL;
		ielength = 0;
		if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_GSN_ADDR, instance, iedata, ielength))
		{
			std::string Value;
			HexDumpToStr(iedata,(unsigned int)ielength,Value);
			if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context GSN Address Instance %d Val:%s\n", instance, Value.c_str()));

			if(Direction == ProxyDirection::TO_SGW_REMOTE)
			{
				if(instance == 0)
				{
					//Control Plane
					if(UpdateRecord)
					{
						//TBD is a GTP-C address change allowed/possible?
						mempcpy(&pdpRecord.Pgw.c_plane_addr.sin_addr,iedata,std::min(ielength,sizeof(pdpRecord.Pgw.c_plane_addr.sin_addr)));
						if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context setting P-GW control plane addr:%s %u\n", inet_ntoa(pdpRecord.Pgw.c_plane_addr.sin_addr), htons(pdpRecord.Pgw.c_plane_addr.sin_port)));
					}
					mempcpy(iedata,&sgw_c_local_addr.sin_addr,ielength);
				}
				else
				{
					//Data Plane
					if(UpdateRecord)
					{
						//TODO: when there is a change
						//m_ThePdpManagerInterface->RemoveDataPlaneReferences(pdpRecord);
						//else any entry will be removed upon pdpRecord removal

						mempcpy(&pdpRecord.Pgw.u_plane_addr.sin_addr,iedata,std::min(ielength,sizeof(pdpRecord.Pgw.u_plane_addr.sin_addr)));
						if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context setting P-GW data plane addr:%s %u\n", inet_ntoa(pdpRecord.Pgw.u_plane_addr.sin_addr), htons(pdpRecord.Pgw.u_plane_addr.sin_port)));

						m_ThePdpManagerInterface->AddDataPlaneReferencesIfNeeded(pdpRecord, DataPlaneTeids);
					}
					mempcpy(iedata,&sgw_u_local_addr.sin_addr,ielength);
				}
			}
			else if(Direction == ProxyDirection::TO_PGW_REMOTE)
			{
				if(instance == 0)
				{
					//Control Plane
					if(UpdateRecord)
					{
						//TBD is a GTP-C address change allowed/possible?
						mempcpy(&pdpRecord.Sgw.c_plane_addr.sin_addr,iedata,std::min(ielength,sizeof(pdpRecord.Sgw.c_plane_addr.sin_addr)));
						if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context setting S-GW control plane addr:%s %u\n", inet_ntoa(pdpRecord.Sgw.c_plane_addr.sin_addr), htons(pdpRecord.Sgw.c_plane_addr.sin_port)));
					}
					mempcpy(iedata,&pgw_c_local_addr.sin_addr,ielength);
				}
				else
				{
					//Data Plane
					if(UpdateRecord)
					{
						//TODO: when there is a change
						//m_ThePdpManagerInterface->RemoveDataPlaneReferences(pdpRecord);
						//else any entry will be removed upon pdpRecord removal

						mempcpy(&pdpRecord.Sgw.u_plane_addr.sin_addr,iedata,std::min(ielength,sizeof(pdpRecord.Sgw.u_plane_addr.sin_addr)));
						if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context setting S-GW data plane addr:%s %u\n", inet_ntoa(pdpRecord.Sgw.u_plane_addr.sin_addr), htons(pdpRecord.Sgw.u_plane_addr.sin_port)));

						m_ThePdpManagerInterface->AddDataPlaneReferencesIfNeeded(pdpRecord, DataPlaneTeids);
					}
					mempcpy(iedata,&pgw_u_local_addr.sin_addr,ielength);
				}
			}
		}
		else
		{
			if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context Could not get GSN Address Instance %d\n", instance));
		}
	}

	if(Direction == ProxyDirection::TO_SGW_REMOTE)
	{
		//re-write GTPV1_IE_RECOVERY
		iedata = NULL;
		ielength = 0;
		if(getGtpv1Ie(buffer, rcvsize, GTPV1_IE_RECOVERY, 0, iedata, ielength))
		{
			*iedata = (unsigned char)RestartCounter::RecoveryCounter;
		}
	}

	return true;
}

bool MessageProcessor::HandleVersion2ControlMessage(PdpRecord &pdpRecord, unsigned char* buffer, ssize_t rcvsize, struct sockaddr_in fromaddr, uint8_t GtpMsgType, ProxyDirection Direction, uint32_t PacketTeid)
{
	unsigned char* iedata = NULL;
	size_t ielength = 0;
	int instance = 0;

	PdpRecord::DataPlaneTeids* DataPlaneTeids = NULL;

	bool UpdateRecord = (GtpMsgType == GTPV2_CREATE_SESSION_RESPONSE || GtpMsgType == GTPV2_CREATE_SESSION_REQUEST ||
						 GtpMsgType == GTPV2_MODIFY_BEARER_RESPONSE || GtpMsgType == GTPV2_MODIFY_BEARER_REQUEST );
	//TODO/TBD additional GtpMsgType values to update
	//Candidates:
	//GtpMsgType == GTPV2_CREATE_BEARER_REQUEST ||
	//GtpMsgType == GTPV2_CREATE_BEARER_RESPONSE ||
	//GtpMsgType == GTPV2_UPDATE_BEARER_REQUEST ||
	//GtpMsgType == GTPV2_UPDATE_BEARER_RESPONSE ||
	//GtpMsgType == GTPV2_DELETE_BEARER_REQUEST ||
	//GtpMsgType == GTPV2_DELETE_BEARER_RESPONSE

	//TBD COMMAND & INDICATION types

	if(UpdateRecord)
	{
		//TODO impact of additional GtpMsgType values for UpdateRecord variable
		//TODO handling collision of TEID presented towards the P-GW; TEID shall be unique for a source IP

		//TODO check IMSI IE V2 byte array vs V1 64-bit
		//TODO check whether IMSI is mandatory on GTPV2_CREATE_SESSION_REQUEST (No SIM emergency access et al); TBD use IMEI on no SIM emergency access?
		if(GtpMsgType == GTPV2_CREATE_SESSION_REQUEST)
		{
			iedata = NULL;
			ielength = 0;
			instance = 0;
			if(getGtpv2Ie(buffer, rcvsize, GTPV2_IE_IMSI, instance, iedata, ielength) && ielength == 8)
				pdpRecord.IMSI = htobe64(*(uint64_t*)iedata);

			iedata = NULL;
			ielength = 0;
			instance = 0;
			std::string apn;
			if(getGtpv2Ie(buffer, rcvsize, GTPV2_IE_APN, instance, iedata, ielength) && ielength != 0)
			{
				ApnToStr(iedata, ielength, apn);
				if(!apn.empty())
					pdpRecord.APN = apn;
			}

			iedata = NULL;
			ielength = 0;
			instance = 0;
			if(getGtpv2Ie(buffer, rcvsize, GTPV2_IE_RAT_TYPE, instance, iedata, ielength) && ielength != 0)
			{
				pdpRecord.RatType = *(uint8_t*)iedata;
			}

			m_ThePdpManagerInterface->SetPgwAddressIfNeeded(pdpRecord);
		}

		iedata = NULL;
		ielength = 0;
		instance = 0;
		if(getGtpv2Ie(buffer, rcvsize, GTPV2_IE_CAUSE, instance, iedata, ielength) && (ielength == 2 || ielength == 6))
		{
			if(GtpMsgType == GTPV2_CREATE_SESSION_RESPONSE || GtpMsgType == GTPV2_MODIFY_BEARER_RESPONSE)
			{
				if(!(*(unsigned char*)iedata == GTPV2_CAUSE_REQUEST_ACCEPTED || *(unsigned char*)iedata == GTPV2_CAUSE_REQUEST_PARTIALLY_ACCEPTED))
				{
					if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP %s request rejected with V2 cause:%u source:%u\n",
							GtpMsgType == GTPV2_CREATE_SESSION_RESPONSE ? "create session" : "modify bearer",
							*(unsigned char*)iedata, *((unsigned char*)iedata+1)));

					if(GtpMsgType == GTPV2_CREATE_SESSION_RESPONSE)
					{
						pdpRecord.SetPdpState(PDP_STATE_CREATE_REJECTED, m_ThePdpManagerTimerInterface);
					}
				}
			}
		}

		iedata = NULL;
		ielength = 0;
		instance = 0;
		if(GtpMsgType == GTPV2_CREATE_SESSION_RESPONSE && getGtpv2Ie(buffer, rcvsize, GTPV2_IE_PDN_ADDRESS_ALLOCATION, instance, iedata, ielength) && (ielength == 5))
		{
			//skip over the address type. TODO IPv6
			pdpRecord.EUA = GetIPv4AddressStr(*(uint32_t*)(iedata+1));
		}

		if(GtpMsgType == GTPV2_MODIFY_BEARER_REQUEST)
			pdpRecord.ModificationCount++;
	}


	//re-write any GSN address if found in the control message
	iedata = NULL;
	ielength = 0;
	instance = -1; 	//Get the first element found.
					//S5/S8 interface has the Sender S-GW F-TEID as instance 0 on Modify Bearer and Session Create Request
					//On Session Create Response we need to handle the P-GW F-TEID with instance 1
	//Control Plane
	if(getGtpv2Ie(buffer, rcvsize, GTPV2_IE_FTEID, instance, iedata, ielength))
	{
		//TODO check for Ipv6 value in FteidType
		//signed char FteidIpVersionAndType = *iedata;
		iedata++;
		ielength--;

		std::string Value;
		unsigned int offset = 4;//Jump over 32-bit TEID
		long unsigned int addresslen = ielength > offset ? ielength-offset : ielength;
		HexDumpToStr(iedata+offset,addresslen,Value);
		if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP GSN Address of FTEID Val:%s\n", Value.c_str()));

		uint32_t ExtractedCplaneTeid = ntohl(*(uint32_t*)iedata);

		if(Direction == ProxyDirection::TO_SGW_REMOTE)
		{
			if(UpdateRecord)
			{
				mempcpy(&pdpRecord.Pgw.c_plane_addr.sin_addr,iedata+offset,std::min(addresslen,sizeof(pdpRecord.Pgw.c_plane_addr.sin_addr)));
				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP setting P-GW control plane addr:%s %u\n", inet_ntoa(pdpRecord.Pgw.c_plane_addr.sin_addr), htons(pdpRecord.Pgw.c_plane_addr.sin_port)));

				if(pdpRecord.PgwTeidControlPlane != (uint32_t)-1 && pdpRecord.PgwTeidControlPlane != ExtractedCplaneTeid)
					if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP PgwTeidControlPlane:0x%08x change to TEID:0x%08x\n", pdpRecord.PgwTeidControlPlane, ExtractedCplaneTeid));

				pdpRecord.PgwTeidControlPlane = ExtractedCplaneTeid;
#ifdef NO_MAPPING
				pdpRecord.PgwTeidControlPlaneMapped = pdpRecord.PgwTeidControlPlane;
#else
				if(GtpMsgType == GTPV2_CREATE_SESSION_RESPONSE)//TBD also for modify?
					pdpRecord.PgwTeidControlPlaneMapped = m_ThePgwManagerInterface->MakeTeid(pdpRecord.PacketGateway, Direction);
#endif
				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP Context PgwTeidControlPlane:0x%08x Mapped:0x%08x\n", pdpRecord.PgwTeidControlPlane, pdpRecord.PgwTeidControlPlaneMapped));
			}
			*(uint32_t*)iedata = ntohl(pdpRecord.PgwTeidControlPlaneMapped);
			mempcpy(iedata+offset,&sgw_c_local_addr.sin_addr,addresslen);
		}
		else if(Direction == ProxyDirection::TO_PGW_REMOTE)
		{
			if(UpdateRecord)
			{
				mempcpy(&pdpRecord.Sgw.c_plane_addr.sin_addr,iedata+offset,std::min(addresslen,sizeof(pdpRecord.Sgw.c_plane_addr.sin_addr)));
				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP setting S-GW control plane addr:%s %u\n", inet_ntoa(pdpRecord.Sgw.c_plane_addr.sin_addr), htons(pdpRecord.Sgw.c_plane_addr.sin_port)));

				if(pdpRecord.SgwTeidControlPlane != (uint32_t)-1 && pdpRecord.SgwTeidControlPlane != ExtractedCplaneTeid)
					if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP SgwTeidControlPlane:0x%08x change to TEID:0x%08x\n", pdpRecord.SgwTeidControlPlane, ExtractedCplaneTeid));

				pdpRecord.SgwTeidControlPlane = ExtractedCplaneTeid;
#ifdef NO_MAPPING
				pdpRecord.SgwTeidControlPlaneMapped = pdpRecord.SgwTeidControlPlane;
#else
				if(GtpMsgType == GTPV2_CREATE_SESSION_REQUEST)//TBD also for modify?
					pdpRecord.SgwTeidControlPlaneMapped = m_ThePgwManagerInterface->MakeTeid(pdpRecord.PacketGateway, Direction);
#endif
				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP SgwTeidControlPlane:0x%08x Mapped:0x%08x\n", pdpRecord.SgwTeidControlPlane, pdpRecord.SgwTeidControlPlaneMapped));
			}
			*(uint32_t*)iedata = ntohl(pdpRecord.SgwTeidControlPlaneMapped);
			mempcpy(iedata+offset,&pgw_c_local_addr.sin_addr,addresslen);
		}
	}
	else
	{
		//Normal on Modify Bearer Response
		if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP could not get FQDN Address from FTEID IE\n"));
	}

	//TODO: handle GTPV2_DELETE_BEARER_REQUEST
	//NOTE: GTPv2 GTPV2_DELETE_BEARER_REQUEST provides the GTPV2_IE_EPS_BEARER_ID IE outside the GTPV2_IE_BEARER_CONTEXT

	//TODO: handle GTPV2_MODIFY_BEARER_RESPONSE with GTPV2_IE_CAUSE != GTPV2_CAUSE_REQUEST_ACCEPTED
	//NOTE: GTPv2 GTPV2_MODIFY_BEARER_RESPONSE provides the GTPV2_IE_CAUSE outside as well as inside the GTPV2_IE_BEARER_CONTEXT

	iedata = NULL;
	ielength = 0;
	instance = 0; //S5/S8 interface - Bearer Contexts to be created -> Instance 0
	//Data Plane
	if(getGtpv2Ie(buffer, rcvsize, GTPV2_IE_BEARER_CONTEXT, instance, iedata, ielength))
	{
		unsigned char* iedataNested = NULL;
		size_t ielengthNested = 0;
		int instanceNested = 0;

		if(getGtpv2NestedIe(iedata, ielength, GTPV2_IE_EPS_BEARER_ID, instanceNested, iedataNested, ielengthNested))
		{
			uint8_t ExtractedEBI = *(unsigned char*)iedataNested;
			if(!pdpRecord.HaveNsapiEbi(ExtractedEBI))
			{
				if(LOG_CATEGORY_GTP_C)INFO(("GTP-C BEARER CONTEXT set new EBI:0x%02x\n", ExtractedEBI));
				DataPlaneTeids = pdpRecord.GetDataPlaneTeidsForNsapiEbi(ExtractedEBI);
				//TODO/TBD: whether to initialize the PgwTeidDataPlane with the create sequence number for tracking once the response does not deliver the EBI IE
				if(GtpMsgType == GTPV2_CREATE_SESSION_REQUEST)//TBD handling of new EBI when this isn't a GTPV2_CREATE_SESSION_REQUEST
					DataPlaneTeids->PgwTeidDataPlane = pdpRecord.PdpCreatePgwSequenceNumber;
			}
			else
			{
				if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C BEARER CONTEXT existing EBI:0x%02x\n",ExtractedEBI));
				DataPlaneTeids = pdpRecord.GetDataPlaneTeidsForNsapiEbi(ExtractedEBI);
			}
		}
		else
		{
			//Missing mandatory element of GTPV2_IE_BEARER_CONTEXT
			if(LOG_CATEGORY_GTP_C)ERROR(("GTP-C BEARER CONTEXT could not get EBI\n"));
		}

		iedataNested = NULL;
		ielengthNested = 0;
		instanceNested = -1;//Get the first element found. TBD: check FteidIpVersionAndType to make sure it's a SGW/PGW type
								//S5/S8 SGW/PGW F-TEID as instance 2 on Session Create Request & Response
								//S5/S8 SGW F-TEID as instance 1 on 'Modify Bearer Request for Handover' or a TAU/RAU with a SGW change
		if(getGtpv2NestedIe(iedata, ielength, GTPV2_IE_FTEID, instanceNested, iedataNested, ielengthNested))
		{
			//check GTPV2_IE_EPS_BEARER_ID
			//TODO check for Ipv6 value in FteidType
			//unsigned char FteidIpVersionAndType = *iedataNested;
			iedataNested++;
			ielengthNested--;

			std::string Value;
			unsigned int offset = 4;//Jump over 32-bit TEID
			long unsigned int addresslen = ielengthNested > offset ? ielengthNested-offset : ielengthNested;

			HexDumpToStr(iedataNested+offset,addresslen,Value);
			if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C BEARER CONTEXT GSN Address of FTEID Val:%s\n", Value.c_str()));

			uint32_t ExtractedDplaneTeid = ntohl(*(uint32_t*)iedataNested);

			if(DataPlaneTeids == NULL)
			{
				if(GtpMsgType == GTPV2_CREATE_SESSION_RESPONSE && Direction == ProxyDirection::TO_SGW_REMOTE)
				{
					//the ExtractedDplaneTeid of the PGW site is seen for the first time so we locate the DataPlaneTeids with the sequence number
					DataPlaneTeids = pdpRecord.GetDataPlaneTeidsForDplaneTeid(pdpRecord.PdpCreatePgwSequenceNumber,/*PgwOnly*/true);
					if(DataPlaneTeids)
					{
						if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C BEARER CONTEXT located D-TEID set for re-mapping with Sequence Number D-TEID:0x%08x\n", pdpRecord.PdpCreatePgwSequenceNumber));
						DataPlaneTeids->PgwTeidDataPlane = (uint32_t)-1;
						pdpRecord.PdpCreatePgwSequenceNumber = (uint32_t)-1;
					}
					else
					{
						if(LOG_CATEGORY_GTP_C)WARN(("GTP-C BEARER CONTEXT can't locate D-TEID set for re-mapping with Sequence Number D-TEID:0x%08x\n", pdpRecord.PdpCreatePgwSequenceNumber));
					}
				}
				else
				{
					DataPlaneTeids = pdpRecord.GetDataPlaneTeidsForDplaneTeid(ExtractedDplaneTeid);
				}

				if(DataPlaneTeids != NULL)
				{
					if(LOG_CATEGORY_GTP_C)INFO(("GTP-C BEARER CONTEXT located D-TEID set with D-TEID:0x%08x\n", ExtractedDplaneTeid));
				}
				else
				{
					if(LOG_CATEGORY_GTP_C)WARN(("GTP-C BEARER CONTEXT can't locate D-TEID set for re-mapping with D-TEID:0x%08x\n",ExtractedDplaneTeid));
				}
			}

			if(DataPlaneTeids != NULL)
			{
				if(Direction == ProxyDirection::TO_SGW_REMOTE)
				{
					if(UpdateRecord)
					{
						//TODO: when there is a change
						//m_ThePdpManagerInterface->RemoveDataPlaneReferences(pdpRecord);
						//else any entry will be removed upon pdpRecord removal

						mempcpy(&pdpRecord.Pgw.u_plane_addr.sin_addr,iedataNested+offset,std::min(addresslen,sizeof(pdpRecord.Pgw.u_plane_addr.sin_addr)));
						if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP BEARER CONTEXT setting P-GW data plane addr:%s %u\n", inet_ntoa(pdpRecord.Pgw.u_plane_addr.sin_addr), htons(pdpRecord.Pgw.u_plane_addr.sin_port)));

						if(DataPlaneTeids->PgwTeidDataPlane != (uint32_t)-1 && DataPlaneTeids->PgwTeidDataPlane != ExtractedDplaneTeid)
							if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP PgwTeidDataPlane:0x%08x change to TEID:0x%08x\n", DataPlaneTeids->PgwTeidDataPlane, ExtractedDplaneTeid));

						DataPlaneTeids->PgwTeidDataPlane = ExtractedDplaneTeid;
#ifdef NO_MAPPING
						DataPlaneTeids->PgwTeidDataPlaneMapped = DataPlaneTeids->PgwTeidDataPlane;
#else
						if(GtpMsgType == GTPV2_CREATE_SESSION_RESPONSE)//TBD also for modify? Check whether the GTP-U GSN address has changed
							DataPlaneTeids->PgwTeidDataPlaneMapped = m_ThePgwManagerInterface->MakeTeid(pdpRecord.PacketGateway, Direction);
#endif
						if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP BEARER CONTEXT PgwTeidDataPlane:0x%08x Mapped:0x%08x\n", DataPlaneTeids->PgwTeidDataPlane, DataPlaneTeids->PgwTeidDataPlaneMapped));

						m_ThePdpManagerInterface->AddDataPlaneReferencesIfNeeded(pdpRecord, DataPlaneTeids);
					}
					*(uint32_t*)iedataNested = ntohl(DataPlaneTeids->PgwTeidDataPlaneMapped);
					mempcpy(iedataNested+offset,&sgw_u_local_addr.sin_addr,addresslen);
				}
				else if(Direction == ProxyDirection::TO_PGW_REMOTE)
				{
					if(UpdateRecord)
					{
						//TODO: when there is a change
						//m_ThePdpManagerInterface->RemoveDataPlaneReferences(pdpRecord);
						//else any entry will be removed upon pdpRecord removal

						mempcpy(&pdpRecord.Sgw.u_plane_addr.sin_addr,iedataNested+offset,std::min(addresslen,sizeof(pdpRecord.Sgw.u_plane_addr.sin_addr)));
						if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP BEARER CONTEXT setting S-GW data plane addr:%s %u\n", inet_ntoa(pdpRecord.Sgw.u_plane_addr.sin_addr), htons(pdpRecord.Sgw.u_plane_addr.sin_port)));

						if(DataPlaneTeids->SgwTeidDataPlane != (uint32_t)-1 && DataPlaneTeids->SgwTeidDataPlane != ExtractedDplaneTeid)
							if(LOG_CATEGORY_GTP_C)INFO(("GTP-C PDP SgwTeidDataPlane:0x%08x change to TEID:0x%08x\n", DataPlaneTeids->SgwTeidDataPlane, ExtractedDplaneTeid));

						DataPlaneTeids->SgwTeidDataPlane = ExtractedDplaneTeid;
#ifdef NO_MAPPING
						DataPlaneTeids->SgwTeidDataPlaneMapped = pdpRecord.SgwTeidDataPlane;
#else
						if(GtpMsgType == GTPV2_CREATE_SESSION_REQUEST)//TBD also for modify? Check whether the GTP-U GSN address has changed
							DataPlaneTeids->SgwTeidDataPlaneMapped = m_ThePgwManagerInterface->MakeTeid(pdpRecord.PacketGateway, Direction);
#endif
						if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C PDP BEARER CONTEXT SgwTeidDataPlane:0x%08x Mapped:0x%08x\n", DataPlaneTeids->SgwTeidDataPlane, DataPlaneTeids->SgwTeidDataPlaneMapped));

						m_ThePdpManagerInterface->AddDataPlaneReferencesIfNeeded(pdpRecord, DataPlaneTeids);
					}
					*(uint32_t*)iedataNested = ntohl(DataPlaneTeids->SgwTeidDataPlaneMapped);
					mempcpy(iedataNested+offset,&pgw_u_local_addr.sin_addr,addresslen);
				}
			}
		}
		else
		{
			if(LOG_CATEGORY_GTP_C)DEBUG(("GTP-C BEARER CONTEXT could not get FQDN Address\n"));
		}
	}

	if(Direction == ProxyDirection::TO_SGW_REMOTE)
	{
		//re-write GTPV2_IE_RECOVERY
		iedata = NULL;
		ielength = 0;
		instance = -1; 	//Get the first element found.
		if(getGtpv2Ie(buffer, rcvsize, GTPV2_IE_RECOVERY, instance, iedata, ielength) && ielength == 1)
		{
			*iedata = (unsigned char)RestartCounter::RecoveryCounter;
		}
	}

	return true;
}

void MessageProcessor::HandleControlMessage(int fd_from, struct sockaddr_in bindaddr, int fd_src, struct sockaddr_in src, ProxyDirection Direction)
{
	unsigned char buffer[MAX_UDP_PACKET_RCV_SIZE];
    struct sockaddr_in fromaddr;
    int addrlen = sizeof(struct sockaddr_in);

    struct sockaddr_in dstaddr;
    memset(&dstaddr,0,sizeof(struct sockaddr_in));

    ssize_t rcvsize = recvfrom(fd_from, buffer, sizeof(buffer), 0, (struct sockaddr*)&fromaddr, (socklen_t*)&addrlen);

	if(rcvsize != -1)
	{
	    if(Direction == ProxyDirection::TO_SGW_REMOTE)
	    {
	    	pgw_2_sgw_c_data_count += rcvsize;
	    }
	    else
	    {
	    	sgw_2_pgw_c_data_count += rcvsize;
	    }

		if(LOG_CATEGORY_GTP_C)
		{
			PRINT(("GTP-C RX size %u from %s %s port %u:\n",
					rcvsize, Direction == ProxyDirection::TO_SGW_REMOTE ? "P-GW" : "S-GW", inet_ntoa(fromaddr.sin_addr), htons(fromaddr.sin_port)));
			PRINT_DUMP((buffer,rcvsize));
		}

		if(checkGtpPacket(buffer, rcvsize, LOG_LEVEL_INFO && LOG_CATEGORY_GTP_C && LOG_CATEGORY_GTP_CHECK/*log*/) == false)
		{
			ERROR(("GTP-C packet validation failed for packet from %s %s port %u. Ignoring packet\n", Direction == ProxyDirection::TO_SGW_REMOTE ? "P-GW" : "S-GW", inet_ntoa(fromaddr.sin_addr), htons(fromaddr.sin_port)));
			ERROR_DUMP((buffer,rcvsize));

			/*
			V1:
			If a GTP entity receives a Request message within an IP/UDP packet of a length that is inconsistent with the value
			specified in the Length field of the GTP header, then the receiving GTP entity should log the error and shall send the
			Response message with Cause IE value set to "Invalid message format".
			V2:
			For invalid length IEs include the offending IE.
			*/
			uint8_t GtpMsgType = getGtpHeaderMessageType(buffer, rcvsize);
			GTP_VERSION Version = getGtpHeaderVersion(buffer, rcvsize);

			if(Version != GTP_VERSION_UNKNOWN && GtpMsgType != 0)
			{
				uint8_t CauseValue = Version == GTP_VERSION_1 ? GTPV1_CAUSE_INVALID_MESSAGE : GTPV2_CAUSE_INVALID_LENGTH;
				uint32_t SequenceNumber = getGtpHeaderSequenceNumber(buffer, rcvsize);

				SendErrorResponseMessage(fd_from, fromaddr, Version, GtpMsgType, SequenceNumber, CauseValue);
			}
		}
		else
		{
			uint8_t GtpMsgType = getGtpHeaderMessageType(buffer, rcvsize);
			GTP_VERSION Version = getGtpHeaderVersion(buffer, rcvsize);
			uint32_t SequenceNumber = getGtpHeaderSequenceNumber(buffer, rcvsize);

		    if((Version == GTP_VERSION_1 && GtpMsgType == GTPV1_ECHO_REQ) ||
		       (Version == GTP_VERSION_2 && GtpMsgType == GTPV2_ECHO_REQ))
		    {
		    	if(Direction == ProxyDirection::TO_PGW_REMOTE)
		    		ProcessEchoRequestMessage(Version, buffer, rcvsize, fd_from, fromaddr);
		    	return;
		    }

		    if((Version == GTP_VERSION_1 && GtpMsgType == GTPV1_ECHO_RSP) ||
		       (Version == GTP_VERSION_2 && GtpMsgType == GTPV2_ECHO_RSP))
		    {
		    	if(Direction == ProxyDirection::TO_SGW_REMOTE)
		    		ProcessEchoResponseMessage(Version, buffer, rcvsize, fromaddr);
		    	return;
		    }

			uint32_t PacketTeid = getGtpHeaderTeid(buffer, rcvsize);

			PdpRecord pdpRecord;
			uint32_t PdpRecordKey = (uint32_t)-1;

			if(m_ThePdpManagerInterface->GetOrInitPdpRecord(pdpRecord, PdpRecordKey, buffer, rcvsize, fromaddr, GtpMsgType, Direction, PacketTeid))
			{
				if(!pdpRecord.SetVersion(Version))
				{
					if(LOG_CATEGORY_GTP_C)INFO(("GTP-C detected matching PDP context for message with different GTP version from %s MessageType:%u TEID:0x%08x RecordKey:%u\n",
							Direction == ProxyDirection::TO_SGW_REMOTE ? "P-GW" : "S-GW", GtpMsgType, PacketTeid, PdpRecordKey));
				}

				if(Version == GTP_VERSION_1)
				{
					HandleVersion1ControlMessage(pdpRecord, buffer, rcvsize, fromaddr, GtpMsgType, Direction, PacketTeid);
				}
				else if(Version == GTP_VERSION_2)
				{
					HandleVersion2ControlMessage(pdpRecord, buffer, rcvsize, fromaddr, GtpMsgType, Direction, PacketTeid);
				}

				if(GtpMsgType == GTPV1_DELETE_PDP_REQ || GtpMsgType == GTPV2_DELETE_SESSION_REQUEST)
				{
					//TODO/TBD: secondary PDP context / NSAPI_EBI tracking -> GTPV1_IE_TEARDOWN
					pdpRecord.SetPdpState(PDP_STATE_DELETE_REQUESTED, m_ThePdpManagerTimerInterface);
				}
				if(GtpMsgType == GTPV1_DELETE_PDP_RSP || GtpMsgType == GTPV2_DELETE_SESSION_RESPONSE)
				{
					//TODO/TBD: secondary PDP context / NSAPI_EBI tracking -> GTPV1_IE_TEARDOWN
					pdpRecord.SetPdpState(PDP_STATE_DELETED, m_ThePdpManagerTimerInterface);
				}

				if(PdpRecordKey != (uint32_t)-1)
					m_ThePdpManagerInterface->AddOrUpdatePdpRecord(PdpRecordKey, pdpRecord);

				uint32_t teid = PacketTeid;

				if(Direction == ProxyDirection::TO_PGW_REMOTE)
				{
					//TODO: TBD shall we use the default P-GW address when there was no pdpRecord on record?
					//TODO: TBD any requests that originate at the P-GW to be supported?
					dstaddr = pdpRecord.Pgw.c_plane_addr;

					if(PacketTeid != 0)
					{
						if(pdpRecord.PgwTeidControlPlane != (uint32_t)-1)
							teid = pdpRecord.PgwTeidControlPlane;
						else
							ERROR(("GTP-C failed to assign mapped TEID towards P-GW\n"));
					}

					if(GtpMsgType == GTPV2_CREATE_SESSION_REQUEST || GtpMsgType == GTPV1_CREATE_PDP_REQ)
						//TBD PacketTeid should be 0; check just on that variable?
						m_ThePdpManagerInterface->AddPgwCreateSessionRequestReference(pdpRecord,dstaddr);
				}
				else if(Direction == ProxyDirection::TO_SGW_REMOTE)
				{
					if(isResponseGtpMsgType(GtpMsgType) && pdpRecord.SgwAddrPdpResponseAddress.sin_addr.s_addr != 0)
						dstaddr = pdpRecord.SgwAddrPdpResponseAddress;
					else
						//TODO: is there such thing as a 'default' S-GW address when there was no pdpRecord on record?
						dstaddr = pdpRecord.Sgw.c_plane_addr;

					if(PacketTeid != 0)
					{
						if(pdpRecord.SgwTeidControlPlane != (uint32_t)-1)
							teid = pdpRecord.SgwTeidControlPlane;
						else
							ERROR(("GTP-C failed to assign mapped TEID towards S-GW\n"));
					}
				}

				if(dstaddr.sin_addr.s_addr != 0)
				{
					setGtpHeaderTeid(buffer, rcvsize, teid);

					if(LOG_CATEGORY_GTP_C)
					{
						//DEBUG: log the outgoing packet
						checkGtpPacket(buffer, rcvsize, LOG_LEVEL_DEBUG && LOG_CATEGORY_GTP_CHECK/*log*/);
						uint32_t OoutboundSequenceNumber = getGtpHeaderSequenceNumber(buffer, rcvsize);

						//TODO test on loglevel before src & dst conversion to string
						std::string From = FormatStr("%s port %u",inet_ntoa(fromaddr.sin_addr), htons(fromaddr.sin_port));
						std::string Bind = FormatStr("%s port %u",inet_ntoa(bindaddr.sin_addr), htons(bindaddr.sin_port));
						std::string Source = FormatStr("%s port %u",inet_ntoa(src.sin_addr), htons(src.sin_port));
						std::string Destination = FormatStr("%s port %u",inet_ntoa(dstaddr.sin_addr), htons(dstaddr.sin_port));

						INFO(("GTP-C %s %s TEID:0x%08x Seq:%u -> on %s -> fwd via %s -> %s %s TEID:0x%08x Seq:%u\n",
								Direction == ProxyDirection::TO_PGW_REMOTE ? "S-GW" : "P-GW",
								From.c_str(),
								PacketTeid,
								SequenceNumber,
								Bind.c_str(),
								Source.c_str(),
								Direction == ProxyDirection::TO_PGW_REMOTE ? "P-GW" : "S-GW",
								Destination.c_str(),
								teid,
								OoutboundSequenceNumber));
					}

					ssize_t sndsize = sendto(fd_src, buffer, rcvsize, MSG_NOSIGNAL, (const struct sockaddr*)&dstaddr, sizeof(dstaddr));

					if(sndsize == -1)
					{
						ERROR(("GTP-C failed to send packet to %s address %s port %u\n", Direction == ProxyDirection::TO_PGW_REMOTE ? "P-GW" : "S-GW", inet_ntoa(dstaddr.sin_addr), htons(dstaddr.sin_port)));
					}
					else if(LOG_CATEGORY_GTP_C)
					{
						PRINT(("GTP-C TX size %u to %s %s port %u:\n",
								rcvsize, Direction == ProxyDirection::TO_PGW_REMOTE ? "P-GW" : "S-GW", inet_ntoa(dstaddr.sin_addr), htons(dstaddr.sin_port)));
						PRINT_DUMP((buffer,rcvsize));
					}
				}
				else
				{
					ERROR(("GTP-C no valid destination address found for packet to %s\n", Direction == ProxyDirection::TO_PGW_REMOTE ? "P-GW" : "S-GW"));
				}
			}
			else
			{
				uint8_t CauseValue = Version == GTP_VERSION_1 ? GTPV1_CAUSE_NON_EXIST : GTPV2_CAUSE_CONTEXT_NOT_FOUND;
				uint32_t SequenceNumber = getGtpHeaderSequenceNumber(buffer, rcvsize);

				SendErrorResponseMessage(fd_from, fromaddr, Version, GtpMsgType, SequenceNumber, CauseValue);
			}
		}
	}
}

void MessageProcessor::HandleUserDataMessage(int fd_from, struct sockaddr_in bindaddr, int fd_src, struct sockaddr_in src, ProxyDirection Direction)
{
	unsigned char buffer[MAX_UDP_PACKET_RCV_SIZE];
    struct sockaddr_in fromaddr;
    int addrlen = sizeof(struct sockaddr_in);

    struct sockaddr_in dst;
    memset(&dst,0,sizeof(struct sockaddr_in));

    ssize_t rcvsize = recvfrom(fd_from, buffer, sizeof(buffer), 0, (struct sockaddr*)&fromaddr, (socklen_t*)&addrlen);

	if(rcvsize != -1)
	{
		if(LOG_CATEGORY_GTP_U)
		{
			PRINT(("GTP-U RX size %u from %s %s port %u:\n",
					rcvsize, Direction == ProxyDirection::TO_SGW_REMOTE ? "P-GW" : "S-GW", inet_ntoa(fromaddr.sin_addr), htons(fromaddr.sin_port)));
			PRINT_DUMP((buffer,rcvsize));
		}

		if(checkGtpPacket(buffer, rcvsize, LOG_LEVEL_INFO && LOG_CATEGORY_GTP_U && LOG_CATEGORY_GTP_CHECK/*log*/) == false)
		{
			//TODO/TBD Swamping the log with malformed messages -> DOS
			ERROR(("GTP-U packet validation failed for packet from %s %s port %u. Ignoring packet\n", Direction == ProxyDirection::TO_SGW_REMOTE ? "P-GW" : "S-GW", inet_ntoa(fromaddr.sin_addr), htons(fromaddr.sin_port)));
			ERROR_DUMP((buffer,rcvsize));

			/*TODO
			If a GTP entity receives a Request message within an IP/UDP packet of a length that is inconsistent with the value
			specified in the Length field of the GTP header, then the receiving GTP entity should log the error and shall send the
			Response message with Cause IE value set to "Invalid message format".
			*/
		}
		else
		{
			uint8_t GtpMsgType = getGtpHeaderMessageType(buffer, rcvsize);
			GTP_VERSION Version = getGtpHeaderVersion(buffer, rcvsize);

			if(Version != GTP_VERSION_1)
			{
				ERROR(("GTP-U packet unexpected version from %s. Ignoring packet\n", Direction == ProxyDirection::TO_SGW_REMOTE ? "P-GW" : "S-GW"));
				return;
			}

			//TBD: Echo request on data plane???
		    if(GtpMsgType == GTPV1_ECHO_REQ)
		    {
		    	if(Direction == ProxyDirection::TO_PGW_REMOTE)
		    		ProcessEchoRequestMessage(Version, buffer, rcvsize, fd_from, fromaddr);
		    	return;
		    }

			uint32_t in_teid, teid;
			in_teid = teid = getGtpHeaderTeid(buffer, rcvsize);

			bool SendErrorIndicationOnFailure = true;

		    if(GtpMsgType == GTPV1_ERROR)
		    {
		    	if(!ProcessErrorIndicationMessage(teid, Direction, buffer, rcvsize, fd_from, fromaddr))
		    		return;
		    	//teid value is now the unknown TEID at the remote side
		    	SendErrorIndicationOnFailure = false;//no reply if we can't get a destination address
		    }
		    else
		    {
				if(Direction == ProxyDirection::TO_SGW_REMOTE)
				{
					pgw_2_sgw_u_data_count += rcvsize;
				}
				else
				{
					sgw_2_pgw_u_data_count += rcvsize;
				}
		    }

		    bool ReceivedEchoResponseFromPgw = true;
			if(m_ThePdpManagerInterface->GetGpduDestinationAddress(teid, Direction, dst, fromaddr, ReceivedEchoResponseFromPgw, rcvsize))
		    {
				setGtpHeaderTeid(buffer, rcvsize, teid);

				if(LOG_CATEGORY_GTP_U)
				{
					//DEBUG: log the outgoing packet
					checkGtpPacket(buffer, rcvsize, LOG_LEVEL_DEBUG && LOG_CATEGORY_GTP_CHECK/*log*/);
					uint32_t OoutboundSequenceNumber = getGtpHeaderSequenceNumber(buffer, rcvsize);

					//TODO test on loglevel before src & dst conversion to string
					std::string From = FormatStr("%s port %u",inet_ntoa(fromaddr.sin_addr), htons(fromaddr.sin_port));
					std::string Bind = FormatStr("%s port %u",inet_ntoa(bindaddr.sin_addr), htons(bindaddr.sin_port));
					std::string Source = FormatStr("%s port %u",inet_ntoa(src.sin_addr), htons(src.sin_port));
					std::string Destination = FormatStr("%s port %u",inet_ntoa(dst.sin_addr), htons(dst.sin_port));

					INFO(("GTP-U %s %s TEID:0x%08x -> on %s -> fwd via %s -> %s %s TEID:0x%08x Seq:%u\n",
							Direction == ProxyDirection::TO_PGW_REMOTE ? "S-GW" : "P-GW",
							From.c_str(),
							in_teid,
							Bind.c_str(),
							Source.c_str(),
							Direction == ProxyDirection::TO_PGW_REMOTE ? "P-GW" : "S-GW",
							Destination.c_str(),
							teid,
							OoutboundSequenceNumber));
				}
				ssize_t sndsize = sendto(fd_src, buffer, rcvsize, MSG_NOSIGNAL, (const struct sockaddr*)&dst, sizeof(dst));

				//TODO DEBUG: remove the next lines after debug else we have a log file bomber when the P-GW is down
				if(sndsize == -1 && ReceivedEchoResponseFromPgw)
				{
					ERROR(("GTP-U failed to send packet to %s address %s port %u\n", Direction == ProxyDirection::TO_PGW_REMOTE ? "P-GW" : "S-GW", inet_ntoa(dst.sin_addr), htons(dst.sin_port)));
				}
				else if(LOG_CATEGORY_GTP_U)
				{
					PRINT(("GTP-U TX size %u to %s %s port %u:\n",
							rcvsize, Direction == ProxyDirection::TO_PGW_REMOTE ? "P-GW" : "S-GW", inet_ntoa(dst.sin_addr), htons(dst.sin_port)));
					PRINT_DUMP((buffer,rcvsize));
				}
		    }
		    else
		    {
				//TODO DEBUG: remove the next lines after debug else we have a log file bomber when the there is no PDP context on record
		    	if(LOG_CATEGORY_GTP_U)ERROR(("GTP-U no valid destination address found for packet to %s\n", Direction == ProxyDirection::TO_PGW_REMOTE ? "P-GW" : "S-GW"));

				/*When a GTP-U node receives a GTP-U PDU for which no EPS Bearer context, PDP context, MBMS Bearer context, or
				RAB exists, the GTP-U node shall discard the GTP-U PDU and return a GTP error indication to the originating node.
				GTP entities may include the "UDP Port" extension header (Type 0x40), in order to simplify the implementation of
				mechanisms that can mitigate the risk of Denial-of-Service attacks in some scenarios.*/
				if(SendErrorIndicationOnFailure)
					SendDataPlaneErrorIndicationMessage(teid, fd_from, fromaddr, bindaddr);
		    }
		}
	}
}
