/*
 * gtppacket.cpp
 *
 *  Created on: Aug 29, 2018
 *      Author: JpU FK
 */

#include <map>
#include <vector>
#include <ctime>

#include "gtppacket.h"

#include "logger.h"
#include "utils.h"
#include "defs.h"

void fteidtostr(const unsigned char* data, const unsigned int len, std::string& dissectedfteid)
{
	unsigned int offset = 0;
	const bool HaveIPv4Address = *(data+offset) & 0x80;
	const bool HaveIPv6Address = *(data+offset) & 0x40;
	const unsigned char InterfaceType = *(data+offset) & 0x3F;
	offset++;

	uint32_t teid = ntohl(*(uint32_t*)(data+offset));
	offset += sizeof(uint32_t);

	dissectedfteid += FormatStr(" Type:0x%02x TEID:0x%08x", InterfaceType, teid);
	if(HaveIPv4Address)
	{
		uint32_t IPv4Address = ntohl(*(uint32_t*)(data+offset));
		dissectedfteid += FormatStr(" IPv4:0x%04x %s", IPv4Address, GetIPv4AddressStr(*(uint32_t*)(data+offset)));
		offset += sizeof(uint32_t);
	}
	if(HaveIPv6Address)
	{
		std::string IPv6Address;
		HexDumpToStr(data+offset,16,IPv6Address);
		dissectedfteid += FormatStr(" IPv6:%s",IPv6Address.c_str());
	}
}


class gtpv1ConstTagLenValues
{
private:
    static std::map<uint8_t/*Tag*/,uint8_t/*Len*/> gtpv1ConstTagLenValuesMap;

public:

    static uint8_t getGtpv1ConstTvLen(const uint8_t Tag)
    {
    	std::map<uint8_t/*Tag*/,uint8_t/*Len*/>::const_iterator Iter = gtpv1ConstTagLenValuesMap.find(Tag);
    	if(Iter!=gtpv1ConstTagLenValuesMap.end())
    		return Iter->second;
    	return 0;
    }

};

std::map<uint8_t,uint8_t> gtpv1ConstTagLenValues::gtpv1ConstTagLenValuesMap = {
	{GTPV1_IE_CAUSE,1},
	{GTPV1_IE_IMSI,8},
	{GTPV1_IE_RAI,6},
	{GTPV1_IE_TLLI,4},
	{GTPV1_IE_P_TMSI,4},
	//{GTPV1_IE_QOS_PROFILE0,3},
	{GTPV1_IE_REORDER,1},
	{GTPV1_IE_AUTH_TRIPLET,28},
	{GTPV1_IE_MAP_CAUSE,1},
	{GTPV1_IE_P_TMSI_S,3},
	{GTPV1_IE_MS_VALIDATED,1},
	{GTPV1_IE_RECOVERY,1},
	{GTPV1_IE_SELECTION_MODE,1},
	{GTPV1_IE_TEI_DI,4},
	{GTPV1_IE_TEI_C,4},
	{GTPV1_IE_TEI_DII,5},
	{GTPV1_IE_TEARDOWN,1},
	{GTPV1_IE_NSAPI,1},
	{GTPV1_IE_RANAP_CAUSE,1},
	{GTPV1_IE_RAB_CONTEXT,9},
	{GTPV1_IE_RP_SMS,1},
	{GTPV1_IE_RP,1},
	{GTPV1_IE_PFI,2},
	{GTPV1_IE_CHARGING_C,2},
	{GTPV1_IE_TRACE_REF,2},
	{GTPV1_IE_TRACE_TYPE,2},
	{GTPV1_IE_MS_NOT_REACH,1},
	{GTPV1_IE_CHARGING_ID,4}
};


class gtpv1TagStrings
{
private:
    static std::map<uint8_t/*Tag*/,std::string/*String*/> gtpv1TagStringValuesMap;

public:

    static const std::string getGtpv1TagStr(const uint8_t Tag)
    {
    	std::map<uint8_t/*Tag*/,std::string/*String*/>::const_iterator Iter = gtpv1TagStringValuesMap.find(Tag);
    	if(Iter!=gtpv1TagStringValuesMap.end())
    		return Iter->second;
    	return "UNKNOWN";
    }

};

std::map<uint8_t,std::string> gtpv1TagStrings::gtpv1TagStringValuesMap = {
	{GTPV1_IE_CAUSE,"CAUSE"},
	{GTPV1_IE_IMSI,"IMSI"},
	{GTPV1_IE_RAI,"RAI"},
	{GTPV1_IE_TLLI,"TLLI"},
	{GTPV1_IE_P_TMSI,"PTMSI"},
	{GTPV1_IE_REORDER,"REORDER"},
	{GTPV1_IE_AUTH_TRIPLET,"AUTH_TRIPLET"},
	{GTPV1_IE_MAP_CAUSE,"MAP-CAUSE"},
	{GTPV1_IE_P_TMSI_S,"PTMSI-S"},
	{GTPV1_IE_MS_VALIDATED,"MS-VALIDATED"},
	{GTPV1_IE_RECOVERY,"RECOVERY"},
	{GTPV1_IE_SELECTION_MODE,"SELECTION-MODE"},
	{GTPV1_IE_TEI_DI,"TEI_DI"},
	{GTPV1_IE_TEI_C,"TEI-C"},
	{GTPV1_IE_TEI_DII,"TEI-DII"},
	{GTPV1_IE_TEARDOWN,"TEARDOWN"},
	{GTPV1_IE_NSAPI,"NSAPI"},
	{GTPV1_IE_RANAP_CAUSE,"RANAP-CAUSE"},
	{GTPV1_IE_RAB_CONTEXT,"RAB-CONTEXT"},
	{GTPV1_IE_RP_SMS,"RP-SMS"},
	{GTPV1_IE_RP,"RP"},
	{GTPV1_IE_PFI,"PFI"},
	{GTPV1_IE_CHARGING_C,"CHARGING-C"},
	{GTPV1_IE_TRACE_REF,"TRACE-REF"},
	{GTPV1_IE_TRACE_TYPE,"TRACE-TYPE"},
	{GTPV1_IE_MS_NOT_REACH,"MS-NOT-REACHABLE"},
	{GTPV1_IE_CHARGING_ID,"CHARGING-ID"},
	{GTPV1_IE_EUA,"EUA"},
	{GTPV1_IE_MM_CONTEXT,"MM-CONTEXT"},
	{GTPV1_IE_PDP_CONTEXT,"PDP-CONTEXT"},
	{GTPV1_IE_APN,"APN"},
	{GTPV1_IE_PCO,"PCO"},
	{GTPV1_IE_GSN_ADDR,"GSN-ADDR"},
	{GTPV1_IE_MSISDN,"MSISDN"},
	{GTPV1_IE_QOS_PROFILE,"QOS-PROFILE"},
	{GTPV1_IE_AUTH_QUINTUP,"AUTH-QUINT"},
	{GTPV1_IE_TFT,"TFT"},
	{GTPV1_IE_TARGET_INF,"TARGET-INFO"},
	{GTPV1_IE_UTRAN_TRANS,"UTRAN-TRANS"},
	{GTPV1_IE_RAB_SETUP,"RAB-SETUP"},
	{GTPV1_IE_EXT_HEADER_T,"EXT-HEADER-T"},
	{GTPV1_IE_TRIGGER_ID,"TRIGGER-ID"},
	{GTPV1_IE_OMC_ID,"OMC-ID"},
	{GTPV1_IE_RAN_TRANS_CONT,"RAN-TRANSP-CONT"},
	{GTPV1_IE_PDP_CTX_PRIO,"PDP-CTX-PRIO"},
	{GTPV1_IE_RAB_SET_INFO,"RAB-SETUP-INFO"},
	{GTPV1_IE_SGSN_NUM,"SGSN-NUM"},
	{GTPV1_IE_COMM_FLAGS,"COMM-FLAGS"},
	{GTPV1_IE_APN_RESTR,"APN-RESTRICTION"},
	{GTPV1_IE_RP_LCS,"RP-LCS"},
	{GTPV1_IE_RAT_TYPE,"RAT-TYPE"},
	{GTPV1_IE_USER_LOCATION,"USER-LOC"},
	{GTPV1_IE_MS_TIMEZONE,"MS-TZ"},
	{GTPV1_IE_IMEI,"IMEI"},
	{GTPV1_IE_CAMEL_CIC,"CAMEL-CIC"},
	{GTPV1_IE_MBMS_UE_CTX,"MBMS-UE-CTX"},
	{GTPV1_IE_TMGI,"TGMI"},
	{GTPV1_IE_RIM_RA,"RIM-RA"},
	{GTPV1_IE_MBMS_PCO,"MBMS-PCO"},
	{GTPV1_IE_MBMS_RA,"MBMS-RA"},
	//TODO {,""},
	{GTPV1_IE_EARP_I,"EARP-I"},
	{GTPV1_IE_EARP_II,"EARP-II"},
	//TODO {,""},
	{GTPV1_IE_CHARGING_ADDR,"CHARGING-ADDR"},
	{GTPV1_IE_PRIVATE,"PRIVATE-EXT"}
};

class gtpv2TagStrings
{
private:
    static std::map<uint8_t/*Tag*/,std::string/*String*/> gtpv2TagStringValuesMap;

public:

    static const std::string getGtpv2TagStr(const uint8_t Tag)
    {
    	std::map<uint8_t/*Tag*/,std::string/*String*/>::const_iterator Iter = gtpv2TagStringValuesMap.find(Tag);
    	if(Iter!=gtpv2TagStringValuesMap.end())
    		return Iter->second;
    	return "UNKNOWN";
    }

};

std::map<uint8_t,std::string> gtpv2TagStrings::gtpv2TagStringValuesMap = {
	{ GTPV2_IE_IMSI,"IMSI"},
	{ GTPV2_IE_CAUSE,"CAUSE"},
	{ GTPV2_IE_RECOVERY,"RECOVERY"},
	{ GTPV2_IE_APN,"APN"},
	{ GTPV2_IE_AMBR,"AMBR"},
	{ GTPV2_IE_EPS_BEARER_ID,"EPS-BEARER-ID"},
	{ GTPV2_IE_IMEI,"IMEI"},
	{ GTPV2_IE_MSISDN,"MSISDN"},
	{ GTPV2_IE_INDICATION,"INDICATION"},
	{ GTPV2_IE_PCO,"PCO"},
	{ GTPV2_IE_PDN_ADDRESS_ALLOCATION,"PDN-ADDR-ALLOC"},
	{ GTPV2_IE_EPS_BEARER_LVL_QOS,"EPS-BEARER-LVL-QOS"},
	{ GTPV2_IE_EPS_FLOW_QOS,"EPS-FLOW-QOS"},
	{ GTPV2_IE_RAT_TYPE,"RAT-TYPE"},
	{ GTPV2_IE_SERVING_NET,"SERVING-NET"},
	{ GTPV2_IE_EPS_BEARER_TFT,"EPS-BEARER-TFT"},
	{ GTPV2_IE_TAD,"TAD"},
	{ GTPV2_IE_ULI,"ULI"},
	{ GTPV2_IE_FTEID,"F-TEID"},
	{ GTPV2_IE_BEARER_CONTEXT,"BEARER-CTX"},
	{ GTPV2_IE_CHARGING_ID,"CHARGING-ID"},
	{ GTPV2_IE_CHARGING_CHAR,"CHARGING-CHAR"},
	{ GTPV2_IE_PDN_TYPE,"PDN-TYPE"},
	{ GTPV2_IE_PROCEDURE_TRANS_ID,"PROC-TRANS-ID"},
	{ GTPV2_IE_UE_TIMEZONE,"UE-TZ"},
	{ GTPV2_IE_APN_RESTRICTION,"APN-RESTR"},
	{ GTPV2_IE_SEL_MODE,"SEL-MODE"},
	{ GTPV2_IE_FQCSID,"FQCSID"},
};

class gtpv1MessageTypeStrings
{
private:
    static std::map<uint8_t/*Tag*/,std::string/*String*/> gtpv1MessageTypeValuesMap;

public:

    static const std::string getGtpv1MessageTypeStr(const uint8_t Tag)
    {
    	std::map<uint8_t/*Tag*/,std::string/*String*/>::const_iterator Iter = gtpv1MessageTypeValuesMap.find(Tag);
    	if(Iter!=gtpv1MessageTypeValuesMap.end())
    		return Iter->second;
    	return "UNKNOWN";
    }

};

std::string gtppacket::getGtpv1MessageTypeStr(const uint8_t Tag) { return gtpv1MessageTypeStrings::getGtpv1MessageTypeStr(Tag); }

std::map<uint8_t,std::string> gtpv1MessageTypeStrings::gtpv1MessageTypeValuesMap = {
	{GTPV1_ECHO_REQ,"ECHO-REQ"},
	{GTPV1_ECHO_RSP,"ECHO-RSP"},
	{GTPV1_NOT_SUPPORTED,"NOT-SUPPORTED"},
	{GTPV1_CREATE_PDP_REQ,"CREATE-PDP-REQ"},
	{GTPV1_CREATE_PDP_RSP,"CREATE-PDP-RSP"},
	{GTPV1_UPDATE_PDP_REQ,"UPDATE-PDP-REQ"},
	{GTPV1_UPDATE_PDP_RSP,"UPDATE-PDP-RSP"},
	{GTPV1_DELETE_PDP_REQ,"DELETE-PDP-REQ"},
	{GTPV1_DELETE_PDP_RSP,"DELETE-PDP-RSP"},
	{GTPV1_INIT_PDP_ACTIVATE_REQ,"INIT-PDP-ACTIVATE-REQ"},
	{GTPV1_INIT_PDP_ACTIVATE_RSP,"INIT-PDP-ACTIVATE-RSP"},
	{GTPV1_ERROR,"ERROR-IND"},
	{GTPV1_PDU_NOTIF_REQ,"PDU-NOTIF-REQ"},
	{GTPV1_PDU_NOTIF_RSP,"PDU-NOTIF-RSP"},
	{GTPV1_PDU_NOTIF_REJ_REQ,"PDU-NOTIF-REJ-REQ"},
	{GTPV1_PDU_NOTIF_REJ_RSP,"PDU-NOTIF-REJ-RSP"},
	{GTPV1_SUPP_EXT_HEADER,"SUPP-EXT-HEADER"},
	{GTPV1_SND_ROUTE_REQ,"SND-ROUTE-REQ"},
	{GTPV1_SND_ROUTE_RSP,"SND-ROUTE-RSP"},
	{GTPV1_FAILURE_REQ,"FAILURE-REQ"},
	{GTPV1_FAILURE_RSP,"FAILURE-RSP"},
	{GTPV1_MS_PRESENT_REQ,"MS-PRESENT-REQ"},
	{GTPV1_MS_PRESENT_RSP,"MS-PRESENT-RSP"},
	{GTPV1_IDENT_REQ,"IDENT-REQ"},
	{GTPV1_IDENT_RSP,"IDENT-RSP"},
	{GTPV1_SGSN_CONTEXT_REQ,"SGSN-CTX-REQ"},
	{GTPV1_SGSN_CONTEXT_RSP,"SGSN-CTX-RSP"},
	{GTPV1_SGSN_CONTEXT_ACK,"SGSN-CTX-ACK"},
	{GTPV1_FWD_RELOC_REQ,"FWD-RELOC-REQ"},
	{GTPV1_FWD_RELOC_RSP,"FWD-RELOC-RSP"},
	{GTPV1_FWD_RELOC_COMPL,"FWD-RELOC-COMPL"},
	{GTPV1_RELOC_CANCEL_REQ,"RELOC-CNCL-REQ"},
	{GTPV1_RELOC_CANCEL_RSP,"RELOC-CNCL-RSP"},
	{GTPV1_FWD_SRNS,"FWD-SRNS"},
	{GTPV1_FWD_RELOC_ACK,"FWD-RELOC-ACK"},
	{GTPV1_FWD_SRNS_ACK,"FWD-SRNS-ACK"},
	{GTPV1_RAN_INFO_REL,"RAN-INFO-REL"},
	{GTPV1_GPDU,"GPDU"},
};

class gtpv2MessageTypeStrings
{
private:
    static std::map<uint8_t/*Tag*/,std::string/*String*/> gtpv2MessageTypeValuesMap;

public:

    static const std::string getGtpv2MessageTypeStr(const uint8_t Tag)
    {
    	std::map<uint8_t/*Tag*/,std::string/*String*/>::const_iterator Iter = gtpv2MessageTypeValuesMap.find(Tag);
    	if(Iter!=gtpv2MessageTypeValuesMap.end())
    		return Iter->second;
    	return "UNKNOWN";
    }

};

std::string gtppacket::getGtpv2MessageTypeStr(const uint8_t Tag) { return gtpv2MessageTypeStrings::getGtpv2MessageTypeStr(Tag); }

std::map<uint8_t,std::string> gtpv2MessageTypeStrings::gtpv2MessageTypeValuesMap = {
	{GTPV2_ECHO_REQ,"ECHO-REQ"},
	{GTPV2_ECHO_RSP,"ECHO-RSP"},
	{GTPV2_CREATE_SESSION_REQUEST,"CREATE-SESS-REQ"},
	{GTPV2_CREATE_SESSION_RESPONSE,"CREATE-SESS-RSP"},
	{GTPV2_MODIFY_BEARER_REQUEST,"MOD-BEARER-REQ"},
	{GTPV2_MODIFY_BEARER_RESPONSE,"MOD-BEARER-RSP"},
	{GTPV2_DELETE_SESSION_REQUEST,"DEL-SESS-REQ"},
	{GTPV2_DELETE_SESSION_RESPONSE,"DEL-SESS-RSP"},
	{GTPV2_CHANGE_NOTIFICATION_REQUEST,"CHANGE-NOTIF-REQ"},
	{GTPV2_CHANGE_NOTIFICATION_RESPONSE,"CHANGE-NOTIF-RSP"},
	{GTPV2_MODIFY_BEARER_COMMAND,"MOD-BEARER-CMD"},
	{GTPV2_MODIFY_BEARER_FAILURE_INDICATION,"MOD-BEARER-FAIL-IND"},
	{GTPV2_DELETE_BEARER_COMMAND,"DEL-BEARER-CMD"},
	{GTPV2_DELETE_BEARER_FAILURE_INDICATION,"DEL-BEARER-FAIL-IND"},
	{GTPV2_BEARER_RESOURCE_COMMAND,"BEARER-RSC-CMD"},
	{GTPV2_BEARER_RESOURCE_FAILURE_INDICATION,"BEARER-RSC-FAIL-IND"},
	{GTPV2_DOWNLINK_DATA_NOTIFICATION_FAILURE_INDICATION,"DWNLNK-DATA-NOTIF-FAIL-IND"},
	{GTPV2_TRACE_SESSION_ACTIVATION,"TRACE-SESS-ACT"},
	{GTPV2_TRACE_SESSION_DEACTIVATION,"TRACE-SESS-DECT"},
	{GTPV2_STOP_PAGING_INDICATION,"STOP-PAGING-IND"},
	{GTPV2_CREATE_BEARER_REQUEST,"CREATE-BEARER-REQ"},
	{GTPV2_CREATE_BEARER_RESPONSE,"CREATE-BEARER-RSP"},
	{GTPV2_UPDATE_BEARER_REQUEST,"UPDATE-BEARER-REQ"},
	{GTPV2_UPDATE_BEARER_RESPONSE,"UPDATE-BEARER-RSP"},
	{GTPV2_DELETE_BEARER_REQUEST,"DELETE-BEARER-REQ"},
	{GTPV2_DELETE_BEARER_RESPONSE,"DELETE-BEARER-RSP"},
	{GTPV1_GPDU,"GPDU"},
};


const char* gtppacket::getRatTypeStr(const uint8_t val)
{
	switch(val)
	{
	case 1:
		return "UTRAN-3G";
		break;
	case 2:
		return "GERAN-2G";
		break;
	case 3:
		return "WLAN";
		break;
	case 4:
		return "GAN";
		break;
	case 5:
		return "HSPA-Evo";
		break;
	case 6:
		return "EUTRAN-4G";
		break;
	default:
		break;
	}
	return "RAT_TYPE_UNKNOWN";
}

const char* gtppacket::getGtpVersionStr(const GTP_VERSION val)
{
	switch(val)
	{
	case GTP_VERSION_0:
		return "GTP_VERSION_0";
		break;
	case GTP_VERSION_1:
		return "GTP_VERSION_1";
		break;
	case GTP_VERSION_2:
		return "GTP_VERSION_2";
		break;
	default:
		break;
	}
	return "GTP_VERSION_UNKNOWN";
}

// Gets the header version of a GTP packet
// Returns -1 on error
gtppacket::GTP_VERSION gtppacket::getGtpHeaderVersion(const void *data, const uint32_t datalen)
{
	union gtpPacketHelper *packet = (union gtpPacketHelper *) data;
	if(datalen >= GTPV2_HEADER_MIN)
	{
		switch((packet->flags & 0xe0))
		{
		case 0x00:
			return GTP_VERSION_0;
			break;
		case 0x20:
			return GTP_VERSION_1;
			break;
		case 0x40:
			return GTP_VERSION_2;
			break;
		default:
			break;
		}
	    //ERROR(("Unknown packet version flags=0x%x\n", packet->flags));
	}
	return GTP_VERSION_UNKNOWN;
}

uint8_t gtppacket::getResponseMessageType(gtppacket::GTP_VERSION Version, uint8_t RequestMessageType)
{
	if(Version == GTP_VERSION_1)
	{
		switch(RequestMessageType)
		{
		case GTPV1_CREATE_PDP_REQ:
			return GTPV1_CREATE_PDP_RSP;
			break;
		case GTPV1_UPDATE_PDP_REQ:
			return GTPV1_UPDATE_PDP_RSP;
			break;
		case GTPV1_DELETE_PDP_REQ:
			return GTPV1_DELETE_PDP_RSP;
			break;
		case GTPV1_INIT_PDP_ACTIVATE_REQ:
			return GTPV1_INIT_PDP_ACTIVATE_RSP;
			break;
		default:
			break;
		}
	}
	else
	{
		switch(RequestMessageType)
		{
		case GTPV2_CREATE_SESSION_REQUEST:
			return GTPV2_CREATE_SESSION_RESPONSE;
			break;
		case GTPV2_MODIFY_BEARER_REQUEST:
			return GTPV2_MODIFY_BEARER_RESPONSE;
			break;
		case GTPV2_DELETE_SESSION_REQUEST:
			return GTPV2_DELETE_SESSION_RESPONSE;
			break;
		case GTPV2_CHANGE_NOTIFICATION_REQUEST:
			return GTPV2_CHANGE_NOTIFICATION_RESPONSE;
			break;
		case GTPV2_CREATE_BEARER_REQUEST:
			return GTPV2_CREATE_BEARER_RESPONSE;
			break;
		case GTPV2_UPDATE_BEARER_REQUEST:
			return GTPV2_UPDATE_BEARER_RESPONSE;
			break;
		case GTPV2_DELETE_BEARER_REQUEST:
			return GTPV2_DELETE_BEARER_RESPONSE;
			break;
		default:
			break;
		}
	}
	return 0;
}

// Get message type of a packet.
// Returns 0 on error - message type reserved for future use
uint8_t gtppacket::getGtpHeaderMessageType(const void *data, const uint32_t datalen)
{
	if(datalen >= GTPV2_HEADER_MIN)
	{
	    union gtpPacketHelper *packet = (union gtpPacketHelper *) data;
		if ((packet->flags & 0xe0) == 0x20 && datalen >= sizeof(gtpv1HeaderShortVersion))
		{
			// GTP Version 1
			return packet->gtp1s.header.type;
		}
		else if ((packet->flags & 0xe0) == 0x40 && datalen >= sizeof(gtpv2HeaderShortVersion))
		{
			return packet->gtp2s.header.type;
		}
		ERROR(("Unknown packet version flags=0x%x\n", packet->flags));
	}
	return 0;
}

// Gets the header length of a GTP packet
// Returns 0 on error
uint16_t gtppacket::getGtpHeaderLength(const void *data, const uint32_t datalen)
{
	if(datalen >= GTPV2_HEADER_MIN)
	{
		union gtpPacketHelper *packet = (union gtpPacketHelper *) data;
		if ((packet->flags & 0xe7) == 0x20 && datalen >= sizeof(gtpv1HeaderShortVersion))
		{
			//GTP Version 1 w/o sequence number
			return sizeof(gtpv1HeaderShortVersion);
		}
		else if ((packet->flags & 0xe0) == 0x20 && (packet->flags & 0x07) != 0x00 && datalen >= sizeof(gtpv1HeaderExtendedVersion))
		{
			//GTP Version 1 with sequence number
			uint16_t HeaderLength = sizeof(gtpv1HeaderExtendedVersion);
			uint8_t NextExtensionHeaderType = packet->gtp1l.header.next;
			uint8_t Offset = 0;
			while((packet->flags & 0x04) == 0x04 && NextExtensionHeaderType != 0x00 && Offset < (datalen - sizeof(gtpv1HeaderExtendedVersion)))
			{
				uint8_t ExtensionHeaderContentLen = packet->gtp1l.payload[Offset] * 4;//length is given in n * 4 Octets
				HeaderLength += ExtensionHeaderContentLen;
				Offset += ExtensionHeaderContentLen - 1;
				NextExtensionHeaderType = packet->gtp1l.payload[Offset];
				Offset++;
			}
			return HeaderLength;
		}
		else if ((packet->flags & 0xe0) == 0x40 && datalen >= sizeof(gtpv2HeaderShortVersion))
		{
			//GTP Version 2
			if((packet->flags & 0xe8) == 0x48)//TEID present
				return sizeof(gtpv2HeaderExtendedVersion);
			return sizeof(gtpv2HeaderShortVersion);
		}
		else
		{
			ERROR(("Unknown packet version flags=0x%x\n", packet->flags));
			return 0;
		}
	}
	return 0;
}

// Get payload length of a packet.
// Returns 0 on error
uint16_t gtppacket::getGtpHeaderPayloadLength(const void *data, const uint32_t datalen)
{
	if(datalen >= GTPV2_HEADER_MIN)
	{
		union gtpPacketHelper *packet = (union gtpPacketHelper *) data;
		if ((packet->flags & 0xe0) == 0x20 && datalen >= sizeof(gtpv1HeaderShortVersion))
		{
			// GTP Version 1
			return ntohs(packet->gtp1s.header.length);
		}
		else if ((packet->flags & 0xe0) == 0x40 && datalen >= sizeof(gtpv2HeaderShortVersion))
		{
			return ntohs(packet->gtp2s.header.length);
		}
		else
		{
			ERROR(("Unknown packet version flags=0x%x\n", packet->flags));
			return 0;
		}
	}
	return 0;
}

// Get sequence number of a packet.
// Returns 0 on error
uint32_t gtppacket::getGtpHeaderSequenceNumber(const void *data, const uint32_t datalen)
{
	if(datalen >= GTPV2_HEADER_MIN)
	{
		union gtpPacketHelper *packet = (union gtpPacketHelper *)data;
		if ((packet->flags & 0xe2) == 0x22 && datalen >= sizeof(gtpv1HeaderExtendedVersion))
		{
			// GTP Version 1 with 16bit sequence number
			return ntohs(packet->gtp1l.header.seq);
		}
		else if ((packet->flags & 0xe2) == 0x20)
		{
			// GTP Version 1 without sequence number
			return 0;
		}
		else if ((packet->flags & 0xe0) == 0x40 && datalen >= sizeof(gtpv2HeaderShortVersion))
		{
			// GTP Version 2 - 24bit sequence number
			if((packet->flags & 0x08) == 0x08)//TEID present
				return packet->gtp2l.header.getseq();
			return packet->gtp2s.header.getseq();
		}
		else
		{
			ERROR(("Unknown packet version flags=0x%x\n", packet->flags));
			return 0;
		}
	}
	return 0;
}


void gtppacket::setGtpHeaderSequenceNumber(void *data, const uint32_t datalen, const uint32_t SequenceNumber)
{
	if(datalen >= GTPV2_HEADER_MIN)
	{
		union gtpPacketHelper *packet = (union gtpPacketHelper *)data;
		if ((packet->flags & 0xe2) == 0x22 && datalen >= sizeof(gtpv1HeaderExtendedVersion))
		{
			// GTP Version 1 with 16bit sequence number
			packet->gtp1l.header.seq = ntohs((uint16_t)SequenceNumber);
		}
		else if ((packet->flags & 0xe2) == 0x20)
		{
			// GTP Version 1 without sequence number
		}
		else if ((packet->flags & 0xe0) == 0x40 && datalen >= sizeof(gtpv2HeaderShortVersion))
		{
			// GTP Version 2 - 24bit sequence number
			if((packet->flags & 0x08) == 0x08)//TEID present
				packet->gtp2l.header.setseq(SequenceNumber);
			else
				packet->gtp2s.header.setseq(SequenceNumber);
		}
		else
		{
			ERROR(("Unknown packet version flags=0x%x\n", packet->flags));
		}
	}
}


// Get TEID of a packet.
// Returns (uint32_t)(-1) on error
uint32_t gtppacket::getGtpHeaderTeid(const void *data, const uint32_t datalen)
{
	if(datalen >= GTPV2_HEADER_MIN)
	{
		union gtpPacketHelper *packet = (union gtpPacketHelper *)data;
		if ((packet->flags & 0xe0) == 0x20 && datalen >= sizeof(gtpv1HeaderShortVersion))
		{
			// GTP Version 1
			return ntohl(packet->gtp1s.header.teid);
		}
		else if ((packet->flags & 0xe0) == 0x40 && datalen >= sizeof(gtpv2HeaderShortVersion))
		{
			// GTP Version 2
			if((packet->flags & 0x08) == 0x08 && datalen >= sizeof(gtpv2HeaderExtendedVersion))//TEID present
				return ntohl(packet->gtp2l.header.teid);
			return 0;
		}
		else
		{
			ERROR(("Unknown packet version flags=0x%x\n", packet->flags));
			return (uint32_t)-1;
		}
	}
	return (uint32_t)-1;
}

void gtppacket::setGtpHeaderTeid(void *data, const uint32_t datalen, const uint32_t teid)
{
	if(datalen >= GTPV2_HEADER_MIN)
	{
		union gtpPacketHelper *packet = (union gtpPacketHelper *)data;
		if ((packet->flags & 0xe0) == 0x20 && datalen >= sizeof(gtpv1HeaderShortVersion))
		{
			// GTP Version 1
			packet->gtp1s.header.teid = ntohl(teid);
		}
		else if ((packet->flags & 0xe0) == 0x40 && datalen >= sizeof(gtpv2HeaderShortVersion))
		{
			// GTP Version 2
			if((packet->flags & 0x08) == 0x08 && datalen >= sizeof(gtpv2HeaderExtendedVersion))//TEID present
				packet->gtp2l.header.teid = ntohl(teid);
		}
		else
		{
			ERROR(("Unknown packet version flags=0x%x\n", packet->flags));
		}
	}
}

bool checkGtpv1Ies(const unsigned char* data, const unsigned int datalen, const bool log)
{
	unsigned int offset = 0;
	while((offset+2) <= datalen)//presence of tag and 1 byte value minimum requirement
	{
		uint8_t Tag = *(data+offset);
		uint16_t Len = 0;

		offset++;
		if((Tag & 0x80) == 0x00)//TV type - no length element present in IE
		{
			Len = gtpv1ConstTagLenValues::getGtpv1ConstTvLen(Tag);
		}
		else
		{
			Len = ntohs(*(uint16_t*)(data+offset));
			offset += 2;//16bit Length element
		}

		if(log)
		{
			if((offset+Len) <= datalen)
			{
				std::string Value;
				HexDumpToStr(data+offset,(unsigned int)Len,Value);

				if(Tag == GTPV1_IE_GSN_ADDR)
				{
					Value += "->";
					Value += GetIPv4AddressStr(*(uint32_t*)(data+offset));
				}
				else if(Tag == GTPV1_IE_EUA && Len == 6)
				{
					Value += "->";
					//skip the first two bytes which are type organization and address type
					Value += GetIPv4AddressStr(*(uint32_t*)(data+offset+2));
				}
				else if(Tag == GTPV1_IE_IMSI)
				{
					Value += "->";
					std::string IMSI;
					HexDumpToStr(data+offset,(unsigned int)Len,IMSI,true/*reversenibble*/);
					Value += IMSI;
				}
				else if(Tag == GTPV1_IE_MSISDN)
				{
					Value += "->";
					std::string MSISDN;
					//skip the first byte which is numbering plan and number type
					HexDumpToStr(data+offset+1,(unsigned int)Len-1,MSISDN,true/*reversenibble*/);
					Value += MSISDN;
				}
				else if(Tag == GTPV1_IE_APN)
				{
					Value += "->";
					std::string APN;
					ApnToStr(data+offset,(unsigned int)Len,APN);
					Value += APN;
				}

				DEBUG(("\tTag:%03u %20s Len:%03u Val:%s\n", Tag, gtpv1TagStrings::getGtpv1TagStr(Tag).c_str(), Len, Value.c_str()));
			}
			else
			{
				ERROR(("\tLength error at value offset:%u\n",offset));
			}
		}
		offset += Len;
	}
	return offset == datalen;
}

bool gtppacket::getGtpv1Ie(const unsigned char* packet, const unsigned int packetlen, const unsigned char IeTag, const unsigned char IeInstance, unsigned char*& iedata, size_t& ielen)
{
	if(getGtpHeaderVersion(packet,packetlen) == gtppacket::GTP_VERSION_1)
	{
		uint16_t HeaderLength = getGtpHeaderLength(packet,packetlen);

		unsigned int offset = HeaderLength;
		int Instance = -1;
		while((offset+2) <= packetlen)//presence of tag and 1 byte value minimum requirement
		{
			uint8_t Tag = *(packet+offset);
			uint16_t Len = 0;

			offset++;
			if((Tag & 0x80) == 0x00)//TV type - no length element present in IE
			{
				Len = gtpv1ConstTagLenValues::getGtpv1ConstTvLen(Tag);
			}
			else
			{
				Len = ntohs(*(uint16_t*)(packet+offset));
				offset += 2;//16bit Length element
			}

			if((offset+Len) <= packetlen)
			{
				if(Tag == IeTag)
				{
					Instance++;
					if(Instance == IeInstance)
					{
						iedata = (unsigned char*)packet+offset;
						ielen = Len;
						return true;
					}
				}
			}
			else//length error
				return false;

			offset += Len;
		}
	}
	return false;
}

bool checkGtpv2Ies(const unsigned char* data, const unsigned int datalen, const unsigned int nestinglevel, const bool log)
{
	unsigned int offset = 0;

	std::string levelPrefix;
	for(unsigned int i=0;i<nestinglevel;i++)
		levelPrefix += '\t';

	while((offset+4) <= datalen)//presence of tag, length and instance values minimum requirement
	{
		uint8_t Tag = *(data+offset);
		offset++;

		uint16_t Len = ntohs(*(uint16_t*)(data+offset));
		offset += 2;//16bit Length element

		uint8_t Instance = (*(data+offset) & 0x0F);
		offset++;

		if(log)
		{
			if((offset+Len) <= datalen)
			{
				std::string Value;
				HexDumpToStr(data+offset,(unsigned int)Len,Value);

				if(Tag == GTPV2_IE_FTEID)
				{
					fteidtostr(data+offset,(unsigned int)Len,Value);
				}
				else if(Tag == GTPV2_IE_PDN_ADDRESS_ALLOCATION && Len == 5)
				{
					Value += "->";
					Value += GetIPv4AddressStr(*(uint32_t*)(data+offset+1));
				}
				else if(Tag == GTPV2_IE_IMSI)
				{
					Value += "->";
					std::string IMSI;
					HexDumpToStr(data+offset,(unsigned int)Len,IMSI,true/*reversenibble*/);
					Value += IMSI;
				}
				else if(Tag == GTPV2_IE_MSISDN)
				{
					Value += "->";
					std::string MSISDN;
					HexDumpToStr(data+offset,(unsigned int)Len,MSISDN,true/*reversenibble*/);
					Value += MSISDN;
				}
				else if(Tag == GTPV2_IE_APN)
				{
					Value += "->";
					std::string APN;
					ApnToStr(data+offset,(unsigned int)Len,APN);
					Value += APN;
				}

				DEBUG(("%sTag:%03u %20s Inst:%u Len:%03u Val:%s\n", levelPrefix.c_str(), Tag, gtpv2TagStrings::getGtpv2TagStr(Tag).c_str(), Instance, Len, Value.c_str()));

				if(Tag == GTPV2_IE_BEARER_CONTEXT)
					checkGtpv2Ies(data+offset,Len,nestinglevel+1,log);
			}
			else
			{
				ERROR(("\tLength error at value offset:%u\n",offset));
			}
		}
		offset += Len;
	}
	return offset == datalen;
}

bool getGtpv2IeInternal(const unsigned char* data, const unsigned int dataLen, const unsigned int offset, const unsigned char ieTag, int& IeInstance, unsigned char*& IeData, size_t& IeLen)
{
	unsigned int offs = offset;
	while((offs+4) <= dataLen)//presence of tag, length and instance values minimum requirement
	{
		uint8_t Tag = *(data+offs);
		offs++;

		uint16_t Len = ntohs(*(uint16_t*)(data+offs));
		offs += 2;//16bit Length element

		uint8_t Instance = (*(data+offs) & 0x0F);
		offs++;

		if((offs+Len) <= dataLen)
		{
			if(Tag == ieTag && (IeInstance == -1 || Instance == (uint8_t)IeInstance))
			{
				IeData = (unsigned char*)data+offs;
				IeLen = Len;
				IeInstance = Instance;
				return true;
			}
		}
		else//length error
			return false;

		offs += Len;
	}
	return false;
}


bool gtppacket::getGtpv2Ie(const unsigned char* packet, const unsigned int packetlen, const unsigned char ieTag, int& IeInstance, unsigned char*& IeData, size_t& IeLen)
{
	if(getGtpHeaderVersion(packet,packetlen) == gtppacket::GTP_VERSION_2)
	{
		uint16_t HeaderLength = getGtpHeaderLength(packet,packetlen);
		return getGtpv2IeInternal(packet, packetlen, HeaderLength, ieTag, IeInstance, IeData, IeLen);
	}
	return false;
}

bool gtppacket::getGtpv2NestedIe(const unsigned char* ieValue, const unsigned int ieValueLen, const unsigned char ieTag, int& IeInstance, unsigned char*& IeData, size_t& IeLen)
{
	return getGtpv2IeInternal(ieValue, ieValueLen, 0, ieTag, IeInstance, IeData, IeLen);
}

bool gtppacket::checkGtpPacket(const void *data, const uint32_t datalen, const bool log)
{
	gtppacket::GTP_VERSION Version = getGtpHeaderVersion(data,datalen);
	bool PacketOk = false;
	if(Version != GTP_VERSION_UNKNOWN)
	{
		PacketOk = true;
		uint16_t HeaderLength = getGtpHeaderLength(data,datalen);
		uint16_t MessageType = getGtpHeaderMessageType(data,datalen);
		uint16_t PayloadLength = getGtpHeaderPayloadLength(data,datalen);
		uint32_t SequenceNumber = getGtpHeaderSequenceNumber(data,datalen);
		uint32_t Teid = getGtpHeaderTeid(data,datalen);

		if(MessageType == 0 || PayloadLength == 0 || HeaderLength == 0)//TBD is there a message without payload?
			PacketOk = false;

		uint16_t MandatoryHeaderLen = Version == GTP_VERSION_2 ? GTPV2_HEADER_MIN : GTPV1_HEADER_MIN;//Mandatory header length is 8 Octets except for GTPv2 that defines 4 Octets as mandatory
		if((PayloadLength + MandatoryHeaderLen) != datalen)
		{
	        ERROR(("Length mismatch length=0x%04x not equal receive length 0x%04x\n", (PayloadLength+MandatoryHeaderLen), datalen));
			PacketOk = false;
		}

		if(log)
		{
			INFO(("%s HeaderCheck:%s Flags:0x%02x HeaderLen:%u MessageType:%03u %s PayloadLen:%u Seq:%u Teid:0x%08x\n",
					gtppacket::getGtpVersionStr(Version), PacketOk ? "OK" : "NOK", *(unsigned char*)data, HeaderLength,
					MessageType, Version == GTP_VERSION_2 ? gtpv2MessageTypeStrings::getGtpv2MessageTypeStr(MessageType).c_str() : gtpv1MessageTypeStrings::getGtpv1MessageTypeStr(MessageType).c_str(),
					PayloadLength, SequenceNumber, Teid));
		}

		if(PacketOk && MessageType != GTPV1_GPDU)
		{
			if(Version == GTP_VERSION_2)
				PacketOk &= checkGtpv2Ies(((unsigned char*)data)+HeaderLength, datalen-HeaderLength, 1, log);
			else
				PacketOk &= checkGtpv1Ies(((unsigned char*)data)+HeaderLength, datalen-HeaderLength, log);
		}
	}
	return PacketOk;
}
