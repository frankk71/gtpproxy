/*
 * utils.c
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#include <string>
#include <set>
#include <vector>
#include <ctime>

#include <stdarg.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "utils.h"

#include "logger.h"


constexpr char hexmap[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

void HexDumpToStr(const unsigned char* data, const unsigned int len, std::string& dump, bool reversenibble)
{
	dump.resize(len*2,' ');
	for(unsigned int i=0;i<len;++i)
	{
		if(reversenibble)//TBCD
		{
			dump[2 * i]     = hexmap[data[i] & 0x0F];
			dump[2 * i + 1] = hexmap[(data[i] & 0xF0) >> 4];
			if(dump[2 * i + 1] == 'f')//drop filler digit
				dump[2 * i + 1] = ' ';
		}
		else
		{
			dump[2 * i]     = hexmap[(data[i] & 0xF0) >> 4];
			dump[2 * i + 1] = hexmap[data[i] & 0x0F];
		}
	}
}

void ApnToStr(const unsigned char* data, const unsigned int len, std::string& dump)
{
	std::string s;

	for(unsigned int i=0;i<len;)
	{
		unsigned int elementLen = data[i++];
		s.assign((const char*)(data+i),elementLen);

		i += elementLen;

		if(i<len)
			s += '.';

		dump += s;
	}
}

std::string vformat(const char *fmt, va_list ap)
{
    // Allocate a buffer on the stack that's big enough for us almost
    // all the time.  Be prepared to allocate dynamically if it doesn't fit.
    size_t size = 1024;
    char stackbuf[1024];
    std::vector<char> dynamicbuf;
    char *buf = &stackbuf[0];
    va_list ap_copy;

    while (1) {
        // Try to vsnprintf into our buffer.
        va_copy(ap_copy, ap);
        int needed = vsnprintf (buf, size, fmt, ap);
        va_end(ap_copy);

        // NB. C99 (which modern Linux and OS X follow) says vsnprintf
        // failure returns the length it would have needed.  But older
        // glibc and current Windows return -1 for failure, i.e., not
        // telling us how much was needed.

        if (needed <= (int)size && needed >= 0) {
            // It fit fine so we're done.
            return std::string (buf, (size_t) needed);
        }

        // vsnprintf reported that it wanted to write more characters
        // than we allotted.  So try again using a dynamic buffer.  This
        // doesn't happen very often if we chose our initial size well.
        size = (needed > 0) ? (needed+1) : (size*2);
        dynamicbuf.resize(size);
        buf = &dynamicbuf[0];
    }
}

std::string FormatStr(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    std::string buf = vformat(fmt, ap);
    va_end(ap);
    return buf;
}

std::string FormatLocalTime(const time_t t)
{
	std::tm * ptm = std::localtime(&t);
	char buffer[32] = {0};

	// Format: yyyy.mm.dd hh:mm:ss
	std::strftime(buffer, sizeof(buffer), "%Y.%m.%d %H:%M:%S", ptm);
	return buffer;
}

const char* GetIPv4AddressStr(const uint32_t Address)
{
	struct sockaddr_in addr;
	addr.sin_addr.s_addr = Address;
	return inet_ntoa(addr.sin_addr);
}

void CheckSocketErrorNo(int Result, struct sockaddr_in& addr, const char* Operation)
{
	if(Result != 0)
	{
		ERROR(("Socket %s operation address %s port %u failed with %d - %s\n", Operation, inet_ntoa(addr.sin_addr), htons(addr.sin_port), errno, strerror(errno)));
	}
	else
	{
		DEBUG(("Socket %s operation address %s port %u succeeded\n", Operation, inet_ntoa(addr.sin_addr), htons(addr.sin_port)));
	}
}

std::string VectorOfStringsToCsv(const std::vector<std::string>& StringArray)
{
	std::string s;
	const size_t size = StringArray.size();
	for(size_t i=0;i<StringArray.size();i++)
	{
		s += StringArray[i];
		if(i+1<size)
			s += ",";
	}
	return s;
}
