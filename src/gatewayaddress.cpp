/*
 * gatewayaddress.cpp
 *
 *  Created on: Feb 22, 2019
 *      Author: JpU FK
 */

#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "gatewayaddress.h"

#include "defs.h"
#include "utils.h"

GatewayAddress::GatewayAddress()
{
	memset(&c_plane_addr,0,sizeof(struct sockaddr_in));
	c_plane_addr.sin_port = htons(GTPC_DEFAULT_PORT);

	memset(&u_plane_addr,0,sizeof(struct sockaddr_in));
	u_plane_addr.sin_port = htons(GTPU_DEFAULT_PORT);
}

std::string GatewayAddress::ToString() const
{
	std::string ControlPlaneAddr = inet_ntoa(c_plane_addr.sin_addr);
	std::string DataPlaneAddr = inet_ntoa(u_plane_addr.sin_addr);

	return FormatStr("GW C/D:%s-%u/%s-%u",
			ControlPlaneAddr.c_str(), htons(c_plane_addr.sin_port),
			DataPlaneAddr.c_str(), htons(u_plane_addr.sin_port));
}



