/*
 * TimerWheelManager.cpp
 *
 *  Created on: Jul 26, 2019
 *      Author: JpU FK
 */

#include "timerwheelmanager.h"
#include "logger.h"


class TimerWheelEvent
{
public:
	//Note: this class can't be copied since MemberTimerEvent assignment operator and copy constructor are deleted

	TimerWheelEvent(TimerWheelManager* Helper, const uint32_t KeyValue, const uint64_t Schedule)
        : m_TimerWheelManager(Helper), m_KeyValue(KeyValue), m_TimerEvent(this)
	{
		ScheduleTimerOnWheel(Schedule);
    }

	void Reschedule(const uint64_t Schedule)
	{
		ScheduleTimerOnWheel(Schedule);
    }

    void Cancel()
    {
    	//DEBUG(("TimerId:%p Key:%u canceled\n", static_cast<void*>(this), m_KeyValue));
    	m_TimerEvent.cancel();
    	m_TimerWheelManager->m_TimerIdKeeper.erase(static_cast<void*>(this));
    	delete this;
    }

    uint32_t GetKeyValue() const { return m_KeyValue; }

private:

    TimerWheelManager* m_TimerWheelManager;

    void OnTimerExpired()
    {
    	//DEBUG(("TimerId:%p Key:%u expired\n",static_cast<void*>(this), m_KeyValue));
    	m_TimerWheelManager->m_TimerIdKeeper.erase(static_cast<void*>(this));
    	m_TimerWheelManager->m_ExpiredTimers.push_back(std::make_tuple(static_cast<void*>(this), m_KeyValue));
    	delete this;
    }

    void ScheduleTimerOnWheel(const uint64_t Schedule)
    {
    	m_TimerWheelManager->m_TimerWheel->schedule(&m_TimerEvent, Schedule);
    	//DEBUG(("TimerId:%p Key:%u scheduled in %llu seconds\n", static_cast<void*>(this), m_KeyValue, Schedule));
    }

    uint32_t m_KeyValue;

    MemberTimerEvent<TimerWheelEvent, &TimerWheelEvent::OnTimerExpired> m_TimerEvent;
};



TimerWheelManager::TimerWheelManager()
{
	//the TimerWheel object can't be copied since its assignment operator and copy constructor are deleted
	m_TimerWheel = new TimerWheel;
	time(&m_LastAdvanceOnTimerWheel);
}

TimerWheelManager::~TimerWheelManager()
{
	DEBUG(("Closing timer wheel with %u element(s)\n", m_TimerIdKeeper.size()));
	for(std::set<void*>::const_iterator Iter = m_TimerIdKeeper.begin(), EndIter = m_TimerIdKeeper.end(); Iter!=EndIter; Iter++)
	{
		TimerWheelEvent* Event = static_cast<TimerWheelEvent*>(*Iter);
		m_TimerIdKeeper.erase(static_cast<void*>(Event));
		delete Event;
	}
	delete m_TimerWheel;
}

void TimerWheelManager::AdvanceTimerWheel(time_t Now, ExpiredTimersType& ExpiredTimers)
{
	if(Now - m_LastAdvanceOnTimerWheel > 0)//avoid assertion in m_TimerWheel->advance()
	{
		m_TimerWheel->advance(Now - m_LastAdvanceOnTimerWheel);
		m_LastAdvanceOnTimerWheel = Now;
		ExpiredTimers = m_ExpiredTimers;
		m_ExpiredTimers.clear();

		//DEBUG
		/*
		Tick t = m_TimerWheel->ticks_to_next_event();
		if(t == Tick(std::numeric_limits<Tick>::max()))
		{
			DEBUG(("TimerWheel current tick %llu - no timers on wheel\n", m_TimerWheel->now()));
		}
		else
		{
			DEBUG(("TimerWheel current tick %llu - next event in %llu ticks\n", m_TimerWheel->now(), t));
		}
		*/
	}
}

void* TimerWheelManager::CreateTimer(const uint32_t KeyValue, const time_t TimerTime)
{
	uint64_t Schedule = TimerTime - m_LastAdvanceOnTimerWheel;
	if(Schedule == 0)
		Schedule++;//avoid assertion in TimerWheel::schedule()

	TimerWheelEvent* TimerId = new TimerWheelEvent(this, KeyValue, Schedule);
	m_TimerIdKeeper.insert(static_cast<void*>(TimerId));

	//DEBUG(("TimerWheel current tick %llu - scheduled TimerId:%p with Key:%u in %llu ticks\n", m_TimerWheel->now(), static_cast<void*>(TimerId), KeyValue, Schedule));

	return static_cast<void*>(TimerId);
}

bool TimerWheelManager::RescheduleTimer(void* TimerId, const time_t TimerTime)
{
	bool retval = false;
	try
	{
		if(TimerId != NULL && m_TimerIdKeeper.find(TimerId) != m_TimerIdKeeper.end())
		{
			uint64_t Schedule = TimerTime - m_LastAdvanceOnTimerWheel;
			if(Schedule == 0)
				Schedule++;//avoid assertion in TimerWheel::schedule()

			TimerWheelEvent* Event = static_cast<TimerWheelEvent*>(TimerId);
			Event->Reschedule(Schedule);

			//DEBUG(("TimerWheel current tick %llu - Rescheduled TimerId:%p with Key:%u in %llu ticks\n", m_TimerWheel->now(), TimerId, Event->GetKeyValue(), Schedule));

			retval = true;
		}
		else
			WARN(("Invalid TimerId:%p\n", TimerId));
	}
	catch(...)
	{
		ERROR(("Operation on TimerId:%p failed\n", TimerId));
	}
	return retval;
}

bool TimerWheelManager::CancelTimer(void*& TimerId)
{
	bool retval = false;
	try
	{
		if(TimerId != NULL && m_TimerIdKeeper.find(TimerId) != m_TimerIdKeeper.end())
		{
			TimerWheelEvent* Event = static_cast<TimerWheelEvent*>(TimerId);
			//DEBUG(("Cancel TimerId:%p Key:%u\n", TimerId, Event->GetKeyValue()));

			Event->Cancel();//Cancel() removes the Event from m_TimerIdKeeper and invalidates the object(pointer)
			TimerId = NULL;
			retval = true;
		}
		else
			WARN(("Invalid TimerId:%p\n", TimerId));
	}
	catch(...)
	{
		ERROR(("Operation on TimerId:%p failed\n", TimerId));
	}
	return retval;
}



#ifdef TIMERWHEELTEST
//Artefacts from testing in the pdpmanager class

		ExpiredTimersType Timers;

		Timers.erase(std::remove(Timers.begin(), Timers.end(), ExpiredTimers[i]), Timers.end());

		if(Timers.size() == 3)
		{
			TimerIdAndKeyType t;
			uint32_t Key;
			void* TimerId;

			std::tie(TimerId, Key) = Timers[0];
			m_PdpTimerWheelManager.RescheduleTimer(TimerId,Now+15);

			std::tie(TimerId, Key) = t = Timers[1];
			m_PdpTimerWheelManager.CancelTimer(TimerId);

			Timers.erase(std::remove(Timers.begin(), Timers.end(), t), Timers.end());

			m_PdpTimerWheelManager.RescheduleTimer(TimerId,Now+15);
			m_PdpTimerWheelManager.CancelTimer(TimerId);

			TimerId = NULL;
			m_PdpTimerWheelManager.RescheduleTimer(TimerId,Now+15);
			m_PdpTimerWheelManager.CancelTimer(TimerId);
		}

		if(Timers.size() == 0)
		{
			while(Timers.size() < 5)
			{
				uint32_t Key = Timers.size();
				void* TimerId = m_PdpTimerWheelManager.CreateTimer(Key,Now+10+Key);
				Timers.push_back(std::make_tuple(TimerId, Key));
			}
		}
#endif
