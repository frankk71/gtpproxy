################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/TimerWheelManager.cpp \
../src/configuration.cpp \
../src/gatewayaddress.cpp \
../src/globals.cpp \
../src/gtppacket.cpp \
../src/gtpproxy.cpp \
../src/logger.cpp \
../src/messageprocessor.cpp \
../src/packetgateway.cpp \
../src/pdpmanager.cpp \
../src/pdprecord.cpp \
../src/pgwmanager.cpp \
../src/restartcount.cpp \
../src/utils.cpp \
../src/vty.cpp 

C_SRCS += \
../src/cmdline.c 

OBJS += \
./src/TimerWheelManager.o \
./src/cmdline.o \
./src/configuration.o \
./src/gatewayaddress.o \
./src/globals.o \
./src/gtppacket.o \
./src/gtpproxy.o \
./src/logger.o \
./src/messageprocessor.o \
./src/packetgateway.o \
./src/pdpmanager.o \
./src/pdprecord.o \
./src/pgwmanager.o \
./src/restartcount.o \
./src/utils.o \
./src/vty.o 

CPP_DEPS += \
./src/TimerWheelManager.d \
./src/configuration.d \
./src/gatewayaddress.d \
./src/globals.d \
./src/gtppacket.d \
./src/gtpproxy.d \
./src/logger.d \
./src/messageprocessor.d \
./src/packetgateway.d \
./src/pdpmanager.d \
./src/pdprecord.d \
./src/pgwmanager.d \
./src/restartcount.d \
./src/utils.d \
./src/vty.d 

C_DEPS += \
./src/cmdline.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -I"../inc" -O3 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	g++ -I"../inc" -O3 -g -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


